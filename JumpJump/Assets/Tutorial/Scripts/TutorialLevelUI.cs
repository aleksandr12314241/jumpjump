using UnityEngine;
using Common;
using Gameplay;
using DG.Tweening;

namespace Tutorial.Level {
    public class TutorialLevelUI : MonoBehaviour {
        [SerializeField]
        private RectTransform tutorialScreensRT;
        [SerializeField]
        private TutorialScreensData[] tutorialScreensDatas;
        [SerializeField]
        private Animator blackFieldAnimator;
        [SerializeField]
        private Animator tutorialFingesAnimator;

        private const float hideScreenPosX = 879f;
        private const float shownScreenPosX = 279f;

        private void Start() {
            tutorialScreensRT.anchoredPosition = new Vector2(hideScreenPosX, tutorialScreensRT.anchoredPosition.y);
        }

        internal void ShowScreens(CellType cellType) {
            tutorialScreensRT.DOKill();
            //устанавливать правильный скрин

            tutorialScreensRT.DOAnchorPosX(shownScreenPosX, 0.5f);

            for (int i = 0; i < tutorialScreensDatas.Length; i++) {
                if (cellType == tutorialScreensDatas[i].cellType) {
                    tutorialScreensDatas[i].screenGameObject.SetActive(true);
                    continue;
                }

                tutorialScreensDatas[i].screenGameObject.SetActive(false);
            }
        }

        internal void HideScreens(float delay) {
            tutorialScreensRT.DOKill();

            tutorialScreensRT.DOAnchorPosX(tutorialScreensRT.anchoredPosition.x, delay).OnComplete(() => {
                tutorialScreensRT.DOAnchorPosX(hideScreenPosX, 0.5f).OnComplete(() => {
                    for (int i = 0; i < tutorialScreensDatas.Length; i++) {
                        tutorialScreensDatas[i].screenGameObject.SetActive(false);
                    }
                });
            });
        }
        internal void HideScreensImmediately() {
            tutorialScreensRT.DOKill();
            for (int i = 0; i < tutorialScreensDatas.Length; i++) {
                tutorialScreensDatas[i].screenGameObject.SetActive(false);
            }
        }

        internal void ShowBlackField() {
            blackFieldAnimator.gameObject.SetActive(false);
            blackFieldAnimator.gameObject.SetActive(true);
        }

        internal void HideBlackFieldImmediately() {
            blackFieldAnimator.gameObject.SetActive(false);
        }

        internal void HideBlackField() {
            blackFieldAnimator.SetBool("StartHide", true);
            blackFieldAnimator.transform.DOScale(Vector3.one, 0.5f).OnComplete(() => {
                blackFieldAnimator.gameObject.SetActive(false);
            });
        }

        internal void ShowFingerScroll() {
            tutorialFingesAnimator.gameObject.SetActive(true);
            tutorialFingesAnimator.SetBool("isScrollField", true);
        }

        internal void ShowFinger() {
            tutorialFingesAnimator.gameObject.SetActive(true);
            tutorialFingesAnimator.SetBool("isScrollField", false);
        }

        internal void HideFinger() {
            tutorialFingesAnimator.gameObject.SetActive(false);
        }

        [System.Serializable]
        internal struct TutorialScreensData {
            [SerializeField]
            internal CellType cellType;
            [SerializeField]
            internal GameObject screenGameObject;
        }
    }
}