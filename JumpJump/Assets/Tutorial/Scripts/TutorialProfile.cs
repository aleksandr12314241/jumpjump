using System.Collections.Generic;
using UnityEngine;
using Gameplay;
using Common;

namespace Tutorial {
    public class TutorialProfile {
        [SerializeField]
        internal bool isFirstStart;
        [SerializeField]
        internal bool[] doneLevels;
        //[SerializeField]
        //internal List<CellType> cellsShown;
        [SerializeField]
        internal List<TutorialCellTypeData> cellsShown;

        public TutorialProfile() {
            //��������� ��������� �������������� � TutorialProfileToSave
            TutorialProfileToSave tutorialProfileToSave = SaveSystem.LoadTutorialProfileToSave();

            SetTutorialProfile(tutorialProfileToSave);
        }
        private void SetTutorialProfile(TutorialProfileToSave tutorialProfileToSave) {
            isFirstStart = tutorialProfileToSave.isFirstStart;

            doneLevels = new bool[tutorialProfileToSave.doneLevels.Length];
            for (int i = 0; i < doneLevels.Length; i++) {
                doneLevels[i] = tutorialProfileToSave.doneLevels[i];
            }

            cellsShown = new List<TutorialCellTypeData>();
            for (int i = 0; i < tutorialProfileToSave.cellsShown.Count; i++) {
                TutorialCellTypeData tutorialCellTypeData = new TutorialCellTypeData() {
                    cellType = tutorialProfileToSave.cellsShown[i].cellType,
                    currentLevelIndex = tutorialProfileToSave.cellsShown[i].currentLevelIndex
                };
                cellsShown.Add(tutorialCellTypeData);
            }
        }

        internal void AddCellShown(CellType currentCellType, int currentLevelIndex) {
            TutorialCellTypeData tutorialCellTypeData = cellsShown.Find(x => x.cellType == currentCellType);
            if (tutorialCellTypeData != null) {
                return;
            }

            cellsShown.Add(new TutorialCellTypeData() {
                cellType = currentCellType,
                currentLevelIndex = currentLevelIndex

            });
            SaveSystem.SavTutorialeProfile(this);
        }

        internal bool IsShowTutorial(CellType currentCellType, int currentLevelIndex) {
            TutorialCellTypeData tutorialCellTypeData = cellsShown.Find(x => x.cellType == currentCellType);

            if (tutorialCellTypeData == null) {
                return true;
            }


            return currentLevelIndex <= tutorialCellTypeData.currentLevelIndex;
        }

        internal class TutorialCellTypeData {
            internal CellType cellType;
            internal int currentLevelIndex;
        }
    }
}