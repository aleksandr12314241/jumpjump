using System.Collections.Generic;
using UnityEngine;
using Gameplay;

namespace Tutorial {

    public class TutorialProfileToSave {
        [SerializeField]
        public bool isFirstStart;
        [SerializeField]
        internal bool[] doneLevels;
        [SerializeField]
        public List<TutorialCellTypeData> cellsShown;

        public TutorialProfileToSave() {
            isFirstStart = true;
            doneLevels = new bool[3];
            cellsShown = new List<TutorialCellTypeData>();
        }

        public TutorialProfileToSave(TutorialProfile tutorialProfile) {
            isFirstStart = tutorialProfile.isFirstStart;

            doneLevels = new bool[tutorialProfile.doneLevels.Length];
            for (int i = 0; i < doneLevels.Length; i++) {
                doneLevels[i] = tutorialProfile.doneLevels[i];
            }

            cellsShown = new List<TutorialCellTypeData>();
            for (int i = 0; i < tutorialProfile.cellsShown.Count; i++) {
                TutorialCellTypeData tutorialCellTypeData = new TutorialCellTypeData() {
                    cellType = tutorialProfile.cellsShown[i].cellType,
                    currentLevelIndex = tutorialProfile.cellsShown[i].currentLevelIndex
                };
                cellsShown.Add(tutorialCellTypeData);
            }
        }
        public class TutorialCellTypeData {
            internal CellType cellType;
            internal int currentLevelIndex;
        }
    }
}