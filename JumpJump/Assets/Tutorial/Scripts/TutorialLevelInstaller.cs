using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Tutorial.Level {
    //[SerializeField]
    //protected Transform fieldTransform;
    //[Inject]
    //private LevelPrefabsDataSet levelPrefabsDataSet;

    public class TutorialLevelInstaller : MonoInstaller {
        public override void InstallBindings() {
            Container.BindInterfacesAndSelfTo<TutorialLevel>().AsSingle();

            Container.Bind<TutorialLevelUI>().FromComponentInHierarchy().AsSingle();
        }
    }
}