using System.Collections.Generic;
using UnityEngine;
using Gameplay;
using Zenject;
using System;
using Tools;

namespace Tutorial.Level {
    public class TutorialLevel : IInitializable, IDisposable {
        internal List<CellType> cellsTutorial;

        [Inject]
        private TutorialLevelUI tutorialLevelUI;
        [Inject]
        private TutorialProfile tutorialProfile;
        [Inject]
        private LevelGenerateController generateFieldController;
        [Inject]
        private FollowCameraUI followCameraUI;
        [Inject]
        private FollowCamera followCamera;

        private CellType currentCellTypeTutorial;

        private Player player;

        private int currentLevelIndex;

#if UNITY_EDITOR
        private ConfigDataSet configDataSet;
#endif

        public void Initialize() {

            currentCellTypeTutorial = CellType.Void;

            cellsTutorial = new List<CellType> {
                CellType.End,
                CellType.Spike,
                CellType.Enemy_0,
                CellType.DestroyedOnStep,
                CellType.Portal,
                CellType.EnemyShooter,
                CellType.EnemySpiked,
                CellType.EnemyTwisting,
                CellType.Spring,
                CellType.WallMoving,
                CellType.WallPushing
            };

            InitSubscribers();

            //�������� ������ 3� ������
        }

        public void Dispose() {
            generateFieldController.OnLevelGenerateEnd -= OnLevelGenerateEnd;
            generateFieldController.OnLevelsEnd -= OnLevelsEnd;
            generateFieldController.OnRestart -= OnRestart;

            followCameraUI.OnStartDragCamera -= OnStartDragCamera;

            generateFieldController = null;
            followCameraUI = null;
        }

        private void InitSubscribers() {
            generateFieldController.OnLevelGenerateEnd += OnLevelGenerateEnd;
            generateFieldController.OnLevelsEnd += OnLevelsEnd;
            generateFieldController.OnRestart += OnRestart;
        }

        private void OnLevelGenerateEnd(LevelGenerateController.GenerateLevelData level) {
            tutorialLevelUI.HideScreensImmediately();

            currentLevelIndex = level.CurrentLevelIndex;

            if (currentLevelIndex == 1) {
                followCameraUI.OnStartDragCamera -= OnStartDragCamera;
                followCameraUI.OnStartDragCamera += OnStartDragCamera;
            }

            InitPlayer(level.Player);

            Cell[][] cells = level.Cells;
            List<Enemy> enemies = level.Enemies;

            currentCellTypeTutorial = CellType.Void;

            for (int i = 0; i < enemies.Count; i++) {
                if (cellsTutorial.Contains(enemies[i].CellType) && tutorialProfile.IsShowTutorial(enemies[i].CellType, currentLevelIndex)) {
                    //���������� ���������
                    currentCellTypeTutorial = enemies[i].CellType;
                    break;
                }
            }

            if (currentCellTypeTutorial == CellType.Void) {
                for (int i = 0; i < cells.Length; i++) {
                    for (int j = 0; j < cells[i].Length; j++) {
                        if (cells[i][j] == null) {
                            continue;
                        }

                        if (cellsTutorial.Contains(cells[i][j].FloorCellType) && tutorialProfile.IsShowTutorial(cells[i][j].FloorCellType, currentLevelIndex)) {
                            //���������� ���������
                            currentCellTypeTutorial = cells[i][j].FloorCellType;
                            break;
                        }

                        if (cellsTutorial.Contains(cells[i][j].AboveCellType) && tutorialProfile.IsShowTutorial(cells[i][j].AboveCellType, currentLevelIndex)) {
                            //���������� ���������
                            currentCellTypeTutorial = cells[i][j].AboveCellType;
                            break;
                        }
                    }

                    if (currentCellTypeTutorial != CellType.Void) {
                        break;
                    }
                }
            }

#if UNITY_EDITOR            
            configDataSet = Resources.Load<ConfigDataSet>("ConfigDataSet");
            if (configDataSet.IsEditorPlaying) {
                tutorialLevelUI.HideFinger();
                tutorialLevelUI.HideBlackField();
                tutorialLevelUI.HideScreensImmediately();
            }
            else {
                StartTutorial();
            }
#else
            StartTutorial();
#endif

        }

        private void StartTutorial() {
            if (tutorialProfile.isFirstStart) {
                currentCellTypeTutorial = CellType.End;

                tutorialLevelUI.ShowFinger();
                tutorialLevelUI.ShowBlackField();
                tutorialLevelUI.ShowScreens(CellType.End);

                return;
            }

            if (currentLevelIndex == 0) {
                tutorialLevelUI.ShowFinger();
            }
            else if (currentLevelIndex == 1) {
                followCamera.SetIsPlayerControlImmediately(true);
                tutorialLevelUI.ShowFingerScroll();
            }
            else if (currentLevelIndex == 2) {
                tutorialLevelUI.ShowFinger();
            }
            else {
                tutorialLevelUI.HideBlackFieldImmediately();
            }

            if (currentCellTypeTutorial != CellType.Void) {
                tutorialLevelUI.ShowScreens(currentCellTypeTutorial);
            }
            else {
                tutorialLevelUI.HideBlackFieldImmediately();
            }
        }

        private void InitPlayer(Player player) {

            this.player = player;

            RemovePlayerListners();

            player.OnStartJump += OnStartJump;
            player.OnEndJump += OnEndJump;
            player.OnGoToPortal += OnGoToPortal;
            player.OnDamage += OnDamage;
        }

        private void RemovePlayerListners() {
            player.OnStartJump -= OnStartJump;
            player.OnEndJump -= OnEndJump;
            player.OnStartPush -= OnStartPush;
            player.OnEndPush -= OnEndPush;
            player.OnEndJump -= OnEndJump;
            player.OnGoToPortal -= OnGoToPortal;
            player.OnDamage -= OnDamage;
        }

        private void OnDamage() {
            OnPlayerAction();
        }

        private void OnGoToPortal(Vector2Int arg1, Vector2Int arg2) {
            OnPlayerAction();
        }

        private void OnEndPush(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            OnPlayerAction();
        }

        private void OnStartPush(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            OnPlayerAction();
        }

        private void OnEndJump(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            OnPlayerAction();
        }

        private void OnStartJump(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            OnPlayerAction();
        }

        private void OnPlayerAction() {
            tutorialLevelUI.HideFinger();

            tutorialLevelUI.HideBlackField();

            tutorialLevelUI.HideScreens(1f);
        }

        private void OnLevelsEnd() {
        }

        internal void OnWinLevel() {
            if (tutorialProfile.isFirstStart) {
                tutorialProfile.isFirstStart = false;
            }

            if (currentLevelIndex == 1) {
                followCameraUI.OnStartDragCamera -= OnStartDragCamera;
            }

            tutorialProfile.AddCellShown(currentCellTypeTutorial, currentLevelIndex);
        }

        internal void OnLoseLevel() {
            //StartTutorial();
        }

        private void OnRestart() {
            StartTutorial();
        }

        private void OnStartDragCamera() {
            if (currentLevelIndex == 1) {
                tutorialLevelUI.HideFinger();
            }
        }
    }
}