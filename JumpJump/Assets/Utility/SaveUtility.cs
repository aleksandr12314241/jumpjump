using UnityEngine;
using System.IO;
using System.Text;

namespace Utility {
    public class SaveUtility {
        //GETTERS
        internal static string GetSavePath(string saveName) {
#if UNITY_EDITOR

            if (!Directory.Exists(Application.dataPath + "/Data")) {
                Directory.CreateDirectory(Application.dataPath + "/Data");
            }

            return Path.Combine(Application.dataPath, "Data/" + saveName + ".json");
#elif UNITY_ANDROID||UNITY_IOS
            return Path.Combine( Application.persistentDataPath, saveName + ".json" );
#else
            return Path.Combine( Application.persistentDataPath, saveName + ".json" );
#endif
        }

        internal static void SaveObject(string saveName, object saveObject) {
            string savePath = GetSavePath(saveName);
            string json = JsonUtility.ToJson(saveObject);

            //#if UNITY_EDITOR
            //            string savePathDecipher = Application.dataPath + "/Data/" + saveName + "_decipher.json";
            //            File.WriteAllText(savePathDecipher, json, Encoding.UTF8);
            //#endif
            File.WriteAllText(savePath, json, Encoding.UTF8);
        }

        internal static T LoadObject<T>(string saveName) {
            string savePath = GetSavePath(saveName);
            string jsonString = File.ReadAllText(savePath);
            return JsonUtility.FromJson<T>(jsonString);
        }
    }
}