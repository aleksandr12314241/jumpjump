using UnityEngine;
using UnityEngine.UI;

namespace Common {

    public abstract class WindowMonoBehaviour : MonoBehaviour {
        [SerializeField]
        protected Button backButton;
        [SerializeField]
        protected RectTransform containerRectTransform;

        internal bool IsShown {
            get;
            private set;
        }

        private void Start() {
            InitStart();
        }

        protected void InitStartBase() {
            Hide();
            backButton.onClick.AddListener( Hide );
        }

        internal virtual void InitAwake() {
        }

        internal abstract void InitStart();

        internal virtual void Show() {
            containerRectTransform.gameObject.SetActive( true );
            backButton.gameObject.SetActive( true );

            SetIsShown( true );
        }

        internal virtual void Hide() {
            containerRectTransform.gameObject.SetActive( false );
            backButton.gameObject.SetActive( false );

            SetIsShown( false );
        }

        protected void SetIsShown( bool isShown ) {
            this.IsShown = isShown;
        }
    }
}