using System;

namespace Utility.Do {
    public static class DoUtility {
        public static T Do<T>( this T obj, Action<T> action ) {
            return Do<T>( obj, action, true );
        }

        public static T Do<T>( this T obj, Action<T> action, bool when ) {
            if ( when ) {
                action.Invoke( obj );
            }
            return obj;
        }
    }
}