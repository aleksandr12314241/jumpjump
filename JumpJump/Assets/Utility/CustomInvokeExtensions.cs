using System;
using UnityEngine;
using System.Collections;

namespace Utility {

    public static class CustomInvokeExtensions {
        public static void CustomInvoke(this MonoBehaviour me, float time, Action theDelegate) {
            me.StartCoroutine(ExecuteAfterTime(time, theDelegate));
        }

        private static IEnumerator ExecuteAfterTime(float delay, Action theDelegate) {
            yield return new WaitForSeconds(delay);
            theDelegate();
        }
    }
}
