using UnityEngine;
using UnityEngine.UI;
using Common;
using System;

namespace Menu {

    public class MenuLevelItem : MonoBehaviour {
        internal Action<int> OnClickTryPlay = (int arg) => { };

        [SerializeField]
        private RectTransform myRectTransform;
        [SerializeField]
        private Button myButton;
        [SerializeField]
        private Text levelNameText;
        [SerializeField]
        private CanvasGroup canvasGroup;

        private int levelIndex;

        public RectTransform MyRectTransform {
            get => myRectTransform;
        }

        public void Init(int levelIndex, SequenceLevelsDataSet.LevelInfoSequence levelInfoSequence) {
            myButton.onClick.AddListener(TryPlay);
            this.levelIndex = levelIndex;
            levelNameText.text = levelIndex.ToString() + " | " + levelInfoSequence.levelName;
        }

        internal void SetActive(bool isActive) {
            canvasGroup.alpha = isActive ? 1f : 0.5f;
        }

        private void TryPlay() {
            OnClickTryPlay(levelIndex);
        }
    }
}