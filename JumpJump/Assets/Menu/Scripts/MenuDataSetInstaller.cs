using UnityEngine;
using Zenject;

namespace Menu {
    [CreateAssetMenu(fileName = "MenuDataSetInstaller", menuName = "JumpJump/MenuDataSetInstaller")]
    public class MenuDataSetInstaller : ScriptableObjectInstaller<MenuDataSetInstaller> {
        [SerializeField]
        private SkinMenuDataSet skinMenuDataSet;

        public override void InstallBindings() {
            Container.BindInstances(skinMenuDataSet);
        }
    }
}
