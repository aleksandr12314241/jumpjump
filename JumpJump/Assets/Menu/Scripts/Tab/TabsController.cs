using UnityEngine;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Menu {
    public class TabsController : MonoBehaviour {
        internal event Action<int> OnOpenTab = (int param) => { };

        [SerializeField]
        RectTransform containerTabRT;
        [SerializeField]
        EventTrigger eventTrigger;
        [SerializeField]
        TabAndButton[] tabs;

        private int indexOpenTab = -1;

        float screenWidth;

        RectTransform rectCanvas;
        Camera cameraMain;
        Vector2 startDragPosition;
        DateTime beginDragDateTime;

        internal int OpenTab {
            get
            {
                return indexOpenTab;
            }
        }

#if UNITY_EDITOR
        private void OnValidate() {
            if (tabs.Length <= 0) {
                Debug.LogError("|TabsContainer|<OnValidate> ������ ���� ������");
                return;
            }

            for (int i = 0; i < tabs.Length; i++) {
                if (tabs[i].Button == null) {
                    Debug.LogError("|TabsContainer|<OnValidate>  � " + i + "��� ���� ������ =  null");
                    return;
                }
                else if (tabs[i].Tab == null) {
                    Debug.LogError("|TabsContainer|<OnValidate>  � " + i + "��� ���� ��� =  null");
                    return;
                }
            }
        }
#endif

        void Start() {
            rectCanvas = FindObjectOfType<CanvasScaler>().GetComponent<RectTransform>();
            cameraMain = Camera.main;

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.BeginDrag;
            entry.callback.AddListener((data) => { OnBeginDrag((PointerEventData)data); });
            eventTrigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drag;
            entry.callback.AddListener((data) => { OnDrag((PointerEventData)data); });
            eventTrigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((data) => { OnEndDrag((PointerEventData)data); });
            eventTrigger.triggers.Add(entry);

            for (var i = 0; i < tabs.Length; i++) {
                var index = i;
                tabs[i].Tab.InitBase(i);
                tabs[i].Tab.OnOpenMe += ShowTab;
            }

            screenWidth = GetComponent<RectTransform>().rect.width;
            containerTabRT.sizeDelta = new Vector2(screenWidth * tabs.Length, containerTabRT.sizeDelta.y);

            for (var i = 0; i < tabs.Length; i++) {
                var index = i;
                tabs[i].Button.tabButton.onClick.AddListener(() => {
                    ShowTab(index);
                });
            }

            for (int i = 0; i < tabs.Length; i++) {
                tabs[i].Init();
            }

            indexOpenTab = 1;
            tabs[indexOpenTab].ShowTabForce();

            for (int i = 0; i < tabs.Length; i++) {
                int indexMove = indexOpenTab - i;
                tabs[i].MoveImmediately(indexMove);

                if (indexOpenTab == i) {
                    continue;
                }
                tabs[i].HideTabForce();
            }
        }

        internal void OnShowTabByIndex(int index) {
            if (indexOpenTab == index) {
                return;
            }
            ShowTab(index);
        }

        private void ShowTab(int indexTab) {

            if (tabs.Length < 0) {
                Debug.Log("|TabsContainer|<OnValidate> ��������  = " + tabs.Length + " ���� ");
                return;
            }

            if (tabs.Length <= indexTab) {
                Debug.Log("|TabsContainer|<OnValidate> ������� ������� ������ = " + indexTab +
                           " �� ���������� � ��������  = " + tabs.Length);
                return;
            }

            indexOpenTab = indexTab;

            for (int i = 0; i < tabs.Length; i++) {
                if (tabs[i] == null) {
                    Debug.Log("|TabsContainer|<OnValidate> ��������� ������ _tabs[" + i + "]  = null");
                    return;
                }

                if (tabs[i].Tab == null) {
                    Debug.Log("|TabsContainer|<OnValidate> ��������� ������ _tabs[" + i + "].Tab  = null");
                    return;
                }

                tabs[i].ShowTabFiction();
            }

            for (int i = 0; i < tabs.Length; i++) {
                int indexMove = indexTab - i;
                tabs[i].Move(indexMove);

                if (i == indexTab) {
                    tabs[i].ShowTabButton();
                }
                else {
                    tabs[i].HideTabButton();
                }
            }
            OnOpenTab(indexTab);
        }


        void OnBeginDrag(PointerEventData eventData) {
            for (int i = 0; i < tabs.Length; i++) {
                tabs[i].BeginDrag();
            }
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectCanvas, eventData.position, cameraMain, out startDragPosition);

            beginDragDateTime = DateTime.Now;
        }

        void OnDrag(PointerEventData eventData) {
            Vector2 dragPosition;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectCanvas, eventData.position, cameraMain, out dragPosition);

            Vector2 offsetPosition = startDragPosition - dragPosition;

            for (int i = 0; i < tabs.Length; i++) {
                tabs[i].Drag(offsetPosition);
            }
        }

        void OnEndDrag(PointerEventData eventData) {
            Vector2 endDragPosition;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectCanvas, eventData.position, cameraMain, out endDragPosition);

            Vector2 offsetPosition = startDragPosition - endDragPosition;

            TimeSpan offsetTime = DateTime.Now - beginDragDateTime;
            //Debug.Log( offsetTime.Milliseconds );
            //Debug.Log( "offsetPosition   " + offsetPosition );

            if (offsetTime.Milliseconds < 100f && (offsetPosition.x * offsetPosition.x) >= (150f * 150f)) {
                if (offsetPosition.x >= 0) {
                    ChangeTab(1);
                }
                else {
                    ChangeTab(-1);
                }
            }
            else if ((offsetPosition.x * offsetPosition.x) >= (250f * 250f)) {
                if (offsetPosition.x >= 0) {
                    ChangeTab(1);
                }
                else {
                    ChangeTab(-1);
                }
            }
            else {
                for (int i = 0; i < tabs.Length; i++) {
                    tabs[i].EndDrag();
                }
            }
        }

        void ChangeTab(int sign) {
            int openTabTemp = indexOpenTab + sign;
            if (openTabTemp < 0 || openTabTemp >= tabs.Length) {
                for (int i = 0; i < tabs.Length; i++) {
                    tabs[i].EndDrag();
                }
                return;
            }

            ShowTab(openTabTemp);
        }

        [System.Serializable]
        private sealed class TabAndButton {
            [SerializeField]
            internal Tab Tab;
            [SerializeField]
            internal TabButton Button;

            internal void Init() {
                Button.Init();
            }

            internal void ShowTab() {
                Tab.ShowTab();
                Button.ShowTab();
            }
            internal void ShowTabForce() {
                Tab.ShowForce();
                Button.ShowTab();
            }

            internal void ShowTabFiction() {
                Tab.ShowTabFiction();
                //Button.ShowTab();
            }

            internal void HideTab() {
                Tab.HideTab();
                Button.HideTab();
            }

            internal void HideTabForce() {
                Tab.HideForce();
                Button.HideTab();
            }

            internal void ShowTabButton() {
                Button.ShowTab();
            }

            internal void HideTabButton() {
                Button.HideTab();
            }

            internal void Move(int indexMove) {
                Tab.Move(indexMove);
            }
            internal void MoveImmediately(int indexMove) {
                Tab.MoveImmediately(indexMove);
            }

            internal void BeginDrag() {
                Tab.BeginDrag();
            }

            internal void Drag(Vector2 offsetPosition) {
                Tab.Drag(offsetPosition);
            }

            internal void EndDrag() {
                Tab.EndDrag();
            }
        }
    }
}
