using Common;
using UnityEngine;
using Zenject;

namespace Menu {
    public class SettingTab : Tab {
        //[SerializeField]
        //private LanguageSettingItem languageSettingItem;

        //[Inject]
        //private ProfileManager profileManager;
        //[Inject]
        //private LanguageDataSet languageDataSet;

        //private LanguageWindow languageWindow;

        protected override void Init() {
            //languageWindow = FindObjectOfType<LanguageWindow>();

            //languageSettingItem.Init();
            //languageSettingItem.OnClickItem += LanguageClick;

            //Debug.Log("SettingsTab  Init");
            //UpdateLanguageData(profileManager.GetProfileLanguage());
        }

        private void LanguageClick() {
            //languageWindow.Show();
        }

        //internal void UpdateLanguageData(Language language) {
        //    //LanguageData languageData = languageDataSet.GetLanguageDataByLanguage(language);
        //    //languageSettingItem.SetLanguageIcon(languageData.languageIcon);
        //    //languageSettingItem.SetTextById(languageData.localizationLanguageId);
        //}
        //internal void OnChangeLanguage(Language language) {
        //    UpdateLanguageData(language);
        //}


        protected override void ApplyStartShowTab() {
        }
        protected override void ApplyEndShowTab() {
        }

        protected override void ApplyStartActionHideTab() {
        }
        protected override void ApplyEndActionHideTab() {
        }

        private void MoreFramesClick() {
            //shopFramesWindow.StartShow();
        }
        private void MoreStickersClick() {
            //shopStickersWindow.StartShow();
        }

        private void OpneChallenge() {
            //ChallengeServ challengeServ = servController.GetFirstChallengeServ();
            //
            //if( challengeServ == null ) {
            //    return;
            //}
            //challengeEntreyInfoWindow.StartShow( challengeServ );
        }
    }
}
