using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Common;
using UnityEngine.UI;
using System;

namespace Menu {

    public class ShopTab : Tab {
        [SerializeField]
        private SkinItem skinItemPrf;
        [SerializeField]
        private ScrollRect skinScrollRect;
        [SerializeField]
        private Button overlayButton;

        [Inject]
        private SkinMenuDataSet skinMenuDataSet;
        [Inject]
        private Profile profile;
        [Inject]
        private InfoUI infoUI;

        private Image skinScrollRectImage;
        private Image viewportImage;

        private SkinItem[] skinItems;
        private int currentSkinIndex;

        private int currentSkinIndexClick;

        private bool isActivateSkinsButton;

        protected override void Init() {
            currentSkinIndex = -1;
            currentSkinIndexClick = -1;

            GenerateSkinItems();

            isActivateSkinsButton = true;

            overlayButton.onClick.AddListener(ClickOverlaButton);

            skinScrollRectImage = skinScrollRect.GetComponent<Image>();
            viewportImage = skinScrollRect.viewport.GetComponent<Image>();

            ActivateScrollRect();
        }

        private void ClickOverlaButton() {
            ActivateScrollRect();
        }

        protected override void ApplyStartShowTab() {
        }
        protected override void ApplyEndShowTab() {
            ActivateScrollRect();
        }

        protected override void ApplyStartActionHideTab() {
        }
        protected override void ApplyEndActionHideTab() {
        }

        private void GenerateSkinItems() {
            skinItems = new SkinItem[skinMenuDataSet.SkinDatas.Length];

            for (int i = 0; i < skinMenuDataSet.SkinDatas.Length; i++) {
                skinItems[i] = Instantiate(skinItemPrf);
                skinItems[i].MyRectTransform.SetParent(skinScrollRect.content, false);
                skinItems[i].Init(i, skinMenuDataSet.SkinDatas[i]);
                skinItems[i].OnClickClothe += OnClickClothe;

                if (profile.availableSkins.Contains(skinMenuDataSet.SkinDatas[i].skinType)) {
                    skinItems[i].SetActive();
                }

                if (profile.currentSkin == skinMenuDataSet.SkinDatas[i].skinType) {
                    skinItems[i].ChooseSkin();
                    currentSkinIndex = i;
                }
                else {
                    skinItems[i].UnchooseSkin();
                }
            }

            int raw = skinMenuDataSet.SkinDatas.Length / 2;

            GridLayoutGroup gridLayoutGroup = skinScrollRect.content.GetComponent<GridLayoutGroup>();
            skinScrollRect.content.sizeDelta = new Vector2(
                skinScrollRect.content.sizeDelta.x, (gridLayoutGroup.spacing.y + gridLayoutGroup.cellSize.y) * (raw + 1));
        }

        private void OnClickClothe(int skinIndex) {

            if (currentSkinIndex == skinIndex) {
                return;
            }

            if (skinMenuDataSet.SkinDatas[skinIndex].skinAvailableType == SkinAvailableType.Available ||
                profile.availableSkins.Contains(skinMenuDataSet.SkinDatas[skinIndex].skinType)) {
                ClotheSkin(skinIndex);
                return;
            }
            else {
                if (skinMenuDataSet.SkinDatas[skinIndex].skinAvailableType == SkinAvailableType.ByCoin) {
                    BuyByCoins(skinIndex);
                    return;
                }
                else if (skinMenuDataSet.SkinDatas[skinIndex].skinAvailableType == SkinAvailableType.ByVideo) {
                    currentSkinIndexClick = -1;

                    ByVideo(skinIndex);
                }
            }
        }

        private void BuyByCoins(int skinIndex) {
            if (currentSkinIndexClick == -1) {
                currentSkinIndexClick = skinIndex;

                //���������� ���� ����������
                infoUI.ShowInfoMousePosition("������ �� ������", 120f);

                DiactivateScrollRect();
                DiactivateAllSkinItems(currentSkinIndexClick);
            }
            else if (currentSkinIndexClick == skinIndex) {
                //�������
                Debug.Log("�������");
                profile.AddSkin(skinMenuDataSet.SkinDatas[skinIndex].skinType);
                ActivateScrollRect();
                skinItems[skinIndex].SetActive();
                ClotheSkin(skinIndex);
            }
            else {
                ActivateScrollRect();
            }
        }

        private void ByVideo(int skinIndex) {
            profile.AddSkin(skinMenuDataSet.SkinDatas[skinIndex].skinType);
            skinItems[skinIndex].SetActive();
            ClotheSkin(skinIndex);
        }

        private void ActivateScrollRect() {
            skinScrollRectImage.enabled = true;
            viewportImage.enabled = true;

            overlayButton.gameObject.SetActive(false);

            currentSkinIndexClick = -1;

            ActivateAllSkinItems();
        }

        private void ActivateAllSkinItems() {
            if (isActivateSkinsButton) {
                return;
            }

            isActivateSkinsButton = true;

            for (int i = 0; i < skinItems.Length; i++) {
                skinItems[i].ActivateButton();
            }
        }

        private void DiactivateScrollRect() {
            skinScrollRectImage.enabled = false;
            viewportImage.enabled = false;

            overlayButton.gameObject.SetActive(true);
        }

        private void DiactivateAllSkinItems(int skinIndexesExclude = -1) {
            isActivateSkinsButton = false;

            for (int i = 0; i < skinItems.Length; i++) {
                if (skinIndexesExclude == i) {
                    continue;
                }
                skinItems[i].DiactivateButton();
            }
        }

        private void ClotheSkin(int skinIndex) {
            skinItems[currentSkinIndex].UnchooseSkin();

            currentSkinIndex = skinIndex;
            currentSkinIndexClick = -1;

            skinItems[currentSkinIndex].ChooseSkin();
        }
    }
}