using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Menu {
    [RequireComponent(typeof(Button))]
    public class TabButton : MonoBehaviour {
        [SerializeField]
        internal Button tabButton;
        [SerializeField]
        Image emptyIconImage;
        [SerializeField]
        Image fillIconImage;
        [SerializeField]
        Animator downButtonAnim;

        RectTransform rectTransformButton;
        Color colorButton;
        RectTransform rectTransformBackButton;
        RectTransform rectTransformButtonIcon;
        float startSizeXButton;
        float endSizeXButton;
        float endPosIcon = 30f;
        float startSizeDeltaIcon = 90f;
        float endSizeDeltaIcon = 120f;
        float timeAnimation = 0.3f;
        Vector2 startSizeDelta;

        internal void Init() {
            rectTransformButton = tabButton.GetComponent<RectTransform>();
            rectTransformButtonIcon = emptyIconImage.GetComponent<RectTransform>();
            startSizeXButton = rectTransformButton.sizeDelta.x;
            endSizeXButton = 300f;
            endPosIcon = 30f;
            timeAnimation = 0.1f;
            startSizeDelta = rectTransformButton.sizeDelta;
        }

        internal void ShowTab() {
            //rectTransformButtonIcon.DOAnchorPosY( endPosIcon, timeAnimation );
            //rectTransformButtonIcon.DOSizeDelta( new Vector2( endSizeDeltaIcon, endSizeDeltaIcon ), timeAnimation );
            //rectTransformButton.DOSizeDelta( new Vector2( endSizeXButton, startSizeDelta.y ), timeAnimation );
            //downButtonAnim.SetTrigger( "show" );

            emptyIconImage.gameObject.SetActive(false);
            fillIconImage.gameObject.SetActive(true);
        }

        internal void HideTab() {
            //rectTransformButtonIcon.DOAnchorPosY( 0, timeAnimation );
            //rectTransformButtonIcon.DOSizeDelta( new Vector2( startSizeDeltaIcon, startSizeDeltaIcon ), timeAnimation );
            //rectTransformButton.DOSizeDelta( new Vector2( startSizeXButton, startSizeDelta.y ), timeAnimation );

            emptyIconImage.gameObject.SetActive(true);
            fillIconImage.gameObject.SetActive(false);
        }

        internal void ActivateButton() {
            gameObject.SetActive(true);
        }

        internal void DiactivateButton() {
            gameObject.SetActive(false);
        }
    }
}
