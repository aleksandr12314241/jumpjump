using System;
using UnityEngine;
using DG.Tweening;

namespace Menu {
    public abstract class Tab : MonoBehaviour {
        internal event Action<int> OnOpenMe = (int indexTab) => { };

        [SerializeField]
        RectTransform container;

        RectTransform myRectTransform;

        Vector2 startDragPosition;

        internal bool IsShown {
            private set;
            get;
        }

        internal void InitBase(int index) {
            myRectTransform = GetComponent<RectTransform>();
            myRectTransform.anchoredPosition = new Vector2(myRectTransform.rect.width * index, myRectTransform.anchoredPosition.y);

            Init();
        }

        protected abstract void Init();

        internal void ShowForce() {
            IsShown = true;
            gameObject.SetActive(true);

            //myRectTransform.anchoredPosition = new Vector2( -indexMove * myRectTransform.rect.width, myRectTransform.anchoredPosition.y );
        }

        internal void ShowTabFiction() {
            gameObject.SetActive(true);
        }

        internal void ShowTab() {
            if (IsShown) {
                return;
            }

            IsShown = true;
            gameObject.SetActive(true);

            ApplyEndShowTab();
        }

        protected abstract void ApplyStartShowTab();
        protected abstract void ApplyEndShowTab();

        internal void HideForce() {
            IsShown = false;
            gameObject.SetActive(false);
        }

        internal void HideTab() {
            if (!IsShown) {
                return;
            }

            IsShown = false;
            gameObject.SetActive(false);

            ApplyEndActionHideTab();
        }

        protected abstract void ApplyStartActionHideTab();
        protected abstract void ApplyEndActionHideTab();

        internal void Move(int indexMove) {
            myRectTransform.DOKill();

            if (indexMove == 0) {
                ApplyStartShowTab();
            }

            myRectTransform.DOAnchorPosX(-indexMove * myRectTransform.rect.width, 0.3f).OnComplete(() => {
                if (indexMove == 0) {
                    ShowTab();
                }
                else {
                    HideForce();
                }
            });
        }
        internal void MoveImmediately(int indexMove) {
            myRectTransform.anchoredPosition = new Vector2(-indexMove * myRectTransform.rect.width, myRectTransform.anchoredPosition.y);
        }

        internal void BeginDrag() {
            myRectTransform.DOKill();

            startDragPosition = myRectTransform.anchoredPosition;
        }

        internal void Drag(Vector2 offsetPosition) {
            myRectTransform.anchoredPosition = startDragPosition - new Vector2(offsetPosition.x, 0f);
        }

        internal void EndDrag() {
            myRectTransform.DOKill();

            myRectTransform.DOAnchorPosX(startDragPosition.x, 0.3f);
        }
    }
}
