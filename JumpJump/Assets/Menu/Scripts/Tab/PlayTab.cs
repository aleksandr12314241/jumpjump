using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UnityEngine.SceneManagement;
using Common;

namespace Menu {
    public class PlayTab : Tab {
        [SerializeField]
        private MenuLevelItem menuLevelItemPrf;
        [SerializeField]
        private ScrollRect levelsScrollRect;

        [Inject]
        private SequenceLevelsDataSet sequenceLevelsDataSet;
        [Inject]
        private LevelChooseService levelChooseService;
        [Inject]
        private ZenjectSceneLoader zenjectSceneLoader;
        [Inject]
        private Profile profile;

        private MenuLevelItem[] menuLevelItemList;
        private GridLayoutGroup gridLayoutGroup;

        protected override void Init() {

            menuLevelItemList = new MenuLevelItem[sequenceLevelsDataSet.Levels.Length];
            float startPositionY = -(menuLevelItemPrf.MyRectTransform.rect.height + 10f);
            float stepY = menuLevelItemPrf.MyRectTransform.rect.height + 15f;

            const float additionalHeightY = 30f;
            levelsScrollRect.content.sizeDelta = new Vector2(levelsScrollRect.content.sizeDelta.x, -startPositionY + (stepY * sequenceLevelsDataSet.Levels.Length) + additionalHeightY);

            for (int i = 0; i < sequenceLevelsDataSet.Levels.Length; i++) {
                menuLevelItemList[i] = Instantiate(menuLevelItemPrf);
                menuLevelItemList[i].MyRectTransform.SetParent(levelsScrollRect.content, false);
                menuLevelItemList[i].MyRectTransform.anchoredPosition = new Vector2(0, startPositionY - (stepY * i));
                menuLevelItemList[i].Init(i, sequenceLevelsDataSet.Levels[i]);
                menuLevelItemList[i].OnClickTryPlay += OnClickTryPlay;

                menuLevelItemList[i].SetActive(profile.level >= i);
            }

            int raw = sequenceLevelsDataSet.Levels.Length / 2;

            gridLayoutGroup = levelsScrollRect.content.GetComponent<GridLayoutGroup>();
            levelsScrollRect.content.sizeDelta = new Vector2(
                levelsScrollRect.content.sizeDelta.x, (gridLayoutGroup.spacing.y + gridLayoutGroup.cellSize.y) * (raw + 1));

            levelsScrollRect.content.anchoredPosition = new Vector2(levelsScrollRect.content.anchoredPosition.x, GetCategoryScrollPosition());
        }

        private float GetCategoryScrollPosition() {
            //string language = LocalizationManager.GetLanguageString();
            //ProgressByLanguage progressByLanguage = _GC.player.GetProgressByLanguage(language);

            float dividerProgress = profile.level / (float)2;

            float raw = Mathf.Ceil(dividerProgress);
            float lastDoneQuest = (gridLayoutGroup.cellSize.y) * (raw + 1);

            float scrollPosition = 0f;
            if (lastDoneQuest >= 2000) {
                scrollPosition = lastDoneQuest - (gridLayoutGroup.cellSize.y);
            }

            return scrollPosition;
        }

        protected override void ApplyStartShowTab() {
        }

        protected override void ApplyEndActionHideTab() {
        }

        protected override void ApplyStartActionHideTab() {
        }
        protected override void ApplyEndShowTab() {
        }

        private void OnClickTryPlay(int indexLevel) {
            if (indexLevel > profile.level) {
                //TODO: ������� ���� ��� ������� ����������
                Debug.Log("����, ��� ���������� ����");
                return;
            }

            levelChooseService.SetCurrentLevelIndex(indexLevel);

            zenjectSceneLoader.LoadScene("Level", LoadSceneMode.Single);
        }
    }
}