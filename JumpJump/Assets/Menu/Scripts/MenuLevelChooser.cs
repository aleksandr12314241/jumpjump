using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Common;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

namespace Menu {
    [RequireComponent(typeof(ScrollRect))]
    public class MenuLevelChooser : MonoBehaviour {
        [SerializeField]
        private MenuLevelItem menuLevelItemPrf;

        [Inject]
        private SequenceLevelsDataSet sequenceLevelsDataSet;
        [Inject]
        private LevelChooseService levelChooseService;
        [Inject]
        private ZenjectSceneLoader zenjectSceneLoader;
        [Inject]
        private Profile profile;

        private ScrollRect levelsScrollRect;
        private MenuLevelItem[] menuLevelItemList;

        private void Start() {
            levelsScrollRect = GetComponent<ScrollRect>();

            menuLevelItemList = new MenuLevelItem[sequenceLevelsDataSet.Levels.Length];
            float startPositionY = -(menuLevelItemPrf.MyRectTransform.rect.height + 10f);
            float stepY = menuLevelItemPrf.MyRectTransform.rect.height + 15f;

            const float additionalHeightY = 30f;
            levelsScrollRect.content.sizeDelta = new Vector2(levelsScrollRect.content.sizeDelta.x, -startPositionY + (stepY * sequenceLevelsDataSet.Levels.Length) + additionalHeightY);

            for (int i = 0; i < sequenceLevelsDataSet.Levels.Length; i++) {
                menuLevelItemList[i] = Instantiate(menuLevelItemPrf);
                menuLevelItemList[i].MyRectTransform.SetParent(levelsScrollRect.content, false);
                menuLevelItemList[i].MyRectTransform.anchoredPosition = new Vector2(0, startPositionY - (stepY * i));
                menuLevelItemList[i].Init(i, sequenceLevelsDataSet.Levels[i]);
                menuLevelItemList[i].OnClickTryPlay += OnClickTryPlay;

                menuLevelItemList[i].SetActive(profile.level >= i);
            }
        }

        private void OnClickTryPlay(int indexLevel) {
            if (indexLevel > profile.level) {
                //TODO: ������� ���� ��� ������� ����������
                Debug.Log("����, ��� ���������� ����");
                return;
            }

            levelChooseService.SetCurrentLevelIndex(indexLevel);

            zenjectSceneLoader.LoadScene("Level", LoadSceneMode.Single);
        }
    }
}