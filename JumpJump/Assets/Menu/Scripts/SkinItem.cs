using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Common;

namespace Menu {
    [RequireComponent(typeof(Button))]
    public class SkinItem : MonoBehaviour {
        internal Action<int> OnClickClothe = (int arg) => { };

        [SerializeField]
        private RectTransform myRectTransform;
        [SerializeField]
        private Image chooseImage;
        [SerializeField]
        private Button myButton;
        [SerializeField]
        private GameObject coinGO;
        [SerializeField]
        private Text coinCountText;
        [SerializeField]
        private GameObject videoGO;

        private int skinIndex;
        private SkinType skinType;

        public RectTransform MyRectTransform {
            get => myRectTransform;
        }

        internal void Init(int skinIndex, SkinMenuDataSet.SkinData skinData) {
            myButton.onClick.AddListener(ClickClothe);

            this.skinIndex = skinIndex;
            skinType = skinData.skinType;

            if (skinData.skinAvailableType == SkinAvailableType.Available) {
                SetActive();
            }
            else if (skinData.skinAvailableType == SkinAvailableType.ByCoin) {
                coinGO.SetActive(true);
                videoGO.SetActive(false);

                coinCountText.text = skinData.coinCount.ToString();
            }
            else if (skinData.skinAvailableType == SkinAvailableType.ByVideo) {
                coinGO.SetActive(false);
                videoGO.SetActive(true);
            }

            UnchooseSkin();
        }

        internal void ChooseSkin() {
            chooseImage.gameObject.SetActive(true);
        }
        internal void UnchooseSkin() {
            chooseImage.gameObject.SetActive(false);
        }

        internal void SetActive() {
            coinGO.SetActive(false);
            videoGO.SetActive(false);
        }

        private void ClickClothe() {
            OnClickClothe(skinIndex);
        }

        internal void ActivateButton() {
            myButton.enabled = true;
        }

        internal void DiactivateButton() {
            myButton.enabled = false;
        }
    }
}