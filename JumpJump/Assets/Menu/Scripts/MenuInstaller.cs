using UnityEngine;
using Zenject;
using Common;
using UnityEngine.UI;
using System;
using Common;

namespace Menu {

    public class MenuInstaller : MonoInstaller {
        [Inject]
        private PrefabsDataSet prefabsDataSet;

        public override void InstallBindings() {
            InstalObjects();

            InstalObjectsPool();

            InitSignals();
        }

        private void InstalObjects() {
            Container.Bind<MenuController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<SettingTab>().FromComponentInHierarchy().AsSingle();
            Container.BindInterfacesAndSelfTo<SettingPanel>().FromComponentInHierarchy().AsSingle();
            Container.Bind<MenuLevelChooser>().FromComponentInHierarchy().AsSingle();
            Container.Bind<PlayTab>().FromComponentInHierarchy().AsSingle();
            Container.Bind<ShopTab>().FromComponentInHierarchy().AsSingle();
            Container.Bind<InfoUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<UpMenuUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<CoinsFlyController>().FromComponentInHierarchy().AsSingle();
        }

        private void InstalObjectsPool() {
            Container.BindFactory<Vector2, Vector2, int, CoinFly, CoinFly.Factory>()
                .FromPoolableMemoryPool<Vector2, Vector2, int, CoinFly, CoinFly.CoinPool>(poolBinder => poolBinder
                .WithInitialSize(20)
                .FromComponentInNewPrefab(prefabsDataSet.coinFlyPrf)
                .UnderTransform(FindObjectOfType<CoinsFlyController>().transform));
        }

        private void InitSignals() {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<OnChangeSoundOn>();
            Container.BindSignal<OnChangeSoundOn>().ToMethod<Profile>((x, s) => x.OnChangeSoundOn(s.soundOn)).FromResolve();
            Container.BindSignal<OnChangeSoundOn>().ToMethod<SettingPanel>((x, s) => x.OnChangeSoundOn(s.soundOn)).FromResolve();
        }
    }
}