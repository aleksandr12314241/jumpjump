using UnityEngine;
using Common;

namespace Menu {
    [CreateAssetMenu(fileName = "SkinMenuDataSet", menuName = "JumpJump/Menu/SkinMenuDataSet", order = 1)]
    public class SkinMenuDataSet : ScriptableObject {
        [SerializeField]
        private SkinData[] skinDatas;

        public SkinData[] SkinDatas {
            get => skinDatas;
        }

        [System.Serializable]
        public class SkinData {
            [SerializeField]
            internal SkinType skinType;
            [SerializeField]
            internal Sprite image;
            [SerializeField]
            internal SkinAvailableType skinAvailableType;
            [SerializeField]
            internal int coinCount = 10;
        }
    }
}