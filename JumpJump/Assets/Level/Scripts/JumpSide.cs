namespace Gameplay {

    public enum JumpSide {
        Left = 0,
        Right = 1,
        Portal = 2,
        LeftBack = 3,
        RightBack = 4,
        None = 5,
        ToCell = 6
    }
}