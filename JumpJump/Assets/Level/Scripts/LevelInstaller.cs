using Zenject;
using UnityEngine;
using Gameplay.UI;
using System;
using Common;

namespace Gameplay {
    public class LevelInstaller : MonoInstaller {
        [SerializeField]
        protected Transform fieldTransform;
        [Inject]
        private LevelPrefabsDataSet levelPrefabsDataSet;
        [Inject]
        private PrefabsDataSet prefabsDataSet;

        public override void InstallBindings() {
            Container.Bind<LevelController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<LevelGenerateController>().FromComponentInHierarchy().AsSingle();
            Container.Bind<PlayerControlUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<FollowCamera>().FromComponentInHierarchy().AsSingle();
            Container.Bind<LoseWindowUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<WinWindowUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<FollowCameraUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<UpMenuUI>().FromComponentInHierarchy().AsSingle();
            Container.Bind<CoinsFlyController>().FromComponentInHierarchy().AsSingle();

            //InitTutoarial();

            InstalObjectsPool();

            InitSignals();
        }

        private void InstalObjectsPool() {
            Container.BindFactory<Vector2, Vector2, int, CoinFly, CoinFly.Factory>()
                .FromPoolableMemoryPool<Vector2, Vector2, int, CoinFly, CoinFly.CoinPool>(poolBinder => poolBinder
                .WithInitialSize(20)
                .FromComponentInNewPrefab(prefabsDataSet.coinFlyPrf)
                .UnderTransform(FindObjectOfType<CoinsFlyController>().transform));

            InstalObjectsPoolCells();

            InstalObjectsPoolEnemies();
        }

        private void InstalObjectsPoolCells() {
            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellCommon, Cell.FactoryCommon>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellCommon, Cell.CellCommonPool>(poolBinder => poolBinder
                .WithInitialSize(50)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellEmptyPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellCommon, Cell.FactoryEnd>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellCommon, Cell.CellCommonPoolEnd>(poolBinder => poolBinder
                .WithInitialSize(10)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellEndPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSpike, Cell.FactorySpike>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSpike, Cell.CellSpikePool>(poolBinder => poolBinder
                .WithInitialSize(20)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSpikePrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellDestroyedOnStep, Cell.FactoryCellDestroyedOnStep>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellDestroyedOnStep, Cell.CellDestroyedOnStepPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellDestroyedOnStepPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellPortal, Cell.FactoryCellPortal>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellPortal, Cell.CellPortalPool>(poolBinder => poolBinder
                .WithInitialSize(10)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellPortalPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.FactoryCellSlideLeftDown>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.CellSlideLeftDownPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSlideLeftDownPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.FactoryCellSlideRightDown>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.CellSlideRightDownPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSlideRightDownPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.FactoryCellSlideLeft>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.CellSlideLeftPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSlideLeftPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.FactoryCellSlideRight>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSlide, Cell.CellSlideRightPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSlideRightPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellEmptyMoving, Cell.FactoryCellEmptyMoving>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellEmptyMoving, Cell.CellEmptyMovingPool>(poolBinder => poolBinder
                .WithInitialSize(20)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellEmptyMovingPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, CellType, CellType, AboveCellData, CellSpring, Cell.FactoryCellSpring>()
                .FromPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, CellSpring, Cell.CellSpringPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.cellSpringPrf)
                .UnderTransform(fieldTransform));
        }

        private void InstalObjectsPoolEnemies() {
            Container.BindFactory<Vector2Int, AboveCellData, Enemy_0, Enemy.Enemy_0Factory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, Enemy_0, Enemy.Enemy_0Pool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.enemy_0Prf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, WallMovingEnemy, Enemy.WallMovingFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, WallMovingEnemy, Enemy.WallMovingPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.wallMovingEnemyPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, EnemyShooter, Enemy.EnemyShooterFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, EnemyShooter, Enemy.EnemyShooterPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(levelPrefabsDataSet.enemyShooterPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, Vector2Int, int, float, Bullet, Bullet.BulletFactory>()
                .FromPoolableMemoryPool<Vector2Int, Vector2Int, int, float, Bullet, Bullet.BulletPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.bulletPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, EnemyTwisting, Enemy.EnemyTwistingFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, EnemyTwisting, Enemy.EnemyTwistingPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.enemyTwistingPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, EnemySpiked, Enemy.EnemySpikedFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, EnemySpiked, Enemy.EnemySpikedPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.enemySpikedPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, WallPushing, Enemy.WallPushingFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, WallPushing, Enemy.WallPushingPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.wallPushingPrf)
                .UnderTransform(fieldTransform));

            Container.BindFactory<Vector2Int, AboveCellData, Coin, Enemy.CoinFactory>()
                .FromPoolableMemoryPool<Vector2Int, AboveCellData, Coin, Enemy.CoinPool>(poolBinder => poolBinder
                .WithInitialSize(15)
                .FromComponentInNewPrefab(levelPrefabsDataSet.coinPrf)
                .UnderTransform(fieldTransform));
        }

        private void InitSignals() {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<OnLoseLevel>();
            Container.BindSignal<OnLoseLevel>().ToMethod<LoseWindowUI>((x, s) => x.Show()).FromResolve();
            Container.BindSignal<OnLoseLevel>().ToMethod<Tutorial.Level.TutorialLevel>((x, s) => x.OnLoseLevel()).FromResolve();

            Container.DeclareSignal<OnWinLevel>();
            Container.BindSignal<OnWinLevel>().ToMethod<WinWindowUI>((x, s) => x.Show()).FromResolve();
            Container.BindSignal<OnWinLevel>().ToMethod<Tutorial.Level.TutorialLevel>((x, s) => x.OnWinLevel()).FromResolve();

            Container.DeclareSignal<OnContinueLevel>();
            Container.BindSignal<OnContinueLevel>().ToMethod<LevelController>((x, s) => x.ContinuePlayLevelAsGhost()).FromResolve();

            Container.DeclareSignal<OnStartNextLevel>();
            Container.BindSignal<OnStartNextLevel>().ToMethod<LevelGenerateController>((x, s) => x.GenerateNextLevel()).FromResolve();

            Container.DeclareSignal<OnRestartLevel>();
            Container.BindSignal<OnRestartLevel>().ToMethod<LevelController>((x, s) => x.Restart()).FromResolve();

            Container.DeclareSignal<OnStartPlayerControl>();
            Container.BindSignal<OnStartPlayerControl>().ToMethod<LevelController>((x, s) => x.StartPlayerControl()).FromResolve();

            Container.DeclareSignal<OnCollectCoinLevel>();
            Container.BindSignal<OnCollectCoinLevel>().ToMethod<CoinsFlyController>((x, s) => x.AddCoins(s.collectedCoinsValue, s.fieldPosition)).FromResolve();
            //Container.BindSignal<OnCollectCoinLevel>().ToMethod<UpMenuUI>((x, s) => x.CollectCoinLevel(s.collectedCoinsValue)).FromResolve();

            Container.DeclareSignal<OnMovePlayer>();
            Container.BindSignal<OnMovePlayer>().ToMethod<FollowCameraUI>((x, s) => x.OnMovePlayer()).FromResolve();
            Container.BindSignal<OnMovePlayer>().ToMethod<FollowCamera>((x, s) => x.OnMovePlayer()).FromResolve();
        }
    }
}