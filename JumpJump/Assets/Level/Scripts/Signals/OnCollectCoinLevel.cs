using UnityEngine;

namespace Gameplay {
    public class OnCollectCoinLevel {
        internal int collectedCoinsValue;
        internal Vector3 fieldPosition;
    }
}