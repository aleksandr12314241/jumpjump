using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TestAnimModels : MonoBehaviour {
    //[SerializeField]
    //private Slider sl;
    [SerializeField]
    private SkinnedMeshRenderer smr;
    [SerializeField]
    private Button showButton;
    [SerializeField]
    private Button hideButton;
    [SerializeField]
    private Button stopButton;
    [SerializeField]
    private Transform cubeTransform;

    DG.Tweening.Core.DOGetter<float> getter;
    private Tween skinnedMeshRendererTween;

    void Start() {
        //sl.onValueChanged.AddListener(delegate
        //{
        //    ValueChangeCheck();
        //});

        showButton.onClick.AddListener(Show);
        hideButton.onClick.AddListener(Hide);
        stopButton.onClick.AddListener(Stop);
    }

    //public void ValueChangeCheck() {
    //    smr.SetBlendShapeWeight(0, sl.value * 100);
    //}

    private void Show() {
        skinnedMeshRendererTween.Kill();

        skinnedMeshRendererTween = DOTween.To(() => smr.GetBlendShapeWeight(0), x => smr.SetBlendShapeWeight(0, x), 100f, 5f);

        cubeTransform.DOMoveX(2f, 5f);
    }

    private void Hide() {
        skinnedMeshRendererTween.Kill();

        skinnedMeshRendererTween = DOTween.To(() => smr.GetBlendShapeWeight(0), x => smr.SetBlendShapeWeight(0, x), 0f, 2f);

        cubeTransform.DOMoveX(-2f, 2f);
    }

    private void Stop() {
        //DOTween.KillAll();

        skinnedMeshRendererTween.Kill();
        //DOTween.Kill(skinnedMeshRendererTween.target);
        //DOTween.Kill(smr);
        //DOTween.Kill(skinnedMeshRendererTween);

    }
}
