using System;
using UnityEngine;
using UnityEngine.UI;
//using Tools.LevelEditor;
using Zenject;
using UnityEngine.EventSystems;

namespace Gameplay.UI {
    public class PlayerControlUI : MonoBehaviour {
        //[SerializeField]
        //private Button jumpLeftButton;
        [SerializeField]
        private EventTrigger jumpLeftET;
        //[SerializeField]
        //private Button jumpRightButton;
        [SerializeField]
        private EventTrigger jumpRightET;
        [SerializeField]
        private Player player;

        private bool isPlay;

        [Inject]
        private SignalBus signalBus;

        internal void Init(Player player) {
            this.player = player;

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) => {
                JumpLeft();
            });
            jumpLeftET.triggers.Add(entry);
            //jumpLeftButton.onClick.AddListener(JumpLeft);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener((data) => {
                JumpRight();
            });
            jumpRightET.triggers.Add(entry);
            //jumpRightButton.onClick.AddListener(JumpRight);
        }

#if UNITY_EDITOR
        private void Update() {
            if (Input.GetKey(KeyCode.A)) {
                JumpLeft();
            }
            else if (Input.GetKey(KeyCode.D)) {
                JumpRight();
            }
        }
#endif
        private void JumpLeft() {
            if (!isPlay) {
                return;
            }
            player.TryJumpLeft();

            signalBus.Fire(new OnMovePlayer() {
            });
        }

        private void JumpRight() {
            if (!isPlay) {
                return;
            }

            player.TryJumpRight();

            signalBus.Fire(new OnMovePlayer() {
            });
        }

        internal void SetIsPlay(bool isPlay) {
            this.isPlay = isPlay;
        }
    }
}