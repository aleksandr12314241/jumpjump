using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UnityEngine.SceneManagement;

namespace Gameplay.UI {
    public class BackButtonUI : MonoBehaviour {
        [SerializeField]
        private Button backButton;

        [Inject]
        private ZenjectSceneLoader zenjectSceneLoader;

        // Start is called before the first frame update
        private void Start() {
            backButton.onClick.AddListener(GoToMenu);
        }

        private void GoToMenu() {
            zenjectSceneLoader.LoadScene("Menu", LoadSceneMode.Single);
        }
    }
}