using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gameplay.UI {
    public class RestartUI : MonoBehaviour {
        [SerializeField]
        private Button restartButton;

        [Inject]
        private SignalBus signalBus;

        private void Start() {
            restartButton.onClick.AddListener( Restart );
        }

        private void Restart() {
            signalBus.Fire( new OnRestartLevel() {
            } );
        }
    }
}