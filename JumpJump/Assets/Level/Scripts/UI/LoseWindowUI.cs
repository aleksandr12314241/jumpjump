using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;

namespace Gameplay.UI {

    public class LoseWindowUI : MonoBehaviour {
        [SerializeField]
        private Button closeButton;
        [SerializeField]
        private RectTransform container;
        [SerializeField]
        private Button restartButton;
        [SerializeField]
        private Button continueButton;

        [Inject]
        private SignalBus signalBus;

        private void Start() {
            //closeButton.onClick.AddListener(Hide);
            restartButton.onClick.AddListener(Restart);
            continueButton.onClick.AddListener(Continue);

            Hide();
        }

        internal void Show() {
            container.gameObject.SetActive(true);
            closeButton.gameObject.SetActive(true);

            restartButton.enabled = true;
            continueButton.enabled = true;

            container.DOKill();
            //container.DOAnchorPos(Vector2.zero, 1f).OnComplete(Hide);
        }

        private void Hide() {
            container.DOKill();
            container.gameObject.SetActive(false);
            closeButton.gameObject.SetActive(false);

            restartButton.enabled = false;
            continueButton.enabled = false;
        }

        private void Restart() {
            Hide();

            signalBus.Fire(new OnRestartLevel() {
            });
        }

        private void Continue() {
            Hide();

            signalBus.Fire(new OnContinueLevel() {
            });
        }

        //public T OnComplete<T>( this T t, Action<T> action ) where T : LoseWindow {
        //    action.Invoke( t );
        //    return t;
        //}
    }
}