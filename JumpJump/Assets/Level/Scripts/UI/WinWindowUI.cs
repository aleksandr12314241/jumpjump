using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Common;

namespace Gameplay.UI {

    public class WinWindowUI : WindowMonoBehaviour {
        [SerializeField]
        private Button nextButton;
        [SerializeField]
        private RectTransform container;

        [Inject]
        private SignalBus signalBus;

        internal override void InitStart() {
            nextButton.onClick.AddListener( LoadNextLevel );

            InitStartBase();
            backButton.onClick.RemoveAllListeners();

            backButton.onClick.AddListener( LoadNextLevel );

            Hide();
        }

#if UNITY_EDITOR
        private void Update() {
            if ( Input.GetKey( KeyCode.Return ) ) {
                if ( IsShown ) {
                    LoadNextLevel();
                }
            }
        }
#endif

        internal override void Show() {
            base.Show();

            container.gameObject.SetActive( true );
            backButton.gameObject.SetActive( true );

            container.DOKill();
            container.DOAnchorPos( Vector2.zero, 1f );
        }

        internal override void Hide() {
            SetIsShown( false );

            container.DOKill();
            container.gameObject.SetActive( false );
            backButton.gameObject.SetActive( false );

            base.Hide();
        }

        private void LoadNextLevel() {
            container.DOKill();

            Hide();

            signalBus.Fire( new OnStartNextLevel() {
            } );
        }
    }
}