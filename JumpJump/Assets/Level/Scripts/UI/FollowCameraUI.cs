using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UnityEngine.EventSystems;
using System;

namespace Gameplay {
    public class FollowCameraUI : MonoBehaviour {
        internal Action OnStartDragCamera = () => { };

        [SerializeField]
        private EventTrigger startDragCameraET;
        [SerializeField]
        private Button startFollowButton;

        [Inject]
        private FollowCamera followCamera;

        private void Start() {
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.BeginDrag;
            entry.callback.AddListener((data) => {
                StartDragCamera((PointerEventData)data);
            });
            startDragCameraET.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drag;
            entry.callback.AddListener((data) => {
                StartDragCameraMove((PointerEventData)data);
            });

            startDragCameraET.triggers.Add(entry);

            startFollowButton.onClick.AddListener(StartFollow);

            startDragCameraET.gameObject.SetActive(true);
            startFollowButton.gameObject.SetActive(false);
        }

        internal void StopFollowCameraToPlayerByDrag() {
            startFollowButton.gameObject.SetActive(true);
        }

        private void StartDragCamera(PointerEventData data) {
            OnStartDragCamera();

            followCamera.StartDragCamera(data);

            StopFollowCameraToPlayerByDrag();
        }
        private void StartDragCameraMove(PointerEventData data) {
            followCamera.DragCamera(data);
        }

        private void StartFollow() {
            followCamera.StartFollow();

            startFollowButton.gameObject.SetActive(false);
        }

        internal void OnMovePlayer() {
            startFollowButton.gameObject.SetActive(false);
        }
    }
}