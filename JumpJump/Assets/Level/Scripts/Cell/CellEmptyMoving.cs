using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {

    public class CellEmptyMoving : Cell {
        internal Action<Vector2Int, Vector2Int, Vector2Int> OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };

        private CellMover cellMover;
        private Player player;

        protected override void Init(AboveCellData aboveCellData) {
            AboveCellDataEmptyMoving cellDataEmptyMoving = (AboveCellDataEmptyMoving)aboveCellData;

            cellMover = GetComponent<CellMover>();
            cellMover.Init(cellDataEmptyMoving.path, cellDataEmptyMoving.speed);
            cellMover.RemoveAllListeners();
            cellMover.OnChangeCellIndex += ChangeCellIndex;

            player = null;
        }

        protected override void StartPlayAction() {
            cellMover.StartMove();
        }

        protected override void Die() {
        }

        internal override void Step(Player player) {
            base.Step(player);

            player.transform.SetParent(this.transform);

            player.StepOnMovingCell();

            this.player = player;

            RemovePlayerListners();

            player.OnStartJump += OnStartJump;
            player.OnStartPush += OnStartPush;
            player.OnGoToPortal += OnGoToPortal;
        }

        internal void StepFrom() {
            if (player == null) {
                return;
            }

            player.StepFromMovingCell();
            RemovePlayerListners();

            player = null;
        }

        private void OnGoToPortal(Vector2Int arg1, Vector2Int arg2) {
            StepFrom();
        }

        private void OnStartPush(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            StepFrom();
        }

        private void OnStartJump(JumpSide arg1, Vector2Int arg2, Vector2Int arg3) {
            StepFrom();
        }

        private void RemovePlayerListners() {
            if (player == null) {
                return;
            }

            player.OnStartJump -= OnStartJump;
            player.OnStartPush -= OnStartPush;
            player.OnGoToPortal -= OnGoToPortal;
        }

        private void ChangeCellIndex(Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) {
            OnChangeCellIndex(newCellIndex, oldCellIndex, direction);

            if (player != null) {
                player.ChangeIndexByCellMoving(newCellIndex);
            }
        }
        internal void RemoveAllListeners() {
            OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };
        }
    }
}