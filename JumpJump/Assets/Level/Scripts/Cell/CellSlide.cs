using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {

    public class CellSlide : Cell {
        private Vector2Int direction;

        protected override void Init(AboveCellData aboveCellData) {
            AboveCellDataSlide aboveCellDataSlide = (AboveCellDataSlide)aboveCellData;

            direction = new Vector2Int(aboveCellDataSlide.directionX, aboveCellDataSlide.directionY);
        }

        protected override void StartPlayAction() {
        }

        protected override void Die() {
        }

        internal override void Step(Player player) {
            base.Step(player);

            player.Push(direction);
        }
    }
}