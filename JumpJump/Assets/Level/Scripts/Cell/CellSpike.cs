using UnityEngine;
using DG.Tweening;

namespace Gameplay {
    public class CellSpike : Cell {
        [SerializeField]
        private Transform spikeTransform;
        [SerializeField]
        private ColliderPlayerDetected colliderPlayerDetected;

        private SkinnedMeshRenderer mySkinnedMeshRenderer;
        private Tween skinnedMeshRendererTween;

        private float startDelay;
        private float frequency;

        private const float startPosSpike = -0.01f;
        private const float endPosSpike = 0.5f;

        private const float startPosSpikeBlendShape = 0f;
        private const float endPosSpikeBlendShape = 100f;

        protected override void Init(AboveCellData aboveCellData) {
            AboveCellDataSpike aboveCellDataSpike = (AboveCellDataSpike)aboveCellData;

            startDelay = aboveCellDataSpike.startDelay;
            frequency = aboveCellDataSpike.frequency;

            colliderPlayerDetected.OnDetectPlayer = (Player player) => { };
            colliderPlayerDetected.OnDetectPlayer += OnDetectPlayer;

            mySkinnedMeshRenderer = modelTransform.GetComponent<SkinnedMeshRenderer>();

            HideImmediately();
        }

        private void OnDetectPlayer(Player player) {
            player.Damage(CellType.Spike);
        }

        protected override void Die() {
            spikeTransform.DOKill();
        }

        protected override void StartPlayAction() {
            HideImmediately();

            FirstShow();
        }

        private void FirstShow() {
            spikeTransform.DOKill();

            spikeTransform.DOLocalMove(new Vector3(0f, startPosSpike, 0f), startDelay).OnComplete(ShowSpike);
        }

        internal void ShowSpike() {
            float animTimeShow = (frequency / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow / 4f).OnComplete(() => {
                colliderPlayerDetected.ActivateCollider();
            });

            spikeTransform.DOKill();
            spikeTransform.DOLocalMove(new Vector3(0f, endPosSpike, 0f), animTimeShow).OnComplete(WaitShowSpike);

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween = DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(0), x => mySkinnedMeshRenderer.SetBlendShapeWeight(0, x), endPosSpikeBlendShape, animTimeShow);
        }

        private void WaitShowSpike() {
            spikeTransform.DOKill();
            spikeTransform.DOLocalMove(new Vector3(0f, endPosSpike, 0f), (frequency / 2f)).OnComplete(HideSpike);

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween = DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(0), x => mySkinnedMeshRenderer.SetBlendShapeWeight(0, x), endPosSpikeBlendShape, (frequency / 2f));
        }

        private void HideSpike() {
            float animTimeShow = (frequency / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow - (animTimeShow / 4f)).OnComplete(() => {
                colliderPlayerDetected.DiActivateCollider();
            });

            spikeTransform.DOKill();
            spikeTransform.DOLocalMove(new Vector3(0f, startPosSpike, 0f), animTimeShow).OnComplete(WaitHideSpike);

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween = DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(0), x => mySkinnedMeshRenderer.SetBlendShapeWeight(0, x), startPosSpikeBlendShape, animTimeShow);
        }

        private void WaitHideSpike() {
            spikeTransform.DOKill();
            spikeTransform.DOLocalMove(new Vector3(0f, startPosSpike, 0f), (frequency / 2f)).OnComplete(ShowSpike);

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween = DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(0), x => mySkinnedMeshRenderer.SetBlendShapeWeight(0, x), startPosSpikeBlendShape, (frequency / 2f));
        }

        private void HideImmediately() {
            colliderPlayerDetected.DiActivateCollider();

            spikeTransform.DOKill();
            spikeTransform.localPosition = new Vector3(0f, startPosSpike, 0f);

            mySkinnedMeshRenderer.SetBlendShapeWeight(0, 0);
        }
    }
}