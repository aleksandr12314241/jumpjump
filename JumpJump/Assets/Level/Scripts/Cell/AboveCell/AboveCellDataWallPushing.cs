using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataWallPushing : AboveCellData {
        [SerializeField]
        internal float startDelay;
        [SerializeField]
        internal float frequency;
        [SerializeField]
        internal WallPushingDirectionType wallPushingDirectionType;

        internal AboveCellDataWallPushing(Vector2Int cellIndex) : base(cellIndex) {
            frequency = 1f;

            wallPushingDirectionType = WallPushingDirectionType.Right;
        }
        internal AboveCellDataWallPushing(Vector2Int cellIndex, float frequency, WallPushingDirectionType wallPushing) : base(cellIndex) {
            this.frequency = frequency;

            this.wallPushingDirectionType = wallPushing;
        }

        public static AboveCellDataWallPushing GetAboveCellDataWallPushing(AboveCellDataToSave aboveCellDataToSave) {
            float frequency = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("frequency")).value);

            string wallPushingStr = aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("wallPushingDirectionType")).value;
            WallPushingDirectionType wallPushing = (WallPushingDirectionType)Enum.Parse(typeof(WallPushingDirectionType), wallPushingStr);

            return new AboveCellDataWallPushing(aboveCellDataToSave.cellIndex, frequency, wallPushing);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_frequency = new AboveCellDataToSave.AboveCellParam("frequency", frequency.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_wallPushing = new AboveCellDataToSave.AboveCellParam("wallPushingDirectionType", wallPushingDirectionType.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_frequency, aboveCellParam_wallPushing
            });
        }

        internal enum WallPushingDirectionType {
            Left = 0,
            Right = 1,
            LeftDown = 2,
            RightDown = 3
        }
    }
}