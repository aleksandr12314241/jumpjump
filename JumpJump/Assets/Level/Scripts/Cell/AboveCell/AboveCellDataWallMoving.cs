using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataWallMoving : AboveCellData {
        [SerializeField]
        internal float startDelay;
        [SerializeField]
        internal float speed;
        [SerializeField]
        internal List<Vector2Int> path;

        public AboveCellDataWallMoving(Vector2Int cellIndex) : base(cellIndex) {
            startDelay = 0f;
            speed = 0.5f;
            path = new List<Vector2Int>() {
                cellIndex
            };
        }

        public AboveCellDataWallMoving(Vector2Int cellIndex, float startDelay, float speed, List<Vector2Int> path) : base(cellIndex) {
            this.startDelay = startDelay;
            this.speed = speed;
            this.path = new List<Vector2Int>();
            for (int i = 0; i < path.Count; i++) {
                this.path.Add(path[i]);
            }
        }

        public static AboveCellDataWallMoving GetAboveCellDataWallMoving(AboveCellDataToSave aboveCellDataToSave) {
            float startDelay = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("startDelay")).value);
            float speed = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("speed")).value);
            string[] pathsString = aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("path")).value.Split(';');

            List<Vector2Int> pathTemp = new List<Vector2Int>();
            for (int i = 0; i < pathsString.Length; i++) {
                string[] vec2int = pathsString[i].Split(',');
                pathTemp.Add(new Vector2Int(int.Parse(vec2int[0]), int.Parse(vec2int[1])));
            }

            return new AboveCellDataWallMoving(aboveCellDataToSave.cellIndex, startDelay, speed, pathTemp);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_startDelay = new AboveCellDataToSave.AboveCellParam("startDelay", startDelay.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_speed = new AboveCellDataToSave.AboveCellParam("speed", speed.ToString());

            string pathString = "";
            for (int i = 0; i < path.Count; i++) {
                pathString += path[i].x.ToString() + "," + path[i].y.ToString();

                if (i < path.Count - 1) {
                    pathString += ";";
                }
            }
            AboveCellDataToSave.AboveCellParam aboveCellParam_path = new AboveCellDataToSave.AboveCellParam("path", pathString);

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_startDelay, aboveCellParam_speed, aboveCellParam_path });
        }
    }
}