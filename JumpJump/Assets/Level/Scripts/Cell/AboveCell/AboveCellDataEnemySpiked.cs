using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataEnemySpiked : AboveCellData {
        [SerializeField]
        internal float delayTime;
        [SerializeField]
        internal bool right;
        [SerializeField]
        internal bool up;
        [SerializeField]
        internal bool left;
        [SerializeField]
        internal bool left_left;
        [SerializeField]
        internal bool leftDown;
        [SerializeField]
        internal bool down;
        [SerializeField]
        internal bool rightDown;
        [SerializeField]
        internal bool right_right;

        internal AboveCellDataEnemySpiked(Vector2Int cellIndex) : base(cellIndex) {
            delayTime = 1f;

            right = true;
            up = false;
            left = false;
            left_left = false;
            leftDown = false;
            down = false;
            rightDown = false;
            right_right = false;
        }

        public AboveCellDataEnemySpiked(Vector2Int cellIndex, float delayTime,
            bool right, bool up, bool left, bool left_left, bool leftDown, bool down, bool rightDown, bool right_right) : base(cellIndex) {
            this.delayTime = delayTime;

            this.right = right;
            this.up = up;
            this.left = left;
            this.left_left = left_left;
            this.leftDown = leftDown;
            this.down = down;
            this.rightDown = rightDown;
            this.right_right = right_right;
        }

        public static AboveCellDataEnemySpiked GetAboveCellDataEnemySpiked(AboveCellDataToSave aboveCellDataToSave) {
            float delayTime = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("delayTime")).value);

            bool right = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("right")).value);
            bool up = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("up")).value);
            bool left = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("left")).value);
            bool left_left = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("left_left")).value);
            bool leftDown = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("leftDown")).value);
            bool down = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("down")).value);
            bool rightDown = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("rightDown")).value);
            bool right_right = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("right_right")).value);

            return new AboveCellDataEnemySpiked(aboveCellDataToSave.cellIndex, delayTime, right, up, left, left_left, leftDown, down, rightDown, right_right);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_delayTime = new AboveCellDataToSave.AboveCellParam("delayTime", delayTime.ToString());

            AboveCellDataToSave.AboveCellParam aboveCellParam_right = new AboveCellDataToSave.AboveCellParam("right", right.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_up = new AboveCellDataToSave.AboveCellParam("up", up.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_left = new AboveCellDataToSave.AboveCellParam("left", left.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_left_left = new AboveCellDataToSave.AboveCellParam("left_left", left_left.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_leftDown = new AboveCellDataToSave.AboveCellParam("leftDown", leftDown.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_down = new AboveCellDataToSave.AboveCellParam("down", down.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_rightDown = new AboveCellDataToSave.AboveCellParam("rightDown", rightDown.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_right_right = new AboveCellDataToSave.AboveCellParam("right_right", right_right.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_delayTime,
                 aboveCellParam_right,
                 aboveCellParam_up,
                 aboveCellParam_left,
                 aboveCellParam_left_left,
                 aboveCellParam_leftDown,
                 aboveCellParam_down,
                 aboveCellParam_rightDown,
                 aboveCellParam_right_right
            });
        }
    }
}