using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataSlide : AboveCellData {
        [SerializeField]
        internal int directionX;
        [SerializeField]
        internal int directionY;

        public AboveCellDataSlide(Vector2Int cellIndex) : base(cellIndex) {
            directionX = 0;
            directionY = 0;
        }
        public AboveCellDataSlide(Vector2Int cellIndex, Vector2Int direction) : base(cellIndex) {
            directionX = direction.x;
            directionY = direction.y;
        }

        public static AboveCellDataSlide GetAboveCellDataSlide(AboveCellDataToSave aboveCellDataToSave) {
            int directionX = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionX")).value);
            int directionY = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionY")).value);

            return new AboveCellDataSlide(aboveCellDataToSave.cellIndex, new Vector2Int(directionX, directionY));
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_directionX = new AboveCellDataToSave.AboveCellParam("directionX", directionX.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_directionY = new AboveCellDataToSave.AboveCellParam("directionY", directionY.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_directionX, aboveCellParam_directionY });
        }
    }
}