using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataDestroyedOnStep : AboveCellData {
        [SerializeField]
        internal float timeDestroyed;

        public AboveCellDataDestroyedOnStep( Vector2Int cellIndex ) : base( cellIndex ) {
            timeDestroyed = 1f;
        }

        public AboveCellDataDestroyedOnStep( Vector2Int cellIndex, float timeDestroyed ) : base( cellIndex ) {
            this.timeDestroyed = timeDestroyed;
        }

        public static AboveCellDataDestroyedOnStep GetAboveCellDataDestroyed( AboveCellDataToSave aboveCellDataToSave ) {
            float timeDestroyed = float.Parse( aboveCellDataToSave.aboveCellParams.Find( x => x.key.Equals( "timeDestroyed" ) ).value );

            return new AboveCellDataDestroyedOnStep( aboveCellDataToSave.cellIndex, timeDestroyed );
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_timeDestroyed = new AboveCellDataToSave.AboveCellParam( "timeDestroyed", timeDestroyed.ToString() );

            return new AboveCellDataToSave( cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_timeDestroyed } );
        }
    }
}