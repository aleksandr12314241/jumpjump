using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataEnemyShooter : AboveCellData {
        [SerializeField]
        internal int directionX;
        [SerializeField]
        internal int directionY;
        [SerializeField]
        internal float delayShoot;
        [SerializeField]
        internal float speedShoot;
        [SerializeField]
        internal int shootRange;

        public AboveCellDataEnemyShooter(Vector2Int cellIndex) : base(cellIndex) {
            //����� �����
            directionX = 1;
            directionY = 0;
            delayShoot = 1f;
            speedShoot = 5f;
            shootRange = 10;
        }

        public AboveCellDataEnemyShooter(Vector2Int cellIndex, Vector2Int direction, float delayShoot, float speedShoot, int shootRange) : base(cellIndex) {
            directionX = direction.x;
            directionY = direction.y;
            this.delayShoot = delayShoot;
            this.speedShoot = speedShoot;
            this.shootRange = shootRange;
        }

        public static AboveCellDataEnemyShooter GetAboveCellDataEnemyShooter(AboveCellDataToSave aboveCellDataToSave) {
            int directionX = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionX")).value);
            int directionY = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionY")).value);
            float delayShoot = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("delayShoot")).value);
            float speedShoot = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("speedShoot")).value);
            int shootRange = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("shootRange")).value);

            return new AboveCellDataEnemyShooter(aboveCellDataToSave.cellIndex, new Vector2Int(directionX, directionY), delayShoot, speedShoot, shootRange);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_directionX = new AboveCellDataToSave.AboveCellParam("directionX", directionX.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_directionY = new AboveCellDataToSave.AboveCellParam("directionY", directionY.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_delayShoot = new AboveCellDataToSave.AboveCellParam("delayShoot", delayShoot.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_speedShoot = new AboveCellDataToSave.AboveCellParam("speedShoot", speedShoot.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_shootRange = new AboveCellDataToSave.AboveCellParam("shootRange", shootRange.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_directionX,
                aboveCellParam_directionY,
                aboveCellParam_delayShoot,
                aboveCellParam_speedShoot,
                aboveCellParam_shootRange
            });
        }
    }
}
