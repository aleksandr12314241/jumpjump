using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataSpring : AboveCellData {
        [SerializeField]
        internal float frequency;
        [SerializeField]
        internal int[] countDiscardCells;
        [SerializeField]
        internal int directionX;
        [SerializeField]
        internal int directionY;

        internal AboveCellDataSpring(Vector2Int cellIndex) : base(cellIndex) {
            frequency = 1f;

            countDiscardCells = new int[1];
            countDiscardCells[0] = 2;
            directionX = 1;
            directionY = 0;
        }

        internal AboveCellDataSpring(Vector2Int cellIndex, float frequency, List<int> countDiscardCells, Vector2Int direction) : base(cellIndex) {
            this.frequency = frequency;
            this.countDiscardCells = new int[countDiscardCells.Count];
            for (int i = 0; i < countDiscardCells.Count; i++) {
                this.countDiscardCells[i] = countDiscardCells[i];
            }
            directionX = direction.x;
            directionY = direction.y;
        }

        public static AboveCellDataSpring GetAboveCellDataSpring(AboveCellDataToSave aboveCellDataToSave) {
            float frequency = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("frequency")).value);

            string[] countDiscardCellsStr = aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("countDiscardCells")).value.Split(',');
            List<int> countCellsTemp = new List<int>();
            for (int i = 0; i < countDiscardCellsStr.Length; i++) {
                int countDiscard = int.Parse(countDiscardCellsStr[i]);
                countCellsTemp.Add(countDiscard);
            }

            int directionX = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionX")).value);
            int directionY = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("directionY")).value);

            return new AboveCellDataSpring(aboveCellDataToSave.cellIndex, frequency, countCellsTemp, new Vector2Int(directionX, directionY));
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_frequency = new AboveCellDataToSave.AboveCellParam("frequency", frequency.ToString());

            string countDiscardCellsString = "";
            for (int i = 0; i < countDiscardCells.Length; i++) {
                countDiscardCellsString += countDiscardCells[i];

                if (i < countDiscardCells.Length - 1) {
                    countDiscardCellsString += ",";
                }
            }
            AboveCellDataToSave.AboveCellParam aboveCellParam_countDiscardCells = new AboveCellDataToSave.AboveCellParam("countDiscardCells", countDiscardCellsString);

            AboveCellDataToSave.AboveCellParam aboveCellParam_directionX = new AboveCellDataToSave.AboveCellParam("directionX", directionX.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_directionY = new AboveCellDataToSave.AboveCellParam("directionY", directionY.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_frequency, aboveCellParam_countDiscardCells, aboveCellParam_directionX, aboveCellParam_directionY
            });
        }
    }
}