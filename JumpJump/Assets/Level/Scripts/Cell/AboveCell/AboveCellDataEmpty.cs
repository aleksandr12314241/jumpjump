using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataEmpty : AboveCellData {
        [SerializeField]
        internal int upper = 0;

        public AboveCellDataEmpty( Vector2Int cellIndex ) : base( cellIndex ) {
            upper = 120;
        }
    }
}
