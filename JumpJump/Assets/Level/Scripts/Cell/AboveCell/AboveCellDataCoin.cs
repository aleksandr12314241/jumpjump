using UnityEngine;
using System.Collections.Generic;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataCoin : AboveCellData {
        [SerializeField]
        internal int value;

        internal AboveCellDataCoin(Vector2Int cellIndex) : base(cellIndex) {
            value = 1;
        }

        internal AboveCellDataCoin(Vector2Int cellIndex, int value) : base(cellIndex) {
            this.value = value;
        }

        public static AboveCellDataCoin GetAboveCellDataCoin(AboveCellDataToSave aboveCellDataToSave) {
            int value = int.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("value")).value);

            return new AboveCellDataCoin(aboveCellDataToSave.cellIndex, value);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_value = new AboveCellDataToSave.AboveCellParam("value", value.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_value
            });
        }
    }
}