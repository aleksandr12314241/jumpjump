using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataPortal : AboveCellData {
        [SerializeField]
        internal int otherCellX;
        [SerializeField]
        internal int otherCellY;

        public AboveCellDataPortal( Vector2Int cellIndex ) : base( cellIndex ) {
            otherCellX = cellIndex.x;
            otherCellY = cellIndex.y;
        }

        public AboveCellDataPortal( Vector2Int cellIndex, int otherCellX, int otherCellY ) : base( cellIndex ) {
            this.otherCellX = otherCellX;
            this.otherCellY = otherCellY;
        }

        public static AboveCellDataPortal GetAboveCellDataPortal( AboveCellDataToSave aboveCellDataToSave ) {
            int otherCellX = int.Parse( aboveCellDataToSave.aboveCellParams.Find( x => x.key.Equals( "otherCellX" ) ).value );
            int otherCellY = int.Parse( aboveCellDataToSave.aboveCellParams.Find( x => x.key.Equals( "otherCellY" ) ).value );

            return new AboveCellDataPortal( aboveCellDataToSave.cellIndex, otherCellX, otherCellY );
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_otherCellX = new AboveCellDataToSave.AboveCellParam( "otherCellX", otherCellX.ToString() );
            AboveCellDataToSave.AboveCellParam aboveCellParam_otherCellY = new AboveCellDataToSave.AboveCellParam( "otherCellY", otherCellY.ToString() );

            return new AboveCellDataToSave( cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_otherCellX, aboveCellParam_otherCellY } );
        }
    }
}
