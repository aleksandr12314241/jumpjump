using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataEmptyMoving : AboveCellData {
        [SerializeField]
        internal float speed;
        [SerializeField]
        internal List<Vector2Int> path;

        public AboveCellDataEmptyMoving(Vector2Int cellIndex) : base(cellIndex) {
            speed = 1f;
            path = new List<Vector2Int>() {
                cellIndex
            };
        }

        public AboveCellDataEmptyMoving(Vector2Int cellIndex, float speed, List<Vector2Int> path) : base(cellIndex) {
            this.speed = speed;
            this.path = new List<Vector2Int>();
            for (int i = 0; i < path.Count; i++) {
                this.path.Add(path[i]);
            }
        }

        public static AboveCellDataEmptyMoving GetAboveCellDataEmptyMoving(AboveCellDataToSave aboveCellDataToSave) {
            float speed = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("speed")).value);
            string[] pathsString = aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("path")).value.Split(';');

            List<Vector2Int> pathTemp = new List<Vector2Int>();
            for (int i = 0; i < pathsString.Length; i++) {
                string[] vec2int = pathsString[i].Split(',');
                pathTemp.Add(new Vector2Int(int.Parse(vec2int[0]), int.Parse(vec2int[1])));
            }

            return new AboveCellDataEmptyMoving(aboveCellDataToSave.cellIndex, speed, pathTemp);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_speed = new AboveCellDataToSave.AboveCellParam("speed", speed.ToString());

            string pathString = "";
            for (int i = 0; i < path.Count; i++) {
                pathString += path[i].x.ToString() + "," + path[i].y.ToString();

                if (i < path.Count - 1) {
                    pathString += ";";
                }
            }
            AboveCellDataToSave.AboveCellParam aboveCellParam_path = new AboveCellDataToSave.AboveCellParam("path", pathString);

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_speed, aboveCellParam_path });
        }
    }
}