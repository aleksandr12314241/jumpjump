using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellData {
        [SerializeField]
        protected Vector2Int cellIndex;

        internal AboveCellData( Vector2Int cellIndex ) {
            this.cellIndex = cellIndex;
        }

        public static AboveCellData GetAboveCellData( AboveCellDataToSave aboveCellDataToSave ) {
            return new AboveCellData( aboveCellDataToSave.cellIndex );
        }

        internal virtual AboveCellDataToSave GetAboveCellDataToSave() {
            return new AboveCellDataToSave( cellIndex );
        }
    }
}
