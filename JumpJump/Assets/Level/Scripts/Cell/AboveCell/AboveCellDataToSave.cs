using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataToSave {
        [SerializeField]
        internal Vector2Int cellIndex;
        [SerializeField]
        internal List<AboveCellParam> aboveCellParams;

        public AboveCellDataToSave( Vector2Int cellIndex ) {
            this.cellIndex = cellIndex;
            aboveCellParams = new List<AboveCellParam>();
        }

        public AboveCellDataToSave( Vector2Int cellIndex, List<AboveCellParam> aboveCellParams ) {
            this.cellIndex = cellIndex;

            this.aboveCellParams = aboveCellParams;
        }

        public AboveCellDataToSave( AboveCellDataToSave aboveCellDataToSave ) {
            this.cellIndex = aboveCellDataToSave.cellIndex;

            aboveCellParams = new List<AboveCellParam>();
            for ( int i = 0; i < aboveCellDataToSave.aboveCellParams.Count; i++ ) {
                aboveCellParams.Add( new AboveCellParam( aboveCellDataToSave.aboveCellParams[i].key, aboveCellDataToSave.aboveCellParams[i].value ) );
            }
        }

        [System.Serializable]
        public class AboveCellParam {
            [SerializeField]
            public string key;
            [SerializeField]
            public string value;

            public AboveCellParam( string key, string value ) {
                this.key = key;
                this.value = value;
            }
        }
    }
}