using System.Collections.Generic;
using UnityEngine;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataEnemyTwisting : AboveCellData {
        [SerializeField]
        internal bool clockwise;
        [SerializeField]
        internal float speedRoatation;

        internal AboveCellDataEnemyTwisting(Vector2Int cellIndex) : base(cellIndex) {
            clockwise = false;
            speedRoatation = 2f;
        }

        public AboveCellDataEnemyTwisting(Vector2Int cellIndex, bool clockwise, float speedRoatation) : base(cellIndex) {
            this.clockwise = clockwise;
            this.speedRoatation = speedRoatation;
        }

        public static AboveCellDataEnemyTwisting GetAboveCellDataEnemyTwisting(AboveCellDataToSave aboveCellDataToSave) {
            bool clockwise = bool.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("clockwise")).value);
            float speedRoatation = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("speedRoatation")).value);

            return new AboveCellDataEnemyTwisting(aboveCellDataToSave.cellIndex, clockwise, speedRoatation);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_clockwise = new AboveCellDataToSave.AboveCellParam("clockwise", clockwise.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_speedRoatation = new AboveCellDataToSave.AboveCellParam("speedRoatation", speedRoatation.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() {
                aboveCellParam_clockwise,
                aboveCellParam_speedRoatation
            });
        }
    }
}