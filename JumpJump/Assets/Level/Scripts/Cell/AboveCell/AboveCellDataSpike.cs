using UnityEngine;
using System.Collections.Generic;

namespace Gameplay {
    [System.Serializable]
    public class AboveCellDataSpike : AboveCellData {
        [SerializeField]
        internal float startDelay;
        [SerializeField]
        internal float frequency;

        public AboveCellDataSpike(Vector2Int cellIndex) : base(cellIndex) {
            startDelay = 0f;
            frequency = 1f;
        }

        public AboveCellDataSpike(Vector2Int cellIndex, float startDelay, float frequency) : base(cellIndex) {
            this.startDelay = startDelay;
            this.frequency = frequency;
        }

        public static AboveCellDataSpike GetAboveCellDataSpike(AboveCellDataToSave aboveCellDataToSave) {
            float startDelay = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("startDelay")).value);
            float frequency = float.Parse(aboveCellDataToSave.aboveCellParams.Find(x => x.key.Equals("frequency")).value);

            return new AboveCellDataSpike(aboveCellDataToSave.cellIndex, startDelay, frequency);
        }

        internal override AboveCellDataToSave GetAboveCellDataToSave() {
            AboveCellDataToSave.AboveCellParam aboveCellParam_startDelay = new AboveCellDataToSave.AboveCellParam("startDelay", startDelay.ToString());
            AboveCellDataToSave.AboveCellParam aboveCellParam_frequency = new AboveCellDataToSave.AboveCellParam("frequency", frequency.ToString());

            return new AboveCellDataToSave(cellIndex, new List<AboveCellDataToSave.AboveCellParam>() { aboveCellParam_startDelay, aboveCellParam_frequency });
        }
    }
}