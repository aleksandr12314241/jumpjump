using UnityEngine;
using DG.Tweening;

namespace Gameplay {
    public class CellDestroyedOnStep : Cell {
        private float timeDestroyed;

        private MeshRenderer myMeshRenderer;
        //private SkinnedMeshRenderer mySkinnedMeshRenderer;
        //private Tween skinnedMeshRendererTween;

        protected override void Init(AboveCellData aboveCellData) {
            transform.DOKill();
            //if (skinnedMeshRendererTween != null) {
            //    skinnedMeshRendererTween.Kill();
            //}

            AboveCellDataDestroyedOnStep aboveCellDataDestroyedOnStep = (AboveCellDataDestroyedOnStep)aboveCellData;

            //mySkinnedMeshRenderer = modelTransform.GetComponent<SkinnedMeshRenderer>();
            myMeshRenderer = modelTransform.GetComponent<MeshRenderer>();

            timeDestroyed = aboveCellDataDestroyedOnStep.timeDestroyed;

            //mySkinnedMeshRenderer.enabled = true;
            myMeshRenderer.enabled = true;
        }

        protected override void StartPlayAction() {
        }

        protected override void Die() {
            transform.DOKill();

            float timeWait = 0.5f;

            //skinnedMeshRendererTween.Kill();
            //skinnedMeshRendererTween = DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(0), x => mySkinnedMeshRenderer.SetBlendShapeWeight(0, x), 100f, timeWait);

            transform.DOMove(transform.position, timeWait).OnComplete(() => {
                myMeshRenderer.enabled = false;
            });
        }

        internal override void Restart() {
            transform.DOKill();
            //skinnedMeshRendererTween.Kill();

            transform.position = LevelUtility.GetCellPositionByIndex(cellIndex);

            myMeshRenderer.enabled = true;
            //mySkinnedMeshRenderer.enabled = true;
            //mySkinnedMeshRenderer.SetBlendShapeWeight(0, 0f);
        }

        internal override void Step(Player player) {

            base.Step(player);

            transform.DOKill();
            transform.DOMove(transform.position, timeDestroyed).OnComplete(() => {
                Die();

                if (cellIndex.x == player.NewCellIndex.x && cellIndex.y == player.NewCellIndex.y) {
                    player.Fall(CellType.DestroyedOnStep);
                }
            });
        }
    }
}
