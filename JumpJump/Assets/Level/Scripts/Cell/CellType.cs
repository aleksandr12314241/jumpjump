namespace Gameplay {
    public enum CellType {
        Void = 0,
        Empty = 1,
        Spike = 2,
        End = 3,
        Player = 4,
        Enemy_0 = 5,
        DestroyedOnStep = 6,
        Portal = 7,
        WallMoving = 8,
        SlideLeftDown = 9,
        SlideRightDown = 10,
        SlideLeft = 11,
        SlideRight = 12,
        EmptyMoving = 13,
        EnemyShooter = 14,
        EnemyTwisting = 15,
        EnemySpiked = 16,
        WallPushing = 17,
        Spring = 18,
        Coin = 19
    }
}