using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

namespace Gameplay {

    public class CellMover : MonoBehaviour {
        internal Action<Vector2Int, Vector2Int, Vector2Int> OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };

        private List<Vector2Int> path;
        private float speed;

        private Vector2Int startPoint;
        private Vector2Int currentCellIndex;

        private bool isMoved;
        private bool isCirclePath;

        private int currentPointIndex;
        private int newPointIndex;
        private Vector2Int direction;

        public Vector2Int Direction {
            get => direction;
        }
        public bool IsMoved {
            get => isMoved;
        }

        internal void Init(List<Vector2Int> path, float speed) {
            this.path = path;
            this.speed = speed;
            this.startPoint = path[0];
            this.currentCellIndex = this.startPoint;

            isMoved = path.Count >= 2;
            if (!isMoved) {
                return;
            }

            currentPointIndex = 0;
            newPointIndex++;
            Vector2Int startPoint = path[currentPointIndex];
            Vector2Int endPoint = path[path.Count - 1];

            Vector2Int diff = new Vector2Int(Math.Abs(startPoint.x - endPoint.x), Math.Abs(startPoint.y - endPoint.y));
            isCirclePath = diff.x <= 1 & diff.y <= 1;
        }

        public void StartMove() {
            //if (!isMoved) {
            //    return;
            //}

            MoveToNewPoint();
        }

        public void StopMove() {
            transform.DOKill();
        }

        private void MoveToNewPoint() {
            Vector3 newPoint = LevelUtility.GetCellPositionByIndex(path[newPointIndex]);
            direction = path[newPointIndex] - path[currentPointIndex];

            //this.CustomInvoke(speed / 2f, SetCurrentPoint);

            transform.DOKill();
            transform.DOScale(transform.localScale, speed / 2f).OnComplete(() => {
                //OnChangeCellIndex();
                SetCurrentPoint();
            });

            transform.DOMove(newPoint, speed).OnComplete(() => {
                MoveToNewPoint();
            });
        }

        public void SetCurrentCellIndex(Vector2Int cellIndex) {
            currentCellIndex = cellIndex;
        }

        private void SetCurrentPoint() {
            OnChangeCellIndex(path[newPointIndex], path[currentPointIndex], direction);

            SetCurrentCellIndex(path[newPointIndex]);
            int oldPointIndex = currentPointIndex;
            currentPointIndex = newPointIndex;

            if (isCirclePath) {
                newPointIndex++;
                if (newPointIndex >= path.Count) {
                    newPointIndex = 0;
                }
            }
            else {
                //�������� � ������� ����������� ������� ����
                if (newPointIndex > oldPointIndex) {
                    newPointIndex++;

                    if (newPointIndex >= path.Count) {
                        newPointIndex = path.Count - 2;
                    }
                }
                else {
                    newPointIndex--;
                    if (newPointIndex < 0) {
                        newPointIndex = 1;
                    }
                }
            }
        }

        internal void RemoveAllListeners() {
            OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };
        }
    }
}