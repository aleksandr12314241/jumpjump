using UnityEngine;

namespace Gameplay {

    public class CellPortal : Cell {
        private Vector2Int otherCell;

        protected override void Init(AboveCellData aboveCellData) {
            AboveCellDataPortal aboveCellDataPortal = (AboveCellDataPortal)aboveCellData;

            otherCell = new Vector2Int(aboveCellDataPortal.otherCellX, aboveCellDataPortal.otherCellY);
        }

        protected override void Die() {
        }

        protected override void StartPlayAction() {
        }

        internal override void Step(Player player) {
            base.Step(player);

            if (player.IsInPortal) {
                return;
            }

            player.Portal(cellIndex, otherCell);
        }
    }
}