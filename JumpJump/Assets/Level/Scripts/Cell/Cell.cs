using System;
using UnityEngine;
using Zenject;
using DG.Tweening;

namespace Gameplay {

    public abstract class Cell : MonoBehaviour, IPoolable<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool> {
        [SerializeField]
        protected Transform modelTransform;

        private Vector3 modelStartPos;

        protected IMemoryPool pool;
        protected Vector2Int cellIndex;

        private TextMesh textMesh;

        internal CellType FloorCellType {
            private set; get;
        }
        internal CellType AboveCellType {
            private set; get;
        }
        internal Vector2Int CellIndex {
            get => cellIndex;
        }

        public void OnSpawned(Vector2Int cellIndex, CellType floorCellType, CellType aboveCellType, AboveCellData aboveCellData, IMemoryPool pool) {
            this.pool = pool;
            this.cellIndex = cellIndex;
            FloorCellType = floorCellType;
            AboveCellType = aboveCellType;

            transform.position = LevelUtility.GetCellPositionByIndex(cellIndex);

            modelStartPos = modelTransform.localPosition;

            Init(aboveCellData);
            //AddCellIndexText(cellIndex);
        }

        protected abstract void Init(AboveCellData aboveCellData);

        protected abstract void StartPlayAction();
        protected abstract void Die();
        internal virtual void Step(Player player) {

            modelTransform.DOLocalJump(modelStartPos, -0.1f, 1, 0.3f);

            //modelTransform.DOLocalMoveY(startPostionModel.y - 0.1f, 0.).
            //modelTransform.DOMove
        }

        internal void MoveToNewPositionByIndex(float speed = 0.5f) {
            Vector3 newPosition = LevelUtility.GetCellPositionByIndex(cellIndex);
            transform.DOMove(newPosition, speed);
        }

        private void AddCellIndexText(Vector2Int cellIndex) {
            GameObject textMeshGO = new GameObject();
            textMeshGO.transform.SetParent(this.transform, false);
            textMeshGO.transform.localPosition = Vector3.zero;
            textMeshGO.transform.localEulerAngles = new Vector3(45f, 45f, 0f);

            textMesh = textMeshGO.AddComponent<TextMesh>();
            textMesh.text = cellIndex.x + ";" + cellIndex.y;
            textMesh.anchor = TextAnchor.UpperCenter;
            textMesh.alignment = TextAlignment.Center;
            textMesh.characterSize = 0.4f;
            textMesh.color = Color.black;
            textMesh.fontStyle = FontStyle.Bold;
        }

        internal void Dispose() {
            Die();
            pool.Despawn(this);
        }

        public void OnDespawned() {
            pool = null;
        }

        internal void SetAboveCellType(CellType aboveCellType) {
            AboveCellType = aboveCellType;
        }

        internal void StartPlay() {
            StartPlayAction();
        }

        internal void SetCellIndex(Vector2Int cellIndex) {
            this.cellIndex = cellIndex;

            if (textMesh != null) {
                textMesh.text = cellIndex.x + ";" + cellIndex.y;
            }
        }

        internal virtual void Restart() {
        }

        //empty
        public class FactoryCommon : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellCommon> {
        }
        public class CellCommonPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellCommon> {
        }

        //end
        public class FactoryEnd : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellCommon> {
        }
        public class CellCommonPoolEnd : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellCommon> {
        }

        //spike
        public class FactorySpike : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSpike> {
        }
        public class CellSpikePool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSpike> {
        }

        public class FactoryCellDestroyedOnStep : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellDestroyedOnStep> {
        }
        public class CellDestroyedOnStepPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellDestroyedOnStep> {
        }

        public class FactoryCellPortal : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellPortal> {
        }
        public class CellPortalPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellPortal> {
        }
        public class FactoryCellSlideLeftDown : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide> {
        }
        public class CellSlideLeftDownPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSlide> {
        }
        public class FactoryCellSlideRightDown : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide> {
        }
        public class CellSlideRightDownPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSlide> {
        }
        public class FactoryCellSlideLeft : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide> {
        }
        public class CellSlideLeftPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSlide> {
        }
        public class FactoryCellSlideRight : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSlide> {
        }
        public class CellSlideRightPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSlide> {
        }

        public class FactoryCellEmptyMoving : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellEmptyMoving> {
        }
        public class CellEmptyMovingPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellEmptyMoving> {
        }

        public class FactoryCellSpring : PlaceholderFactory<Vector2Int, CellType, CellType, AboveCellData, CellSpring> {
        }
        public class CellSpringPool : MonoPoolableMemoryPool<Vector2Int, CellType, CellType, AboveCellData, IMemoryPool, CellSpring> {
        }
    }
}