using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Gameplay {

    public class CellSpring : Cell {
        [SerializeField]
        private TextMesh countDiscardTextMesh;

        private int[] countDiscardCells;
        private int currentCountDiscardCells;
        private Vector2Int direction;
        private float frequency;

        protected override void Init(AboveCellData aboveCellData) {
            AboveCellDataSpring aboveCellDataSpring = (AboveCellDataSpring)aboveCellData;

            frequency = aboveCellDataSpring.frequency;
            direction = new Vector2Int(aboveCellDataSpring.directionX, aboveCellDataSpring.directionY);

            countDiscardCells = new int[aboveCellDataSpring.countDiscardCells.Length];
            for (int i = 0; i < aboveCellDataSpring.countDiscardCells.Length; i++) {
                countDiscardCells[i] = aboveCellDataSpring.countDiscardCells[i];
            }

            currentCountDiscardCells = 0;
        }

        protected override void StartPlayAction() {
            currentCountDiscardCells = -1;

            WaitChanheCountDiscard();
        }

        protected override void Die() {
            transform.DOKill();
        }

        private void WaitChanheCountDiscard() {
            currentCountDiscardCells++;

            if (currentCountDiscardCells >= countDiscardCells.Length) {
                currentCountDiscardCells = 0;
            }

            countDiscardTextMesh.text = countDiscardCells[currentCountDiscardCells].ToString();
            transform.DOKill();
            transform.DOMove(transform.position, frequency).OnComplete(WaitChanheCountDiscard);
        }

        internal override void Step(Player player) {
            base.Step(player);

            Vector2Int newCellIndex = cellIndex + (direction * countDiscardCells[currentCountDiscardCells]);
            player.TryJumpToCell(newCellIndex);
        }
    }
}