using UnityEngine;
using Zenject;
using Common;
using System.Collections.Generic;
using System;

#if UNITY_EDITOR
using Tools;
using UnityEditor;
#endif

namespace Gameplay {
    public class LevelGenerateController : MonoBehaviour {
        internal Action<GenerateLevelData> OnLevelGenerateEnd = (GenerateLevelData generateLevelData) => { };
        internal Action OnRestart = () => { };
        internal Action OnLevelsEnd = () => { };
        internal Action OnEditorPlayEnd = () => { };

        [SerializeField]
        private Player playerPrf;
        [Inject]
        private LevelChooseService levelChooseService;
        [Inject]
        private SequenceLevelsDataSet sequenceLevelsDataSet;
        //[Inject]
        //private LevelPrefabsDataSet levelPrefabsDataSet;

        [Inject]
        private readonly Cell.FactoryCommon cellCommonFactory;
        [Inject]
        private readonly Cell.FactoryEnd cellCommonFactoryEnd;
        [Inject]
        private readonly Cell.FactorySpike cellSpikeFactory;
        [Inject]
        private readonly Cell.FactoryCellDestroyedOnStep cellDestroyedOnStep;
        [Inject]
        private readonly Cell.FactoryCellPortal cellPortal;
        [Inject]
        private readonly Cell.FactoryCellSlideLeftDown cellSlideLeftDown;
        [Inject]
        private readonly Cell.FactoryCellSlideRightDown cellSlideRightDown;
        [Inject]
        private readonly Cell.FactoryCellSlideLeft cellSlideLeft;
        [Inject]
        private readonly Cell.FactoryCellSlideRight cellSlideRight;
        [Inject]
        private readonly Cell.FactoryCellEmptyMoving cellEmptyMoving;
        [Inject]
        private readonly Cell.FactoryCellSpring cellSpring;
        [Inject]
        private readonly Enemy.WallMovingFactory enemyWallMovingFactory;
        [Inject]
        private readonly Enemy.Enemy_0Factory enemyEnemy_0Factory;
        [Inject]
        private readonly Enemy.EnemyShooterFactory enemyShooterFactory;
        [Inject]
        private readonly Enemy.EnemyTwistingFactory enemyTwistingFactory;
        [Inject]
        private readonly Enemy.EnemySpikedFactory enemySpikedFactory;
        [Inject]
        private readonly Enemy.WallPushingFactory wallPushingFactory;
        [Inject]
        private readonly Enemy.CoinFactory coinFactory;

        private LevelDataSet levelDataSet;
#if UNITY_EDITOR
        private ConfigDataSet configDataSet;
#endif
        private GenerateLevelData generateLevelData;

        private void OnDestroy() {
#if UNITY_EDITOR
            if (configDataSet.IsEditorPlaying) {
                //Tools.LevelEditor.LevelEditorWindow.OpenEditorWindow();

                configDataSet.SetIsEditorPlaying(false);
            }
#endif
        }

        internal void Init() {
#if UNITY_EDITOR
            configDataSet = Resources.Load<ConfigDataSet>("ConfigDataSet");
#endif
        }

        internal void Generate() {
            levelDataSet = sequenceLevelsDataSet.GetLevelDataSet(levelChooseService.CurrentLevelIndex);

#if UNITY_EDITOR
            if (configDataSet.IsEditorPlaying) {
                string pathToLevel = configDataSet.GetPathToLevel();

                levelDataSet = AssetDatabase.LoadAssetAtPath<LevelDataSet>(pathToLevel);
            }
#endif
            GenerateLevel(levelDataSet);
        }

        private void GenerateLevel(LevelDataSet levelDataSet) {
            Player playerTemp;
            if (generateLevelData == null || generateLevelData.Player == null) {
                playerTemp = Instantiate(playerPrf);
                playerTemp.Init();
            }
            else {
                playerTemp = generateLevelData.Player;
                playerTemp.Restart();
            }

            Cell[][] cellsTemp = new Cell[levelDataSet.FieldSize][];
            List<Enemy> enemies = new List<Enemy>();

            for (int i = 0; i < cellsTemp.Length; i++) {
                cellsTemp[i] = new Cell[levelDataSet.FieldSize];
            }

            for (int i = 0; i < levelDataSet.CellDatas.Count; i++) {
                Cell cell = null;
                Vector2Int cellIndex = new Vector2Int(levelDataSet.CellDatas[i].x, levelDataSet.CellDatas[i].y);

                switch (levelDataSet.CellDatas[i].CellType) {
                    case CellType.Empty:
                    case CellType.Player:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        break;
                    case CellType.End:
                        cell = cellCommonFactoryEnd.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, null);
                        break;
                    case CellType.Enemy_0:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        Enemy_0 enemy_0 = enemyEnemy_0Factory.Create(cellIndex, (AboveCellData)AboveCellDataEnemy_0.GetAboveCellDataEnemy_0(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(enemy_0);
                        break;
                    case CellType.Spike:
                        cell = cellSpikeFactory.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSpike.GetAboveCellDataSpike(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.DestroyedOnStep:
                        cell = cellDestroyedOnStep.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataDestroyedOnStep.GetAboveCellDataDestroyed(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.Portal:
                        cell = cellPortal.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataPortal.GetAboveCellDataPortal(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.WallMoving:
                        WallMovingEnemy wallMoving = enemyWallMovingFactory.Create(cellIndex, (AboveCellData)AboveCellDataWallMoving.GetAboveCellDataWallMoving(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(wallMoving);
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        break;
                    case CellType.SlideLeftDown:
                        cell = cellSlideLeftDown.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSlide.GetAboveCellDataSlide(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.SlideRightDown:
                        cell = cellSlideRightDown.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSlide.GetAboveCellDataSlide(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.SlideLeft:
                        cell = cellSlideLeft.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSlide.GetAboveCellDataSlide(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.SlideRight:
                        cell = cellSlideRight.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSlide.GetAboveCellDataSlide(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.EmptyMoving:
                        cell = cellEmptyMoving.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataEmptyMoving.GetAboveCellDataEmptyMoving(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.EnemyShooter:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        EnemyShooter enemyShooter = enemyShooterFactory.Create(cellIndex, (AboveCellData)AboveCellDataEnemyShooter.GetAboveCellDataEnemyShooter(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(enemyShooter);
                        break;
                    case CellType.EnemyTwisting:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        EnemyTwisting enemyTwisting = enemyTwistingFactory.Create(cellIndex, (AboveCellData)AboveCellDataEnemyTwisting.GetAboveCellDataEnemyTwisting(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(enemyTwisting);
                        break;
                    case CellType.EnemySpiked:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        EnemySpiked enemySpiked = enemySpikedFactory.Create(cellIndex, (AboveCellData)AboveCellDataEnemySpiked.GetAboveCellDataEnemySpiked(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(enemySpiked);
                        break;
                    case CellType.WallPushing:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        WallPushing wallPushing = wallPushingFactory.Create(cellIndex, (AboveCellData)AboveCellDataWallPushing.GetAboveCellDataWallPushing(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(wallPushing);
                        break;
                    case CellType.Spring:
                        cell = cellSpring.Create(cellIndex, levelDataSet.CellDatas[i].CellType, CellType.Void, (AboveCellData)AboveCellDataSpring.GetAboveCellDataSpring(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        break;
                    case CellType.Coin:
                        cell = cellCommonFactory.Create(cellIndex, CellType.Empty, levelDataSet.CellDatas[i].CellType, null);
                        Coin coin = coinFactory.Create(cellIndex, (AboveCellData)AboveCellDataCoin.GetAboveCellDataCoin(levelDataSet.CellDatas[i].aboveCellDataToSave));
                        enemies.Add(coin);
                        break;
                    default:
                        continue;
                }
                cellsTemp[levelDataSet.CellDatas[i].x][levelDataSet.CellDatas[i].y] = cell;
            }

            OnLevelGenerateEnd(generateLevelData = new GenerateLevelData(levelChooseService.CurrentLevelIndex, playerTemp, cellsTemp, enemies));
        }

        private void DestroyOldLevel() {
            for (int i = 0; i < generateLevelData.Cells.Length; i++) {
                for (int j = 0; j < generateLevelData.Cells[i].Length; j++) {
                    if (generateLevelData.Cells[i][j] == null) {
                        continue;
                    }

                    generateLevelData.Cells[i][j].Dispose();
                }
            }

            for (int i = 0; i < generateLevelData.Enemies.Count; i++) {
                generateLevelData.Enemies[i].Dispose();
            }

            generateLevelData.Dispose();
        }

        internal void GenerateNextLevel() {
            DestroyOldLevel();

#if UNITY_EDITOR
            if (configDataSet.IsEditorPlaying) {

                Generate();
                return;
            }
#endif
            if (levelChooseService.CurrentLevelIndex >= sequenceLevelsDataSet.Levels.Length) {
                // ����� �������, ����� � ����
                OnLevelsEnd();
                return;
            }

            Generate();
        }

        internal void Restart() {
            OnRestart();
        }

        internal class GenerateLevelData {
            private int currentLevelIndex;
            private Player player;
            private Cell[][] cells;
            private List<Enemy> enemies;

            public Player Player {
                get => player;
            }
            public Cell[][] Cells {
                get => cells;
            }
            public List<Enemy> Enemies {
                get => enemies;
            }
            public int CurrentLevelIndex {
                get => currentLevelIndex;
                set => currentLevelIndex = value;
            }

            internal GenerateLevelData(int currentLevelIndex, Player player, Cell[][] cells, List<Enemy> enemies) {
                this.currentLevelIndex = currentLevelIndex;
                this.player = player;
                this.cells = cells;
                this.enemies = enemies;
            }

            internal void Dispose() {
                this.cells = null;
                this.enemies = null;
            }
        }
    }
}