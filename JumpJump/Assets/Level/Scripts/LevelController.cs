using System;
using UnityEngine;
using Zenject;
using System.Collections.Generic;
using Common;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using Tools;
using UnityEditor;
#endif

namespace Gameplay {
    [RequireComponent(typeof(LevelGenerateController))]
    public class LevelController : MonoBehaviour {
        private Player player;
        private Cell[][] cells;
        private List<Enemy> enemies;

        private LevelGenerateController generateFieldController;

        [Inject]
        private FollowCamera followCamera;
        [Inject]
        private UI.PlayerControlUI playerControl;
        [Inject]
        private SignalBus signalBus;
        [Inject]
        private LevelChooseService levelChooseService;
        [Inject]
        private ZenjectSceneLoader zenjectSceneLoader;
        [Inject]
        private Profile profile;
        [Inject]
        private UpMenuUI upMenuUI;

#if UNITY_EDITOR
        ConfigDataSet configDataSet;
#endif

        private void Start() {
            Debug.Log("profile   " + profile.level);
#if UNITY_EDITOR
            configDataSet = Resources.Load<ConfigDataSet>("ConfigDataSet");
#endif

            generateFieldController = GetComponent<LevelGenerateController>();
            generateFieldController.Init();

            generateFieldController.OnLevelGenerateEnd += OnLevelGenerateEnd;
            generateFieldController.OnLevelsEnd += OnLevelsEnd;
#if UNITY_EDITOR
            generateFieldController.OnEditorPlayEnd += OnEditorPlayEnd;
#endif

            generateFieldController.Generate();
        }

        private void OnDestroy() {
            generateFieldController.OnLevelGenerateEnd -= OnLevelGenerateEnd;
            generateFieldController.OnLevelsEnd -= OnLevelsEnd;
#if UNITY_EDITOR
            generateFieldController.OnEditorPlayEnd -= OnEditorPlayEnd;
#endif

            generateFieldController = null;
        }

#if UNITY_EDITOR
        private void OnEditorPlayEnd() {
        }
#endif

        private void OnLevelsEnd() {
            zenjectSceneLoader.LoadScene("Menu", LoadSceneMode.Single);
        }

        private void OnLevelGenerateEnd(LevelGenerateController.GenerateLevelData level) {
            cells = level.Cells;
            enemies = level.Enemies;

            RemoveCellsListners();

            for (int i = 0; i < enemies.Count; i++) {
                enemies[i].OnChangeCellIndex += OnChangeAboveCellIndex;

                if (enemies[i].CellType == CellType.WallPushing) {
                    WallPushing wallPushing = (WallPushing)enemies[i];
                    wallPushing.OnSetCellAboveType += OnSetCellAboveType;
                    wallPushing.OnUnSetCellAboveType += OnUnSetCellAboveType;
                }
            }

            for (int i = 0; i < cells.Length; i++) {
                for (int j = 0; j < cells[i].Length; j++) {
                    if (cells[i][j] == null) {
                        continue;
                    }

                    if (cells[i][j].FloorCellType == CellType.EmptyMoving) {
                        CellEmptyMoving cellEmptyMoving = (CellEmptyMoving)cells[i][j];
                        cellEmptyMoving.OnChangeCellIndex += OnChangeCellIndex;
                    }
                }
            }

            InitPlayer(level.Player);

            StartPlayLevel();
        }


        private void InitPlayer(Player player) {
            this.player = player;

            followCamera.SetPlayer(player.transform);
            playerControl.Init(player);

            RemovePlayerListners();
            //player.OnStartJump = ( JumpSide jumpSide, Vector2Int currentCellIndex, Vector2Int newCellIndex ) => { };
            player.OnStartJump += OnStartJump;
            //player.OnEndJump = ( JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex ) => { };
            player.OnEndJump += OnEndJump;
            player.OnGoToPortal += OnGoToPortal;
            player.OnDamage += OnDamage;
            player.OnCollectCoin += OnCollectCoin;
        }

        private void StartPlayLevel() {
            for (int i = 0; i < enemies.Count; i++) {
                enemies[i].StartPlay();
            }

            Cell cellEnd = null;

            for (int i = 0; i < cells.Length; i++) {
                for (int j = 0; j < cells[i].Length; j++) {
                    if (cells[i][j] == null) {
                        continue;
                    }

                    cells[i][j].StartPlay();

                    if (cells[i][j].FloorCellType == CellType.End) {
                        cellEnd = cells[i][j];
                    }
                }
            }

            if (cellEnd != null) {
                followCamera.ShowEndCell(cellEnd.CellIndex);
            }
            else {
                followCamera.StartFollow();
            }
        }

        internal void StartPlayerControl() {
            playerControl.SetIsPlay(true);
        }

        internal void ContinuePlayLevelAsGhost() {
            List<int> pathPointRemoved = new List<int>();
            int pointContinue = player.Path.Count - 1;

            if (cells[player.NewCellIndex.x][player.NewCellIndex.y] != null &&
                cells[player.NewCellIndex.x][player.NewCellIndex.y].FloorCellType == CellType.DestroyedOnStep) {
                cells[player.NewCellIndex.x][player.NewCellIndex.y].Restart();
            }

            for (int i = player.Path.Count - 1; i >= 0; i--) {
                if (IsAllowJump(player.Path[i])) {
                    pathPointRemoved.Add(i);
                    continue;
                }

                Vector2Int cellIndex = player.Path[i];
                if (cells[cellIndex.x][cellIndex.y].AboveCellType == CellType.DestroyedOnStep) {
                    cells[cellIndex.x][cellIndex.y].Restart();

                    pathPointRemoved.Add(i);
                    continue;
                }

                if (cells[cellIndex.x][cellIndex.y].AboveCellType == CellType.Empty) {
                    pathPointRemoved.Add(i);
                    pointContinue = i;
                    break;
                }
            }

            player.ContinuePlayLevelAsGhost(pointContinue, pathPointRemoved);
        }

        private void RemovePlayerListners() {
            player.OnStartJump -= OnStartJump;
            player.OnEndJump -= OnEndJump;
            player.OnStartPush -= OnStartPush;
            player.OnEndPush -= OnEndPush;
            player.OnEndJump -= OnEndJump;
            player.OnGoToPortal -= OnGoToPortal;
            player.OnDamage -= OnDamage;
            player.OnCollectCoin -= OnCollectCoin;
        }

        private void RemoveCellsListners() {
            if (enemies != null || enemies.Count == 0) {
                for (int i = 0; i < enemies.Count; i++) {
                    enemies[i].RemoveAllListeners();
                }
            }

            for (int i = 0; i < cells.Length; i++) {
                for (int j = 0; j < cells[i].Length; j++) {
                    if (cells[i][j] == null) {
                        continue;
                    }

                    if (cells[i][j].FloorCellType == CellType.EmptyMoving) {
                        CellEmptyMoving cellEmptyMoving = (CellEmptyMoving)cells[i][j];
                        cellEmptyMoving.RemoveAllListeners();
                    }
                }
            }
        }

        private void OnGoToPortal(Vector2Int newCellIndex, Vector2Int oldCellIndex) {
        }

        internal void Restart() {
            player.Restart();
            playerControl.SetIsPlay(true);

            for (int i = 0; i < cells.Length; i++) {
                for (int j = 0; j < cells[i].Length; j++) {
                    if (cells[i][j] != null) {
                        cells[i][j].Restart();
                    }
                }
            }

            for (int i = 0; i < enemies.Count; i++) {
                enemies[i].Restart();
            }

            upMenuUI.Restart();

            generateFieldController.Restart();
        }

        private void OnStartJump(JumpSide jumpSide, Vector2Int currentCellIndex, Vector2Int newCellIndex) {
            if (!IsAllowJump(newCellIndex)) {
                player.JumpOnPlace();
                return;
            }

            player.Jump(jumpSide, newCellIndex);
        }

        private void OnStartPush(JumpSide jumpSide, Vector2Int currentCellIndex, Vector2Int newCellIndex) {
        }

        private void OnEndJump(JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) {
            Cell cell = GetCell(newCurrentCellIndex);
            if (cell != null) {
                cell.Step(player);
            }

            if (IsLose(newCurrentCellIndex)) {
                Loose();
            }
            else if (IsWin(newCurrentCellIndex)) {
                Win();
            }
        }

        private void OnEndPush(JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) {

        }

        private void OnDamage() {
            Loose();
        }

        private void OnCollectCoin(int coinValue, Vector2Int cellIndex) {
            signalBus.Fire(new OnCollectCoinLevel() {
                collectedCoinsValue = coinValue,
                fieldPosition = LevelUtility.GetCellPositionByIndexOffset1(cellIndex)
            });
        }

        private void OnChangeAboveCellIndex(CellType aboveCellType, Vector2Int newCellIndex, Vector2Int oldCellIndex) {
            if (newCellIndex.x == oldCellIndex.x && newCellIndex.y == oldCellIndex.y) {
                return;
            }

            if (cells[newCellIndex.x][newCellIndex.y] != null) {
                cells[newCellIndex.x][newCellIndex.y].SetAboveCellType(aboveCellType);
            }

            if (cells[oldCellIndex.x][oldCellIndex.y] != null) {
                cells[oldCellIndex.x][oldCellIndex.y].SetAboveCellType(CellType.Void);
            }
        }

        private void OnChangeCellIndex(Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) {
            Cell cell = cells[newCellIndex.x][newCellIndex.y];

            cells[newCellIndex.x][newCellIndex.y] = cells[oldCellIndex.x][oldCellIndex.y];
            cells[oldCellIndex.x][oldCellIndex.y] = cell;

            cells[newCellIndex.x][newCellIndex.y].SetCellIndex(new Vector2Int(newCellIndex.x, newCellIndex.y));

            if (cells[oldCellIndex.x][oldCellIndex.y] != null) {
                cells[oldCellIndex.x][oldCellIndex.y].SetCellIndex(new Vector2Int(oldCellIndex.x, oldCellIndex.y));
                if (cells[oldCellIndex.x][oldCellIndex.y].FloorCellType != CellType.EmptyMoving) {
                    cells[oldCellIndex.x][oldCellIndex.y].MoveToNewPositionByIndex();
                }
            }
        }

        private void OnSetCellAboveType(CellType cellType, Vector2Int cellIndex) {
            if (cells[cellIndex.x][cellIndex.y] == null) {
                return;
            }

            cells[cellIndex.x][cellIndex.y].SetAboveCellType(cellType);
        }
        private void OnUnSetCellAboveType(CellType cellType, Vector2Int cellIndex) {
            if (cells[cellIndex.x][cellIndex.y] == null) {
                return;
            }

            if (cells[cellIndex.x][cellIndex.y].AboveCellType == cellType) {
                cells[cellIndex.x][cellIndex.y].SetAboveCellType(CellType.Empty);
            }
        }

        private bool IsAllowJump(Vector2Int newPlayerIndex) {
            if (cells[newPlayerIndex.x][newPlayerIndex.y] == null) {
                return true;
            }

            if (cells[newPlayerIndex.x][newPlayerIndex.y].AboveCellType == CellType.WallMoving ||
                cells[newPlayerIndex.x][newPlayerIndex.y].AboveCellType == CellType.WallPushing) {
                return false;
            }
            return true;
        }

        private Cell GetCell(Vector2Int cellIndex) {
            if (cellIndex.x < 0 || cellIndex.y < 0 || cellIndex.x >= cells.Length || cellIndex.y >= cells.Length) {
                return null;
            }
            return cells[cellIndex.x][cellIndex.y];
        }

        private bool IsLose(Vector2Int newPlayerIndex) {
            if (newPlayerIndex.x < 0 || newPlayerIndex.y < 0 || newPlayerIndex.x >= cells.Length || newPlayerIndex.y >= cells.Length) {
                return true;
            }
            if (cells[newPlayerIndex.x][newPlayerIndex.y] == null) {
                return true;
            }

            if (cells[newPlayerIndex.x][newPlayerIndex.y].AboveCellType.Equals(CellType.WallMoving) ||
                cells[newPlayerIndex.x][newPlayerIndex.y].AboveCellType.Equals(CellType.WallPushing)) {
                return true;
            }

            return false;
        }

        private bool IsWin(Vector2Int newCurrentCellIndex) {
            if (newCurrentCellIndex.x < 0 || newCurrentCellIndex.y < 0 || newCurrentCellIndex.x >= cells.Length || newCurrentCellIndex.y >= cells.Length) {
                return false;
            }
            if (cells[newCurrentCellIndex.x][newCurrentCellIndex.y] == null) {
                return false;
            }
            if (cells[newCurrentCellIndex.x][newCurrentCellIndex.y].FloorCellType.Equals(CellType.End)) {
                return true;
            }
            return false;
        }

        private void Loose() {
            signalBus.Fire(new OnLoseLevel() {
                isStartCell = player.Path.Count <= 0
            });
        }

        private void Win() {
            playerControl.SetIsPlay(false);
#if UNITY_EDITOR
            if (!configDataSet.IsEditorPlaying) {
                levelChooseService.AddCurrentLevelIndex();
            }
#else
            levelChooseService.AddCurrentLevelIndex();
#endif

            profile.TrySaveNewLevel(levelChooseService.CurrentLevelIndex);

            // сохранение прогресса
            signalBus.Fire(new OnWinLevel() {
            });
        }
    }
}