using System.Collections;
using UnityEngine;
using DG.Tweening;
using Zenject;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace Gameplay {
    public class FollowCamera : MonoBehaviour {
        // camera will follow this object
        [SerializeField]
        public Transform Target;

        // offset between camera and target
        [SerializeField]
        public Vector3 Offset;
        [SerializeField]
        private bool OffsetIsCameraPos;

        // change this value to get desired smoothness
        [SerializeField]
        public float SmoothTime = 0.3f;

        [Inject]
        private SignalBus signalBus;

        // This value will change at the runtime depending on target movement. Initialize with zero vector.
        private Vector3 velocity = Vector3.zero;

        private Coroutine currentFollowCameraUpdate;

        private bool isShowEndCell = false;

        private Vector2 mouseDownPosition;
        private Vector2 mouseMostDeviation;
        private Vector2 offsetClick = new Vector2(50f, 50f);

        private float moveSpeed = 0.5f;

        private bool isDragCamera = false;
        internal bool IsDragCamera => isDragCamera;
        private bool isPlayerControlImmediately = false;

        private RectTransform canvasRectTransform;

        private void Start() {
            canvasRectTransform = FindObjectOfType<CanvasScaler>().GetComponent<RectTransform>();
        }

        private void Update() {
#if UNITY_EDITOR || UNITY_STANDALONE
            if (Input.GetMouseButton(0)) {

#elif UNITY_IOS || UNITY_ANDROID
            if ( Input.touchCount > 0 ) {
#endif
                if (isShowEndCell) {
                    StartPlayerControl();
                }
            }
        }

        internal void SetPlayer(Transform Target) {
            this.Target = Target;

            Offset = !OffsetIsCameraPos ? Offset : transform.position;
        }

        internal void OnMovePlayer() {
            if (!isDragCamera) {
                return;
            }

            StartFollow();
        }

        internal void StartFollow() {
            isDragCamera = false;

            StopFollow();

            if (Target == null) {
                return;
            }

            currentFollowCameraUpdate = StartCoroutine(FollowCameraUpdate());
        }

        private void StopFollow() {
            if (currentFollowCameraUpdate != null) {
                StopCoroutine(currentFollowCameraUpdate);
                currentFollowCameraUpdate = null;
            }
        }

        internal void StartDragCamera(PointerEventData data) {
            if (isShowEndCell) {
                return;
            }

            if (!isDragCamera) {
                isDragCamera = true;
            }

            mouseDownPosition = Input.mousePosition;
            mouseMostDeviation = Vector2.zero;

            StopFollow();
        }

        internal void DragCamera(PointerEventData data) {
            Vector2 offset = mouseDownPosition - new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            if (offset.x * offset.x > mouseMostDeviation.x * mouseMostDeviation.x ||
                 offset.y * offset.y > mouseMostDeviation.y * mouseMostDeviation.y) {
                mouseMostDeviation = offset;
            }
#if UNITY_EDITOR || UNITY_STANDALONE
            float axisMouseX = Input.GetAxis("Mouse X");
#elif UNITY_IOS || UNITY_ANDROID
                float axisMouseX = Input.GetTouch( 0 ).deltaPosition.x/100f;
#endif
            Vector3 offsetX = new Vector3((offset.x * moveSpeed * Time.deltaTime), 0f, (-1f * offset.x * moveSpeed * Time.deltaTime));
            Vector3 offsetZ = new Vector3((-1f * offset.y * moveSpeed * Time.deltaTime), 0f, (-1f * offset.y * moveSpeed * Time.deltaTime));

            transform.position += offsetX - offsetZ;

            mouseDownPosition = Input.mousePosition;
        }

        private IEnumerator FollowCameraUpdate() {
            while (!isDragCamera) {
                yield return null;
                Vector3 targetPosition = new Vector3(Target.position.x + Offset.x, Offset.y, Target.position.z + Offset.z);
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);
            }
        }

        internal void ShowEndCell(Vector2Int endCellIndex) {
            if (isPlayerControlImmediately) {
                isPlayerControlImmediately = false;
                StartPlayerControlImmediately();
                return;
            }

            Vector3 cellPosition = LevelUtility.GetCellPositionByIndex(endCellIndex);
            Vector3 startPosition = new Vector3(cellPosition.x + Offset.x, Offset.y, cellPosition.z + Offset.z);

            Vector3 endPosition = new Vector3(Target.position.x + Offset.x, Offset.y, Target.position.z + Offset.z);

            transform.position = startPosition;

            isShowEndCell = true;

            StopFollow();

            transform.DOKill();
            transform.DOMove(transform.position, LevelConstants.startPlayTime / 2f).OnComplete(() => {
                transform.DOMove(endPosition, LevelConstants.startPlayTime / 2f).OnComplete(StartPlayerControl);
            });
        }
        internal void SetPlayerControlImmediately() {
            transform.DOKill();

            Vector3 endPosition = new Vector3(Target.position.x + Offset.x, Offset.y, Target.position.z + Offset.z);
            transform.position = endPosition;

            StartPlayerControl();
        }

        internal void SetIsPlayerControlImmediately(bool isPlayerControlImmediately) {
            this.isPlayerControlImmediately = isPlayerControlImmediately;
        }

        internal void StartPlayerControlImmediately() {
            Vector3 endPosition = new Vector3(Target.position.x + Offset.x, Offset.y, Target.position.z + Offset.z);
            transform.position = endPosition;

            StartPlayerControl();
        }

        private void StartPlayerControl() {
            transform.DOKill();

            isShowEndCell = false;

            StartFollow();

            signalBus.Fire(new OnStartPlayerControl() {
            });
        }
    }
}