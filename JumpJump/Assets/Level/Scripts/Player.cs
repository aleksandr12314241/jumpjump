using UnityEngine;
using System;
using DG.Tweening;
using System.Collections.Generic;

namespace Gameplay {
    public class Player : MonoBehaviour {
        internal Action<JumpSide, Vector2Int, Vector2Int> OnStartJump = (JumpSide jumpSide, Vector2Int currentCellIndex, Vector2Int newCellIndex) => { };
        internal Action<JumpSide, Vector2Int, Vector2Int> OnEndJump = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
        internal Action<JumpSide, Vector2Int, Vector2Int> OnStartPush = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
        internal Action<JumpSide, Vector2Int, Vector2Int> OnEndPush = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
        internal Action<Vector2Int, Vector2Int> OnGoToPortal = (Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
        internal Action OnDamage = () => { };
        internal Action<int, Vector2Int> OnCollectCoin = (int coinValue, Vector2Int cellIndex) => { };

        [SerializeField]
        private Collider myCollider;
        [SerializeField]
        private MeshRenderer myRenderer;

        private bool isAlive = true;
        private bool isJump = false;
        private bool isInPortal = false;
        private bool isPush = false;

        private bool isGhost = false;

        private bool onMovingCell = false;

        private Vector2Int oldCellIndex;
        private Vector2Int playerCellIndex;
        private Vector2Int newCellIndex;

        private Material myMaterial;

        private List<Vector2Int> path = new List<Vector2Int>();

        internal Vector2Int NewCellIndex => newCellIndex;
        private Transform startParentTransform;
        private const float timeJump = 0.3f;

        private bool IsAllowJump => isAlive && !isJump && !isInPortal && !isPush;

        private int collectedCoinsValue = 0;
        private List<Vector2Int> cellIndexes = new List<Vector2Int>();

        public bool IsInPortal {
            get => isInPortal;
        }
        public bool IsGhost {
            get => isGhost;
        }

        public List<Vector2Int> Path {
            get => path;
        }

        //public Vector2Int PlayerCellIndex {
        //    get => playerCellIndex;
        //}

        internal void Init() {
            isAlive = true;
            playerCellIndex = Vector2Int.zero;
            newCellIndex = playerCellIndex;
            oldCellIndex = playerCellIndex;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(playerCellIndex);

            startParentTransform = this.transform.parent;

            path = new List<Vector2Int>();

            myMaterial = Instantiate<Material>(myRenderer.material);
            myRenderer.material = myMaterial;

            collectedCoinsValue = 0;
            cellIndexes = new List<Vector2Int>();
        }

        private void OnDestroy() {
            OnStartJump = (JumpSide jumpSide, Vector2Int currentCellIndex, Vector2Int newCellIndex) => { };
            OnEndJump = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
            OnStartPush = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
            OnEndPush = (JumpSide jumpSide, Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
            OnGoToPortal = (Vector2Int newCurrentCellIndex, Vector2Int oldCellIndex) => { };
            OnDamage = () => { };

            Destroy(myMaterial);
        }

        internal void Portal(Vector2Int cellIndex, Vector2Int otherCell) {
            isInPortal = true;

            //�������� �������
            transform.DOMove(transform.position, 0.15f).OnComplete(() => {
                transform.position = LevelUtility.GetCellPositionByIndexOffset1(otherCell);

                SetNewCellIndex(otherCell);

                OnEndJump(JumpSide.Portal, otherCell, cellIndex);

                isInPortal = false;
            });
        }

        internal void Fall(CellType cellType) {
            Damage(cellType);
        }

        internal void Damage(CellType cellType) {
            if (!isAlive) {
                return;
            }

            isAlive = false;

            //Debug.Log("cellType.ToString()   " + cellType.ToString());

            OnDamage();
        }

        //internal void Damage(CellType cellType) {
        //    Damage();
        //}

        private JumpSide GetJumpSideByDirection(Vector2Int direction) {
            if (direction.x > 0 && direction.y == 0) {
                return JumpSide.Right;
            }
            else if (direction.x == 0 && direction.y >= 0) {
                return JumpSide.Left;
            }
            else if (direction.x == 0 && direction.y < 0) {
                return JumpSide.RightBack;
            }
            else if (direction.x < 0 && direction.y == 0) {
                return JumpSide.LeftBack;
            }

            return JumpSide.None;
        }
        internal void TryJumpToCell(Vector2Int newPlayerCellIndex) {
            if (!IsAllowJump) {
                return;
            }

            isJump = true;

            OnStartJump(JumpSide.ToCell, playerCellIndex, newPlayerCellIndex);
        }

        internal void TryJumpRight() {
            if (!IsAllowJump) {
                return;
            }

            isJump = true;

            Vector2Int newPlayerCellIndex = playerCellIndex + new Vector2Int(1, 0);
            OnStartJump(JumpSide.Right, playerCellIndex, newPlayerCellIndex);
        }

        internal void TryJumpLeft() {
            if (!IsAllowJump) {
                return;
            }

            isJump = true;

            Vector2Int newPlayerCellIndex = playerCellIndex + new Vector2Int(0, 1);
            OnStartJump(JumpSide.Left, playerCellIndex, newPlayerCellIndex);
        }

        internal void TryJumpRightBack() {
            if (!IsAllowJump) {
                return;
            }

            isJump = true;

            Vector2Int newPlayerCellIndex = playerCellIndex + new Vector2Int(1, 0);
            OnStartJump(JumpSide.Right, playerCellIndex, newPlayerCellIndex);
        }
        private void CheckSetGhost() {
            if (!IsGhost) {
                return;
            }
            SetGhost(false);
        }

        private void SetGhost(bool isGhost) {
            this.isGhost = isGhost;
            myCollider.enabled = !isGhost;

            float alpha = isGhost ? 0.5f : 1f;
            myMaterial.color = new Color(myMaterial.color.r, myMaterial.color.g, myMaterial.color.b, alpha);
        }

        internal void ContinuePlayLevelAsGhost(int pointContinue, List<int> pathPointRemoved) {
            SetGhost(true);

            if (onMovingCell) {
                StepFromMovingCell();
            }

            playerCellIndex = path[pointContinue];
            newCellIndex = playerCellIndex;
            oldCellIndex = path.Count >= 2 ? path[pointContinue - 1] : playerCellIndex;

            for (int i = 0; i < pathPointRemoved.Count; i++) {
                path.RemoveAt(pathPointRemoved[i]);
            }

            ResetPlayerState();
        }

        internal void TryJumpLeftBack() {
            if (!IsAllowJump) {
                return;
            }

            isJump = true;

            Vector2Int newPlayerCellIndex = playerCellIndex + new Vector2Int(0, 1);
            OnStartJump(JumpSide.Left, playerCellIndex, newPlayerCellIndex);
        }

        internal void Jump(JumpSide jumpSide, Vector2Int newPlayerCellIndex) {
            switch (jumpSide) {
                case JumpSide.Left:
                    JumpLeft(newPlayerCellIndex);
                    break;
                case JumpSide.Right:
                    JumpRight(newPlayerCellIndex);
                    break;
                case JumpSide.ToCell:
                    JumpTo(newPlayerCellIndex);
                    break;
            }
        }

        internal void JumpRight(Vector2Int newPlayerCellIndex) {
            transform.DOKill();
            this.newCellIndex = newPlayerCellIndex;

            Vector3 newPlayerPosition = LevelUtility.GetCellPositionByIndexOffset1(newPlayerCellIndex);

            transform.DOJump(newPlayerPosition, 0.5f, 1, timeJump).OnComplete(() => {
                isJump = false;

                EndJump(newPlayerCellIndex);
            });
        }

        internal void JumpLeft(Vector2Int newPlayerCellIndex) {
            transform.DOKill();
            this.newCellIndex = newPlayerCellIndex;

            Vector3 newPlayerPosition = LevelUtility.GetCellPositionByIndexOffset1(newPlayerCellIndex);

            transform.DOJump(newPlayerPosition, 0.5f, 1, timeJump).OnComplete(() => {
                isJump = false;

                EndJump(newPlayerCellIndex);
            });
        }

        public void JumpTo(Vector2Int newPlayerCellIndex) {
            transform.DOKill();
            this.newCellIndex = newPlayerCellIndex;

            Vector2Int celIndexDistance = new Vector2Int(Math.Abs(Math.Abs(newCellIndex.x) - Math.Abs(playerCellIndex.x)), Math.Abs(Math.Abs(newCellIndex.y) - Math.Abs(playerCellIndex.y)));

            float timeJumpTemp = celIndexDistance.x > celIndexDistance.y ? celIndexDistance.x * timeJump : celIndexDistance.y * timeJump;

            Vector3 newPlayerPosition = LevelUtility.GetCellPositionByIndexOffset1(newPlayerCellIndex);

            transform.DOJump(newPlayerPosition, 0.5f, 1, timeJumpTemp).OnComplete(() => {
                isJump = false;

                EndJump(newPlayerCellIndex);
            });
        }

        public void Push(Vector2Int direction, float speed = 0.3f) {
            isJump = false;
            isPush = true;
            transform.DOKill();

            Vector2Int newPlayerCellIndex = newCellIndex + direction;
            Vector3 newPlayerPosition = LevelUtility.GetCellPositionByIndexOffset1(newPlayerCellIndex);

            JumpSide jumpSide = GetJumpSideByDirection(direction);

            OnStartPush(jumpSide, newPlayerCellIndex, playerCellIndex);
            //OnStartJump(jumpSide, newPlayerCellIndex, playerCellIndex);

            transform.DOMove(newPlayerPosition, speed).OnComplete(() => {
                isPush = false;

                EndJump(newPlayerCellIndex);
            });
        }

        private void EndJump(Vector2Int newPlayerCellIndex) {
            CheckSetGhost();

            Vector2Int oldPlayerCellIndex = playerCellIndex;

            SetNewCellIndex(newPlayerCellIndex);

            OnEndJump(JumpSide.Left, playerCellIndex, oldPlayerCellIndex);
        }

        private void SetNewCellIndex(Vector2Int newPlayerCellIndex) {
            oldCellIndex = playerCellIndex;
            playerCellIndex = newPlayerCellIndex;
            newCellIndex = playerCellIndex;

            path.Add(oldCellIndex);
        }

        internal void JumpOnPlace() {
            isJump = false;

            transform.DOKill();
            transform.DOJump(LevelUtility.GetCellPositionByIndexOffset1(playerCellIndex), 0.35f, 1, timeJump);
        }

        internal void StepOnMovingCell() {
            onMovingCell = true;
        }
        internal void ChangeIndexByCellMoving(Vector2Int newCellIndex) {
            oldCellIndex = playerCellIndex;
            this.newCellIndex = newCellIndex;
            playerCellIndex = newCellIndex;
        }
        internal void StepFromMovingCell() {
            onMovingCell = false;

            transform.SetParent(startParentTransform);
        }

        internal void CollectCoin(int coinValue, Vector2Int cellIndex) {
            collectedCoinsValue += coinValue;

            cellIndexes.Add(cellIndex);

            OnCollectCoin(coinValue, cellIndex);
        }

        internal void Restart() {
            path = new List<Vector2Int>();
            playerCellIndex = Vector2Int.zero;

            collectedCoinsValue = 0;
            cellIndexes = new List<Vector2Int>();

            ResetPlayerState();
        }

        private void ResetPlayerState() {
            transform.DOKill();

            isJump = false;
            isAlive = true;
            isInPortal = false;
            isPush = false;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(playerCellIndex);
        }
    }
}