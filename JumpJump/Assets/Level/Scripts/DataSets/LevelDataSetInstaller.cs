using UnityEngine;
using Zenject;

namespace Gameplay {
    [CreateAssetMenu(fileName = "LevelDataSetInstaller", menuName = "JumpJump/LevelDataSetInstaller")]
    public class LevelDataSetInstaller : ScriptableObjectInstaller<LevelDataSetInstaller> {
        [SerializeField]
        private LevelPrefabsDataSet levelPrefabsDataSet;

        public override void InstallBindings() {
            Container.BindInstances(levelPrefabsDataSet);
        }
    }
}