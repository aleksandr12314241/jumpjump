using UnityEngine;

namespace Gameplay {
    [CreateAssetMenu(fileName = "LevelPrefabsDataSet", menuName = "JumpJump/LevelPrefabsDataSet")]
    public class LevelPrefabsDataSet : ScriptableObject {
        [SerializeField]
        internal Player playerPrf;
        [SerializeField]
        internal Cell cellEmptyPrf;
        [SerializeField]
        internal CellSpike cellSpikePrf;
        [SerializeField]
        internal CellDestroyedOnStep cellDestroyedOnStepPrf;
        [SerializeField]
        internal CellPortal cellPortalPrf;
        [SerializeField]
        internal CellSlide cellSlideLeftDownPrf;
        [SerializeField]
        internal CellSlide cellSlideRightDownPrf;
        [SerializeField]
        internal CellSlide cellSlideLeftPrf;
        [SerializeField]
        internal CellSlide cellSlideRightPrf;
        [SerializeField]
        internal CellEmptyMoving cellEmptyMovingPrf;
        [SerializeField]
        internal Cell cellEndPrf;
        [SerializeField]
        internal Enemy_0 enemy_0Prf;
        [SerializeField]
        internal WallMovingEnemy wallMovingEnemyPrf;
        [SerializeField]
        internal EnemyShooter enemyShooterPrf;
        [SerializeField]
        internal Bullet bulletPrf;
        [SerializeField]
        internal EnemyTwisting enemyTwistingPrf;
        [SerializeField]
        internal EnemySpiked enemySpikedPrf;
        [SerializeField]
        internal WallPushing wallPushingPrf;
        [SerializeField]
        internal CellSpring cellSpringPrf;
        [SerializeField]
        internal Coin coinPrf;
    }
}