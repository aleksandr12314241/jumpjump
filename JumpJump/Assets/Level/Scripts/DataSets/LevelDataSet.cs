using UnityEngine;
using System.Collections.Generic;

namespace Gameplay {
    //[CreateAssetMenu( fileName = "LevelDataSet", menuName = "JumpJump/LevelDataSet", order = 1 )]
    [System.Serializable]
    public class LevelDataSet : ScriptableObject {

        [SerializeField]
        private int fieldSize = 3;
        [SerializeField]
        private List<CellData> cellDatas;

        [SerializeField]
        private List<CellData> cellFloorDatas;

        internal int FieldSize => fieldSize;
        internal List<CellData> CellDatas => cellDatas;

        internal void Init(int fieldSize) {
            this.fieldSize = fieldSize;

            cellDatas = new List<CellData> {
                new CellData( 0, 0, CellType.Player )
            };
        }

        //GETTERS
        internal bool IsEmptyLevel() {
            for (int i = 0; i < cellDatas.Count; i++) {
                if (cellDatas[i].x == 0 && cellDatas[i].y == 0) {
                    continue;
                }

                if (!cellDatas[i].CellType.Equals(CellType.Void)) {
                    return false;
                }
            }

            return true;
        }

        internal CellType GetCellType(Vector2Int cellIndex) {
            for (int i = 0; i < cellDatas.Count; i++) {
                if (cellDatas[i].x == cellIndex.x && cellDatas[i].y == cellIndex.y) {
                    return cellDatas[i].CellType;
                }
            }

            return CellType.Void;
        }
        internal CellData GetCellData(Vector2Int cellIndex) {
            for (int i = 0; i < cellDatas.Count; i++) {
                if (cellDatas[i].x == cellIndex.x && cellDatas[i].y == cellIndex.y) {
                    return cellDatas[i];
                }
            }
            return null;
        }

        internal bool IsContainsExit() {
            for (int i = 0; i < cellDatas.Count; i++) {
                if (cellDatas[i].CellType.Equals(CellType.End)) {
                    return true;
                }
            }

            return false;
        }

        //SET
        internal void CloneLevelDataSet(LevelDataSet levelDataSetToSave) {
            this.fieldSize = levelDataSetToSave.fieldSize;

            this.cellDatas = new List<CellData>();
            for (int i = 0; i < levelDataSetToSave.cellDatas.Count; i++) {
                this.cellDatas.Add(new CellData(levelDataSetToSave.cellDatas[i]));
            }
        }

        //UPDATES
        internal void UpdateFieldSize(int fieldSize) {
            this.fieldSize = fieldSize;

            List<CellData> newCellDatas = new List<CellData> {
                new CellData( 0, 0, CellType.Player )
            };

            for (int i = 0; i < fieldSize; i++) {
                for (int j = 0; j < fieldSize; j++) {
                    if (i == 0 && j == 0) {
                        continue;
                    }
                    if (i + j >= fieldSize) {
                        continue;
                    }

                    CellType cellType = GetCellType(new Vector2Int(i, j));
                    if (cellType.Equals(CellType.Void)) {
                        continue;
                    }

                    newCellDatas.Add(new CellData(GetCellData(new Vector2Int(i, j))));
                }
            }

            cellDatas = newCellDatas;
        }

        internal void UpdateCellType(Vector2Int cellIndex, CellType cellType) {
            for (int i = 0; i < this.cellDatas.Count; i++) {
                if (cellDatas[i].x == cellIndex.x && cellDatas[i].y == cellIndex.y) {
                    cellDatas[i].SetNewCellType(cellType);
                    return;
                }
            }

            cellDatas.Add(new CellData(cellIndex.x, cellIndex.y, cellType));
        }

        //TODO:
        internal void Update�ompliance() {
            int maxFieldSize = 0;
            List<int> removedCells = new List<int>();
            for (int i = 0; i < cellDatas.Count; i++) {
                if (cellDatas[i].x >= maxFieldSize) {
                    maxFieldSize = cellDatas[i].x;
                    break;
                }
                if (cellDatas[i].y >= maxFieldSize) {
                    maxFieldSize = cellDatas[i].y;
                    break;
                }

                if (cellDatas[i].CellType.Equals(CellType.Void)) {
                    removedCells.Add(i);
                }
            }

            for (int i = 0; i < removedCells.Count; i++) {
                cellDatas.RemoveAt(removedCells[i]);
            }
        }

        ////TODO: �������� �����������
        //internal void �ompliance�heck() {
        //    int maxFieldSize = 0;
        //    for ( int i = 0; i < cellDatas.Count; i++ ) {
        //        if ( cellDatas[i].x >= maxFieldSize || cellDatas[i].y >= fieldSize ) {
        //            maxFieldSize = 
        //            break;
        //        }
        //    }
        //}

        [System.Serializable]
        internal class CellData {
            [SerializeField]
            internal int x;
            [SerializeField]
            internal int y;
            [SerializeField]
            private CellType cellType;
            //[SerializeField]
            //internal AboveCellData aboveCellData;
            [SerializeField]
            internal AboveCellDataToSave aboveCellDataToSave;

            internal CellType CellType {
                get => cellType;
            }

            internal CellData(int x, int y, CellType cellType) {
                this.x = x;
                this.y = y;
                this.cellType = cellType;

                aboveCellDataToSave = GetAboveCellDataByCellType(cellType);
            }

            internal CellData(CellData cellData) {
                x = cellData.x;
                y = cellData.y;
                cellType = cellData.cellType;

                aboveCellDataToSave = cellData.aboveCellDataToSave;
            }

            internal void SetNewCellType(CellType newCellType) {
                if (cellType.Equals(newCellType)) {
                    return;
                }

                cellType = newCellType;
                aboveCellDataToSave = GetAboveCellDataByCellType(cellType);
            }

            private AboveCellDataToSave GetAboveCellDataByCellType(CellType cellType) {
                Vector2Int cellIndex = new Vector2Int(x, y);
                switch (cellType) {
                    case CellType.Spike:
                        return new AboveCellDataSpike(cellIndex).GetAboveCellDataToSave();
                    case CellType.Enemy_0:
                        return new AboveCellDataEnemy_0(cellIndex).GetAboveCellDataToSave();
                    case CellType.DestroyedOnStep:
                        return new AboveCellDataDestroyedOnStep(cellIndex).GetAboveCellDataToSave();
                    case CellType.Portal:
                        return new AboveCellDataPortal(cellIndex).GetAboveCellDataToSave();
                    case CellType.WallMoving:
                        return new AboveCellDataWallMoving(cellIndex).GetAboveCellDataToSave();
                    case CellType.SlideLeftDown:
                        return new AboveCellDataSlide(cellIndex, new Vector2Int(-1, 0)).GetAboveCellDataToSave();
                    case CellType.SlideRightDown:
                        return new AboveCellDataSlide(cellIndex, new Vector2Int(0, -1)).GetAboveCellDataToSave();
                    case CellType.SlideLeft:
                        return new AboveCellDataSlide(cellIndex, new Vector2Int(0, 1)).GetAboveCellDataToSave();
                    case CellType.SlideRight:
                        return new AboveCellDataSlide(cellIndex, new Vector2Int(1, 0)).GetAboveCellDataToSave();
                    case CellType.EmptyMoving:
                        return new AboveCellDataEmptyMoving(cellIndex).GetAboveCellDataToSave();
                    case CellType.EnemyShooter:
                        return new AboveCellDataEnemyShooter(cellIndex).GetAboveCellDataToSave();
                    case CellType.EnemyTwisting:
                        return new AboveCellDataEnemyTwisting(cellIndex).GetAboveCellDataToSave();
                    case CellType.EnemySpiked:
                        return new AboveCellDataEnemySpiked(cellIndex).GetAboveCellDataToSave();
                    case CellType.WallPushing:
                        return new AboveCellDataWallPushing(cellIndex).GetAboveCellDataToSave();
                    case CellType.Spring:
                        return new AboveCellDataSpring(cellIndex).GetAboveCellDataToSave();
                    case CellType.Coin:
                        return new AboveCellDataCoin(cellIndex).GetAboveCellDataToSave();
                    default:
                        return new AboveCellDataEmpty(cellIndex).GetAboveCellDataToSave();
                }
            }
        }
    }
}
