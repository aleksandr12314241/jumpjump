using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Common;

namespace Gameplay {
    [CreateAssetMenu(fileName = "SkinLevelDataSet", menuName = "JumpJump/Level/SkinMenuDataSet", order = 1)]
    public class SkinLevelDataSet : ScriptableObject {
        [SerializeField]
        private SkinData[] skinDatas;

        public SkinData[] SkinDatas {
            get => skinDatas;
        }

        [System.Serializable]
        public class SkinData {
            [SerializeField]
            internal SkinType skinType;
            [SerializeField]
            internal Sprite image;
        }
    }
}