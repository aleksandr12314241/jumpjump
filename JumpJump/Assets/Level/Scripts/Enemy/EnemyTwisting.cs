using UnityEngine;
using DG.Tweening;
using System.Collections;

namespace Gameplay {

    public class EnemyTwisting : Enemy {
        [SerializeField]
        protected ColliderPlayerDetected colliderPlayerDetected_beam;

        private Coroutine rotationUpdate = null;

        private int clockwiseDirection;
        private float speedRoatation;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.EnemyTwisting;

            AboveCellDataEnemyTwisting aboveCellDataEnemyTwisting = (AboveCellDataEnemyTwisting)aboveCellData;

            clockwiseDirection = aboveCellDataEnemyTwisting.clockwise ? 1 : -1;

            speedRoatation = aboveCellDataEnemyTwisting.speedRoatation;

            colliderPlayerDetected_beam.OnDetectPlayer = (Player player) => { };
            colliderPlayerDetected_beam.OnDetectPlayer += OnDetectPlayer;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
        }

        protected override void StartPlayAction() {
            //Roatate();
            //Rotation3();
            if (rotationUpdate != null) {
                StopCoroutine(rotationUpdate);
                rotationUpdate = null;
            }

            rotationUpdate = StartCoroutine(RotateUpdate());
        }

        private IEnumerator RotateUpdate() {
            for (; ; ) {
                yield return new WaitForEndOfFrame();
                Rotation3();
            }
        }

        private void Rotation3() {
            float rotationSpeed = 360f / speedRoatation;

            transform.Rotate(clockwiseDirection * Vector3.up, rotationSpeed * Time.deltaTime);
        }

        void Roatate() {
            transform.DOKill();
            transform.localEulerAngles = new Vector3(0f, 0f, 0f);
            transform.DORotate(new Vector3(0f, 359f, 0f), speedRoatation).OnComplete(Roatate);
            //transform.DORotateQuaternion(new Quaternion(0f, 180f, 0f, 1f), speedRoatation).OnComplete(Roatate);
        }

        void Roatate2() {
            transform.DORotate(new Vector3(0f, 360f, 0f), speedRoatation / 2).OnComplete(Roatate);
        }

        protected override void Die() {
            //transform.DOKill();
            colliderPlayerDetected_beam.OnDetectPlayer -= OnDetectPlayer;
            colliderPlayerDetected_beam.OnDetectPlayer = (Player player) => { };

            if (rotationUpdate != null) {
                StopCoroutine(rotationUpdate);
                rotationUpdate = null;
            }
        }

        protected override void OnDetectPlayer(Player player) {
            player.Damage(CellType.Enemy_0);
        }
    }
}