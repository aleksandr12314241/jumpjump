using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Utility;
using Utility.Do;

namespace Gameplay {
    public class EnemyMover : MonoBehaviour, IEnemyMover {
        internal Action<Vector2Int, Vector2Int, Vector2Int> OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };

        private List<Vector2Int> path;
        private float speed;

        private Vector2Int startPoint;
        private Vector2Int currentCellIndex;

        private bool isMoved;
        private bool isCirclePath;

        private int currentPointIndex;
        private int newPointIndex;
        private Vector2Int direction;

        public Vector2Int Direction {
            get => direction;
        }
        public bool IsMoved {
            get => isMoved;
        }

        internal void Init(List<Vector2Int> path, float speed) {
            transform.DOKill();

            this.path = path;
            this.speed = speed;
            this.startPoint = path[0];
            this.currentCellIndex = this.startPoint;

            isMoved = path.Count >= 2;
            if (!isMoved) {
                return;
            }

            currentPointIndex = 0;
            newPointIndex++;
            Vector2Int startPoint = path[currentPointIndex];
            Vector2Int endPoint = path[path.Count - 1];

            Vector2Int diff = new Vector2Int(Math.Abs(startPoint.x - endPoint.x), Math.Abs(startPoint.y - endPoint.y));
            isCirclePath = diff.x <= 1 & diff.y <= 1;
        }

        public void StartMove() {
            //if (!isMoved) {
            //    return;
            //}
            MoveToNewPoint();
        }

        public void StopMove() {
            transform.DOKill();
        }

        void IEnemyMover.SetToStartCellIndex() {
        }

        public void SetCurrentCellIndex(Vector2Int cellIndex) {
            currentCellIndex = cellIndex;
        }

        private void MoveToNewPoint() {
            transform.DOKill();

            if (newPointIndex < 0 && newPointIndex >= path.Count) {
                currentPointIndex = 0;
                newPointIndex++;
            }

            Vector3 newPoint = LevelUtility.GetCellPositionByIndexOffset1(path[newPointIndex]);
            direction = path[newPointIndex] - path[currentPointIndex];
            //this.CustomInvoke(speed / 2f, SetCurrentPoint);

            transform.DOScale(transform.localScale, speed / 2f).OnComplete(() => {
                //OnChangeCellIndex();
                SetCurrentPoint();
            });

            transform.DOMove(newPoint, speed).OnComplete(() => {
                MoveToNewPoint();
            });
        }

        private void SetCurrentPoint() {
            OnChangeCellIndex(path[newPointIndex], path[currentPointIndex], direction);

            SetCurrentCellIndex(path[newPointIndex]);

            if (isCirclePath) {
                newPointIndex++;
                if (newPointIndex >= path.Count) {
                    newPointIndex = 0;
                }
                currentPointIndex = newPointIndex;
            }
            else {
                //�������� � ������� ����������� ������� ����
                if (newPointIndex > currentPointIndex) {
                    newPointIndex++;

                    if (newPointIndex >= path.Count) {
                        currentPointIndex = path.Count - 1;
                        newPointIndex = path.Count - 2;
                    }
                }
                else {
                    newPointIndex--;
                    if (newPointIndex < 0) {
                        currentPointIndex = 0;
                        newPointIndex = 1;
                    }
                }
            }
        }

        internal void RemoveAllListeners() {
            OnChangeCellIndex = (Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) => { };
        }
    }
}