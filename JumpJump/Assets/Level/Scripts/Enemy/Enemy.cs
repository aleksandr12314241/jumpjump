using System;
using UnityEngine;
using Zenject;

namespace Gameplay {
    public abstract class Enemy : MonoBehaviour, IPoolable<Vector2Int, AboveCellData, IMemoryPool> {
        internal Action<CellType, Vector2Int, Vector2Int> OnChangeCellIndex = (CellType aboveCellType, Vector2Int newCellIndex, Vector2Int oldCellIndex) => { };

        [SerializeField]
        protected ColliderPlayerDetected colliderPlayerDetected;

        protected CellType cellType;

        protected Vector2Int startCellIndex;
        private IEnemyMover enemyMover;

        private IMemoryPool pool;

        internal CellType CellType {
            get => cellType;
        }

        protected abstract void Init(Vector2Int cellIndex, AboveCellData aboveCellData);
        protected abstract void Die();

        protected abstract void StartPlayAction();

        public void OnSpawned(Vector2Int cellIndex, AboveCellData aboveCellData, IMemoryPool pool) {
            this.pool = pool;
            startCellIndex = cellIndex;

            Init(cellIndex, aboveCellData);

            colliderPlayerDetected.OnDetectPlayer = (Player player) => { };
            colliderPlayerDetected.OnDetectPlayer += OnDetectPlayer;
        }

        internal virtual void Restart() {
        }

        protected virtual void OnDetectPlayer(Player player) {
        }

        internal void Dispose() {
            colliderPlayerDetected.OnDetectPlayer -= OnDetectPlayer;
            Die();
            pool.Despawn(this);
        }

        public void OnDespawned() {
            pool = null;
        }

        internal void StartPlay() {
            StartPlayAction();
        }

        internal virtual void RemoveAllListeners() {
            OnChangeCellIndex = (CellType aboveCellType, Vector2Int newCellIndex, Vector2Int oldCellIndex) => { };
        }

        public class Enemy_0Factory : PlaceholderFactory<Vector2Int, AboveCellData, Enemy_0> {
        }
        public class Enemy_0Pool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, Enemy_0> {
        }

        public class WallMovingFactory : PlaceholderFactory<Vector2Int, AboveCellData, WallMovingEnemy> {
        }
        public class WallMovingPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, WallMovingEnemy> {
        }

        public class EnemyShooterFactory : PlaceholderFactory<Vector2Int, AboveCellData, EnemyShooter> {
        }
        public class EnemyShooterPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, EnemyShooter> {
        }

        public class EnemyTwistingFactory : PlaceholderFactory<Vector2Int, AboveCellData, EnemyTwisting> {
        }
        public class EnemyTwistingPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, EnemyTwisting> {
        }

        public class EnemySpikedFactory : PlaceholderFactory<Vector2Int, AboveCellData, EnemySpiked> {
        }
        public class EnemySpikedPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, EnemySpiked> {
        }

        public class WallPushingFactory : PlaceholderFactory<Vector2Int, AboveCellData, WallPushing> {
        }
        public class WallPushingPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, WallPushing> {
        }

        public class CoinFactory : PlaceholderFactory<Vector2Int, AboveCellData, Coin> {
        }
        public class CoinPool : MonoPoolableMemoryPool<Vector2Int, AboveCellData, IMemoryPool, Coin> {
        }
    }
}