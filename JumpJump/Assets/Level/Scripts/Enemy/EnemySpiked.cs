using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Gameplay {

    public class EnemySpiked : Enemy {
        [SerializeField]
        private ColliderPlayerDetected right;
        [SerializeField]
        private ColliderPlayerDetected up;
        [SerializeField]
        private ColliderPlayerDetected left;
        [SerializeField]
        private ColliderPlayerDetected leftLeft;
        [SerializeField]
        private ColliderPlayerDetected leftDown;
        [SerializeField]
        private ColliderPlayerDetected down;
        [SerializeField]
        private ColliderPlayerDetected rightDown;
        [SerializeField]
        private ColliderPlayerDetected rightRight;

        private EnemySpikedData[] enemySpikedDatas;

        private float delayTime;
        private List<int> indexShowSpike;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.EnemySpiked;

            AboveCellDataEnemySpiked aboveCellDataEnemySpiked = (AboveCellDataEnemySpiked)aboveCellData;

            delayTime = aboveCellDataEnemySpiked.delayTime;

            enemySpikedDatas = new EnemySpikedData[8];
            indexShowSpike = new List<int>();

            enemySpikedDatas[0].spike = right;
            enemySpikedDatas[0].isActive = aboveCellDataEnemySpiked.right;
            enemySpikedDatas[0].startPosition = right.transform.localPosition;

            enemySpikedDatas[1].spike = up;
            enemySpikedDatas[1].isActive = aboveCellDataEnemySpiked.up;
            enemySpikedDatas[1].startPosition = up.transform.localPosition;

            enemySpikedDatas[2].spike = left;
            enemySpikedDatas[2].isActive = aboveCellDataEnemySpiked.left;
            enemySpikedDatas[2].startPosition = left.transform.localPosition;

            enemySpikedDatas[3].spike = leftLeft;
            enemySpikedDatas[3].isActive = aboveCellDataEnemySpiked.left_left;
            enemySpikedDatas[3].startPosition = leftLeft.transform.localPosition;

            enemySpikedDatas[4].spike = leftDown;
            enemySpikedDatas[4].isActive = aboveCellDataEnemySpiked.leftDown;
            enemySpikedDatas[4].startPosition = leftDown.transform.localPosition;

            enemySpikedDatas[5].spike = down;
            enemySpikedDatas[5].isActive = aboveCellDataEnemySpiked.down;
            enemySpikedDatas[5].startPosition = down.transform.localPosition;

            enemySpikedDatas[6].spike = rightDown;
            enemySpikedDatas[6].isActive = aboveCellDataEnemySpiked.rightDown;
            enemySpikedDatas[6].startPosition = rightDown.transform.localPosition;

            enemySpikedDatas[7].spike = rightRight;
            enemySpikedDatas[7].isActive = aboveCellDataEnemySpiked.right_right;
            enemySpikedDatas[7].startPosition = rightRight.transform.localPosition;

            for (int i = 0; i < enemySpikedDatas.Length; i++) {
                enemySpikedDatas[i].spike.gameObject.SetActive(enemySpikedDatas[i].isActive);
                if (enemySpikedDatas[i].isActive) {
                    indexShowSpike.Add(i);

                    enemySpikedDatas[i].spike.OnDetectPlayer = (Player player) => { };
                    enemySpikedDatas[i].spike.OnDetectPlayer += OnDetectPlayer;
                }
            }

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
        }

        protected override void StartPlayAction() {
            HideImmediately();

            if (indexShowSpike.Count <= 0) {
                return;
            }
            ShowSpike();
        }

        protected override void Die() {
            for (int i = 0; i < indexShowSpike.Count; i++) {
                int index = indexShowSpike[i];
                enemySpikedDatas[index].spike.OnDetectPlayer -= OnDetectPlayer;
                enemySpikedDatas[index].spike.OnDetectPlayer = (Player player) => { };
            }
        }


        internal void ShowSpike() {
            float animTimeShow = (delayTime / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow / 4f).OnComplete(() => {
                for (int i = 0; i < indexShowSpike.Count; i++) {
                    int index = indexShowSpike[i];
                    enemySpikedDatas[index].spike.ActivateCollider();
                }

                transform.DOMove(transform.position, (animTimeShow - (animTimeShow / 4f))).OnComplete(WaitShowSpike);
            });

            for (int i = 0; i < indexShowSpike.Count; i++) {
                int index = indexShowSpike[i];
                enemySpikedDatas[index].spike.gameObject.SetActive(true);
                enemySpikedDatas[index].spike.DOKill();
                enemySpikedDatas[index].spike.transform.DOLocalMove(enemySpikedDatas[index].startPosition, animTimeShow);
            }
        }

        private void WaitShowSpike() {
            transform.DOKill();
            transform.DOMove(transform.position, (delayTime / 2f)).OnComplete(HideSpike);
        }
        private void HideSpike() {
            float animTimeShow = (delayTime / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow - (animTimeShow / 4f)).OnComplete(() => {
                for (int i = 0; i < indexShowSpike.Count; i++) {
                    int index = indexShowSpike[i];
                    enemySpikedDatas[index].spike.DiActivateCollider();
                }

                transform.DOMove(transform.position, (animTimeShow / 4f)).OnComplete(() => {
                    for (int i = 0; i < indexShowSpike.Count; i++) {
                        int index = indexShowSpike[i];
                        enemySpikedDatas[index].spike.DOKill();
                        enemySpikedDatas[index].spike.gameObject.SetActive(false);
                    }

                    WaitHideSpike();
                });
            });

            for (int i = 0; i < indexShowSpike.Count; i++) {
                int index = indexShowSpike[i];
                enemySpikedDatas[index].spike.DOKill();
                enemySpikedDatas[index].spike.transform.DOLocalMove(Vector2.zero, animTimeShow);
            }
        }
        private void WaitHideSpike() {
            transform.DOKill();
            transform.DOMove(transform.position, (delayTime / 2f)).OnComplete(ShowSpike);
        }

        private void HideImmediately() {
            for (int i = 0; i < enemySpikedDatas.Length; i++) {
                enemySpikedDatas[i].spike.DOKill();
                enemySpikedDatas[i].spike.gameObject.SetActive(false);
                enemySpikedDatas[i].spike.transform.localPosition = new Vector3(0f, enemySpikedDatas[i].spike.transform.localPosition.y, 0f);
                enemySpikedDatas[i].spike.DiActivateCollider();
            }
        }

        protected override void OnDetectPlayer(Player player) {
            player.Damage(CellType.Enemy_0);
        }

        private struct EnemySpikedData {
            internal ColliderPlayerDetected spike;
            internal bool isActive;
            internal Vector3 startPosition;

        }
    }
}