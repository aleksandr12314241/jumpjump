using UnityEngine;
using Zenject;
using DG.Tweening;

namespace Gameplay {

    public class Bullet : MonoBehaviour, IPoolable<Vector2Int, Vector2Int, int, float, IMemoryPool> {
        [SerializeField]
        private ColliderPlayerDetected colliderPlayerDetected;
        [SerializeField]
        private Transform bulletModelTransform;

        private Vector3 endPosition;

        private IMemoryPool pool;

        public void OnSpawned(Vector2Int cellIndex, Vector2Int direction, int lengthFly, float speedFly, IMemoryPool pool) {
            this.pool = pool;

            colliderPlayerDetected.OnDetectPlayer = (Player player) => { };
            colliderPlayerDetected.OnDetectPlayer += OnDetectPlayer;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);

            //if (direction.x == 0 && direction.y == 1) {
            //    bulletModelTransform.localEulerAngles = new Vector3(bulletModelTransform.localEulerAngles.x, 90f, bulletModelTransform.localEulerAngles.z);
            //}

            Vector2Int endCell = cellIndex + (direction * lengthFly);
            endPosition = LevelUtility.GetCellPositionByIndexOffset1(endCell);

            transform.DOMove(endPosition, speedFly).OnComplete(Dispose);
        }

        internal void Dispose() {
            transform.DOKill();
            colliderPlayerDetected.OnDetectPlayer -= OnDetectPlayer;
            pool.Despawn(this);
        }

        public void OnDespawned() {
            pool = null;
        }

        private void OnDetectPlayer(Player player) {
            player.Damage(CellType.EnemyShooter);
        }

        public class BulletFactory : PlaceholderFactory<Vector2Int, Vector2Int, int, float, Bullet> {
        }
        public class BulletPool : MonoPoolableMemoryPool<Vector2Int, Vector2Int, int, float, IMemoryPool, Bullet> {
        }
    }
}