using UnityEngine;

namespace Gameplay {
    public interface IEnemyMover {
        public void StartMove();
        public void StopMove();
        public void SetCurrentCellIndex(Vector2Int cellIndex);
        public void SetToStartCellIndex();
    }
}
