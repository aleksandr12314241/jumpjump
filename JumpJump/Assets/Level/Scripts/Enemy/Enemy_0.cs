using System;
using UnityEngine;
using Zenject;

namespace Gameplay {
    [RequireComponent(typeof(EnemyMover))]
    public class Enemy_0 : Enemy {
        private EnemyMover enemyMover;
        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.Enemy_0;

            AboveCellDataEnemy_0 cellDataEnemy_0 = (AboveCellDataEnemy_0)aboveCellData;
            enemyMover = GetComponent<EnemyMover>();
            enemyMover.Init(cellDataEnemy_0.path, cellDataEnemy_0.speed);
            enemyMover.RemoveAllListeners();
            enemyMover.OnChangeCellIndex += ChangeCellIndex;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
        }

        protected override void StartPlayAction() {
            enemyMover.StartMove();
        }

        protected override void Die() {
            enemyMover.StopMove();
        }

        private void ChangeCellIndex(Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) {
            OnChangeCellIndex(CellType.Enemy_0, newCellIndex, oldCellIndex);
        }

        protected override void OnDetectPlayer(Player player) {
            player.Damage(CellType.Enemy_0);
        }
    }
}