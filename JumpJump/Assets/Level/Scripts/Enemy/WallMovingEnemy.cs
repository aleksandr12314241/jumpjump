using UnityEngine;
using DG.Tweening;

namespace Gameplay {
    [RequireComponent(typeof(EnemyMover))]
    public class WallMovingEnemy : Enemy {
        private EnemyMover enemyMover;
        private float startDelay;
        private float speed;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.WallMoving;

            AboveCellDataWallMoving cellDataWallMoving = (AboveCellDataWallMoving)aboveCellData;
            enemyMover = GetComponent<EnemyMover>();

            startDelay = cellDataWallMoving.startDelay;
            speed = cellDataWallMoving.speed;

            enemyMover.Init(cellDataWallMoving.path, cellDataWallMoving.speed);
            enemyMover.RemoveAllListeners();
            enemyMover.OnChangeCellIndex += ChangeCellIndex;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
        }

        protected override void StartPlayAction() {
            transform.DOMove(transform.position, startDelay).OnComplete(() => {
                enemyMover.StartMove();
            });
        }

        protected override void Die() {
            enemyMover.StopMove();
        }

        private void ChangeCellIndex(Vector2Int newCellIndex, Vector2Int oldCellIndex, Vector2Int direction) {
            OnChangeCellIndex(CellType.WallMoving, newCellIndex, oldCellIndex);
        }

        protected override void OnDetectPlayer(Player player) {
            if (!enemyMover.IsMoved) {
                return;
            }

            player.Push(enemyMover.Direction, speed / 2f);
        }
    }
}
