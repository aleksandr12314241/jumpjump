using UnityEngine;
using System.Collections;
using Zenject;

namespace Gameplay {
    public class EnemyShooter : Enemy {
        private Coroutine shootUpdate = null;

        Vector2Int myCellIndex;
        Vector2Int direction;
        private float delayShoot;
        private float speedShoot;
        private int shootRange;

        [Inject]
        private readonly Bullet.BulletFactory bulletFactory;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.EnemyShooter;

            AboveCellDataEnemyShooter cellDataEnemyShooter = (AboveCellDataEnemyShooter)aboveCellData;

            myCellIndex = cellIndex;
            direction = new Vector2Int(cellDataEnemyShooter.directionX, cellDataEnemyShooter.directionY);
            delayShoot = cellDataEnemyShooter.delayShoot;
            speedShoot = cellDataEnemyShooter.speedShoot;
            shootRange = cellDataEnemyShooter.shootRange;

            float rotation = 0;
            if (direction.x == 0 && direction.y == 1) {
                rotation = -90f;
            }
            else if (direction.x == -1 && direction.y == 0) {
                rotation = -180f;
            }
            else if (direction.x == 0 && direction.y == 1) {
                rotation = -270f;
            }

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
            transform.localEulerAngles = new Vector3(0f, rotation, 0f);
        }

        protected override void StartPlayAction() {
            if (shootUpdate != null) {
                StopCoroutine(shootUpdate);
                shootUpdate = null;
            }
            shootUpdate = StartCoroutine(ShootUpdate());
        }

        protected override void Die() {
            if (shootUpdate != null) {
                StopCoroutine(shootUpdate);
                shootUpdate = null;
            }
        }

        private IEnumerator ShootUpdate() {
            for (; ; ) {
                yield return new WaitForSeconds(delayShoot);
                Shoot();
            }
        }

        private void Shoot() {
            bulletFactory.Create(myCellIndex, direction, shootRange, speedShoot);
        }

        protected override void OnDetectPlayer(Player player) {
            player.Damage(CellType.Enemy_0);
        }
    }
}