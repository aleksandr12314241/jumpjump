using UnityEngine;

namespace Gameplay {
    public class Coin : Enemy {
        private int coinValue;
        private Vector2Int cellIndex;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            this.cellIndex = cellIndex;

            AboveCellDataCoin aboveCellDataCoin = (AboveCellDataCoin)aboveCellData;

            coinValue = aboveCellDataCoin.value;

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);
        }

        protected override void Die() {
        }

        protected override void StartPlayAction() {
        }

        protected override void OnDetectPlayer(Player player) {
            player.CollectCoin(coinValue, cellIndex);

            gameObject.SetActive(false);
        }

        internal override void Restart() {
            gameObject.SetActive(true);
        }
    }
}