using UnityEngine;
using DG.Tweening;
using System;

namespace Gameplay {
    public class WallPushing : Enemy {
        internal Action<CellType, Vector2Int> OnSetCellAboveType = (CellType cellType, Vector2Int cellIndex) => { };
        internal Action<CellType, Vector2Int> OnUnSetCellAboveType = (CellType cellType, Vector2Int cellIndex) => { };

        [SerializeField]
        private ColliderPlayerDetected wallPushungPlayerDetected;
        [SerializeField]
        private SkinnedMeshRenderer mySkinnedMeshRenderer;
        private Tween skinnedMeshRendererTween;

        private float startDelay;
        private float frequency;
        private Vector2Int direction;

        private Vector3 wallPushingStartPosition;
        private Vector3 wallPushingEndPosition;

        private const float startPosition = 0.396f;
        private const float endPosition = 2.10f;

        private const float startPosSpikeBlendShape = 0f;
        private const float endPosSpikeBlendShape = 100f;

        private int currentBendShapeWeight = 0;

        protected override void Init(Vector2Int cellIndex, AboveCellData aboveCellData) {
            cellType = CellType.WallPushing;

            AboveCellDataWallPushing aboveCellDataWallPushing = (AboveCellDataWallPushing)aboveCellData;

            startDelay = aboveCellDataWallPushing.startDelay;
            frequency = aboveCellDataWallPushing.frequency;

            switch (aboveCellDataWallPushing.wallPushingDirectionType) {
                case AboveCellDataWallPushing.WallPushingDirectionType.Left:
                    direction = new Vector2Int(0, 1);
                    wallPushingStartPosition = new Vector3(0f, 0f, startPosition);
                    wallPushingEndPosition = new Vector3(0f, 0f, endPosition);
                    wallPushungPlayerDetected.transform.localEulerAngles = new Vector3(0f, -90f, 0f);
                    //mySkinnedMeshRenderer.transform.localEulerAngles = new Vector3(mySkinnedMeshRenderer.transform.localEulerAngles.x, mySkinnedMeshRenderer.transform.localEulerAngles.y, 270f);
                    currentBendShapeWeight = 0;
                    break;
                case AboveCellDataWallPushing.WallPushingDirectionType.Right:
                    direction = new Vector2Int(1, 0);
                    wallPushingStartPosition = new Vector3(startPosition, 0f, 0f);
                    wallPushingEndPosition = new Vector3(endPosition, 0f, 0f);
                    wallPushungPlayerDetected.transform.localEulerAngles = Vector3.zero;
                    //mySkinnedMeshRenderer.transform.localEulerAngles = new Vector3(mySkinnedMeshRenderer.transform.localEulerAngles.x, mySkinnedMeshRenderer.transform.localEulerAngles.y, 270f);
                    currentBendShapeWeight = 1;
                    break;
                case AboveCellDataWallPushing.WallPushingDirectionType.LeftDown:
                    direction = new Vector2Int(-1, 0);
                    wallPushingStartPosition = new Vector3(-startPosition, 0f, 0f);
                    wallPushingEndPosition = new Vector3(-endPosition, 0f, 0f);
                    wallPushungPlayerDetected.transform.localEulerAngles = Vector3.zero;
                    //mySkinnedMeshRenderer.transform.localEulerAngles = new Vector3(mySkinnedMeshRenderer.transform.localEulerAngles.x, mySkinnedMeshRenderer.transform.localEulerAngles.y, 90f);
                    currentBendShapeWeight = 1;
                    break;
                case AboveCellDataWallPushing.WallPushingDirectionType.RightDown:
                    direction = new Vector2Int(0, -1);
                    wallPushingStartPosition = new Vector3(0f, 0f, -startPosition);
                    wallPushingEndPosition = new Vector3(0f, 0f, -endPosition);
                    wallPushungPlayerDetected.transform.localEulerAngles = new Vector3(0f, -90f, 0f);
                    //mySkinnedMeshRenderer.transform.localEulerAngles = new Vector3(mySkinnedMeshRenderer.transform.localEulerAngles.x, mySkinnedMeshRenderer.transform.localEulerAngles.y, 90f);
                    currentBendShapeWeight = 0;
                    break;
            }

            transform.position = LevelUtility.GetCellPositionByIndexOffset1(cellIndex);

            wallPushungPlayerDetected.OnDetectPlayer = (Player player) => { };
            wallPushungPlayerDetected.OnDetectPlayer += OnDetectPlayer;
        }

        protected override void StartPlayAction() {
            UnPushHideImmediately();

            transform.DOKill();
            transform.DOMove(transform.position, startDelay).OnComplete(Push);
        }

        private void Push() {
            float animTimeShow = (frequency / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow / 4f).OnComplete(() => {
                wallPushungPlayerDetected.ActivateCollider();

                OnSetCellAboveType(CellType.WallPushing, startCellIndex + direction);
            });

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween =
                DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(currentBendShapeWeight), x => mySkinnedMeshRenderer.SetBlendShapeWeight(currentBendShapeWeight, x),
                endPosSpikeBlendShape,
                animTimeShow);

            wallPushungPlayerDetected.transform.DOKill();
            wallPushungPlayerDetected.transform.DOLocalMove(wallPushingEndPosition, animTimeShow).OnComplete(WaitPush);
        }
        private void WaitPush() {
            transform.DOKill();
            transform.DOMove(transform.position, (frequency / 2f)).OnComplete(UnPush);
        }

        private void UnPush() {
            float animTimeShow = (frequency / 2f);

            transform.DOKill();
            transform.DOMove(transform.position, animTimeShow - (animTimeShow / 4f)).OnComplete(() => {
                wallPushungPlayerDetected.DiActivateCollider();

                OnUnSetCellAboveType(cellType, startCellIndex + direction);
            });

            skinnedMeshRendererTween.Kill();
            skinnedMeshRendererTween =
                DOTween.To(() => mySkinnedMeshRenderer.GetBlendShapeWeight(currentBendShapeWeight), x => mySkinnedMeshRenderer.SetBlendShapeWeight(currentBendShapeWeight, x),
                startPosSpikeBlendShape,
                animTimeShow);

            wallPushungPlayerDetected.transform.DOKill();
            wallPushungPlayerDetected.transform.DOLocalMove(wallPushingStartPosition, animTimeShow).OnComplete(WaitUnPush);
        }
        private void WaitUnPush() {
            transform.DOKill();
            transform.DOMove(transform.position, (frequency / 2f)).OnComplete(Push);
        }

        internal override void RemoveAllListeners() {
            base.RemoveAllListeners();

            OnSetCellAboveType = (CellType cellType, Vector2Int cellIndex) => { };
            OnUnSetCellAboveType = (CellType cellType, Vector2Int cellIndex) => { };
        }

        protected void UnPushHideImmediately() {
            wallPushungPlayerDetected.DiActivateCollider();
            wallPushungPlayerDetected.transform.localPosition = wallPushingStartPosition;
        }

        protected override void Die() {
            RemoveAllListeners();

            wallPushungPlayerDetected.OnDetectPlayer -= OnDetectPlayer;
            wallPushungPlayerDetected.OnDetectPlayer = (Player player) => { };
        }

        protected override void OnDetectPlayer(Player player) {
            player.Push(direction, frequency / 2f);
        }
    }
}