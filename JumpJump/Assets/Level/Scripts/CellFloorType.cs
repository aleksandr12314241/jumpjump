namespace Gameplay {
    public enum CellFloorType {
        Void = 0,
        Empty = 1,
        Spike = 2,
        End = 3
    }
}