using UnityEngine;

namespace Gameplay {
    public static class LevelUtility {
        private static readonly float step = 1.2f;
        internal static Vector3 GetCellPositionByIndex( Vector2Int cellIndex ) {
            return new Vector3( cellIndex.x * step, 0f, cellIndex.y * step );
        }

        internal static Vector3 GetCellPositionByIndexOffset1( Vector2Int cellIndex ) {
            return new Vector3( cellIndex.x * step, 1f, cellIndex.y * step );
        }
    }
}