using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    [System.Serializable]
    public class ProfileToSave {
        [SerializeField]
        public int level = 0;
        [SerializeField]
        public int coins = 0;

        [SerializeField]
        public SkinType currentSkin;
        [SerializeField]
        public List<SkinType> availableSkins;

        [SerializeField]
        public bool soundOn;
        [SerializeField]
        public string language;

        public ProfileToSave() {
            level = 0;
            coins = 10;

            currentSkin = SkinType.Common;
            availableSkins = new List<SkinType>() { SkinType.Common };

            soundOn = true;
            language = LanguageType.Ru.ToString();
        }

        public ProfileToSave(Profile profile) {
            level = profile.level;
            coins = profile.coins;
            currentSkin = profile.currentSkin;
            availableSkins = profile.availableSkins;
            soundOn = profile.soundOn;
            language = profile.language.ToString();
        }
    }
}
