using Zenject;

namespace Common {

    public class LevelChooseService {
        private int currentLevelIndex;

        internal int CurrentLevelIndex {
            get => currentLevelIndex;
        }

        internal void SetCurrentLevelIndex( int currentLevelIndex ) {
            this.currentLevelIndex = currentLevelIndex;
        }

        internal void AddCurrentLevelIndex() {
            currentLevelIndex++;
        }
    }
}