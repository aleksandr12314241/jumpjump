using UnityEngine;
using Zenject;

namespace Common {
    [CreateAssetMenu(fileName = "CommonDataSetsInstaller", menuName = "JumpJump/CommonDataSetsInstaller")]
    public class CommonDataSetInstaller : ScriptableObjectInstaller<CommonDataSetInstaller> {
        [SerializeField]
        private SequenceLevelsDataSet sequenceLevelsDataSet;
        [SerializeField]
        private PrefabsDataSet prefabsDataSet;

        public override void InstallBindings() {
            Container.BindInstances(sequenceLevelsDataSet);
            Container.BindInstances(prefabsDataSet);
        }
    }
}