using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    [CreateAssetMenu(fileName = "PrefabsDataSet", menuName = "JumpJump/PrefabsDataSet", order = 1)]
    public class PrefabsDataSet : ScriptableObject {
        [SerializeField]
        internal CoinFly coinFlyPrf;
    }
}