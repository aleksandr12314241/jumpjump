using Gameplay;
using UnityEngine;

namespace Common {
    [CreateAssetMenu(fileName = "SequenceLevelsDataSet", menuName = "JumpJump/SequenceLevelsDataSet", order = 1)]
    public class SequenceLevelsDataSet : ScriptableObject {
        [SerializeField]
        private LevelDataSet emptyLevel;
        [SerializeField]
        private LevelInfoSequence[] levels;

        internal LevelInfoSequence[] Levels {
            get => levels;
        }

        internal Gameplay.LevelDataSet GetLevelDataSet(int numLevel) {
            if (numLevel < 0 && numLevel >= Levels.Length) {
                Debug.LogError("<SequenceLevelsDataSet>(SequenceLevelsDataSet) ��� ������ � ����� �������� " + numLevel);
                return emptyLevel;
            }

            if (Levels.Length <= 0) {
                Debug.LogError("<SequenceLevelsDataSet>(SequenceLevelsDataSet) ��� ������ �������. ������� ->>>>> SequenceLevelsDataSet ");
                return emptyLevel;
            }

            return Levels[numLevel].Level;
        }

        internal void SetLevels(LevelInfoSequence[] levels) {
            this.levels = levels;
        }

        [System.Serializable]
        public class LevelInfoSequence {
            [SerializeField]
            internal string levelName;
            [SerializeField]
            private LevelDataSet level;

            internal string LevelName {
                get => levelName;
            }

            internal LevelDataSet Level {
                get => level;
            }

            internal void SetLevel(LevelDataSet level) {
                this.level = level;
            }
        }
    }
}

