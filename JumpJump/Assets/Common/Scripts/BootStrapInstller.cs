using Zenject;

namespace Common {
    public class BootStrapInstller : MonoInstaller {
        public override void InstallBindings() {
            Container.Bind<Profile>().AsSingle().NonLazy();
            Container.Bind<Tutorial.TutorialProfile>().AsSingle().NonLazy();
            Container.Bind<LevelChooseService>().AsSingle();
        }
    }
}