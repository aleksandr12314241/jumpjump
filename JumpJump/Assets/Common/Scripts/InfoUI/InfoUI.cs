using UnityEngine;
using UnityEngine.UI;
using System;

namespace Common {
    //WindowMonoBehaviour
    public class InfoUI : MonoBehaviour {
        internal Action OnShowInfo = () => { };
        internal Action OnHideInfo = () => { };

        [SerializeField]
        private Image back;
        [SerializeField]
        private RectTransform myRectTransform;
        [SerializeField]
        private Image backImg;
        [SerializeField]
        private Text _infoText;
        [SerializeField]
        private Animator _myAnimator;
        [SerializeField]
        private GameObject blockObject;
        [SerializeField]
        private RectTransform tailUp;
        [SerializeField]
        private RectTransform tailDown;
        [SerializeField]
        private RectTransform panelRectTransform;

        private Vector2 rangeXTailDown;
        private Vector2 rangeXTailUp;

        private Camera camera;

        private bool isShown;

        private void Start() {

            RuntimeAnimatorController _runtimeAnimatorController = _myAnimator.runtimeAnimatorController;
            AnimationClip[] clips = _runtimeAnimatorController.animationClips;

            rangeXTailDown = new Vector2(-524f, 524f);
            rangeXTailUp = new Vector2(-366f, 524f);

            camera = Camera.main;

            Hide();
        }

        private void Update() {
            if (!isShown) {
                return;
            }

            if (Input.GetMouseButtonDown(0)) {
                StartHide();
            }
        }

        internal void ShowInfoMousePosition(string textInfo, float offset = 0f, BlockType blockType = BlockType.None) {
            Vector2 mousPosition = Input.mousePosition;
            Vector2 ancoredPositionOnScreen;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(myRectTransform, mousPosition, camera, out ancoredPositionOnScreen);
            TailType tailType = TailType.None;

            if (ancoredPositionOnScreen.y >= 0) {
                tailType = TailType.Up;
                ancoredPositionOnScreen = new Vector2(ancoredPositionOnScreen.x, ancoredPositionOnScreen.y - offset);
            }
            else {
                tailType = TailType.Down;
                ancoredPositionOnScreen = new Vector2(ancoredPositionOnScreen.x, ancoredPositionOnScreen.y + offset);
            }

            ShowInfoWithTailAndPositionY(textInfo, ancoredPositionOnScreen.y, blockType, tailType, ancoredPositionOnScreen.x, true);
        }
        internal void ShowInfo(string textInfo, BlockType blockType = BlockType.None) {
            ShowInfoWithTailAndPositionY(textInfo, 0f, blockType, TailType.None, 0f, true);
        }

        internal void ShowInfoWithPosition(string textInfo, float panelPosY, BlockType blockType = BlockType.None) {
            ShowInfoWithTailAndPositionY(textInfo, panelPosY, blockType, TailType.None, 0f, true);
        }

        internal void ShowInfoWithTail(string textInfo, BlockType blockType, TailType tailType, float tailPosX) {
            ShowInfoWithTailAndPositionY(textInfo, 0f, blockType, tailType, tailPosX, true);
        }

        private void StartShow() {
            OnShowInfo();

            Show();
        }

        private void Show() {
            back.gameObject.SetActive(true);
            panelRectTransform.gameObject.SetActive(true);

            isShown = true;
        }

        internal void ShowInfoWithTailAndPositionY(string textInfo, float panelPosY,
            BlockType blockType, TailType tailType, float tailPosX, bool isShowBack) {
            StartShow();

            _infoText.text = textInfo;

            panelRectTransform.anchoredPosition = new Vector2(0f, panelPosY);

            ShowTail(tailType, tailPosX);

            if (blockType == BlockType.None) {
                blockObject.SetActive(false);
            }
            else {
                blockObject.SetActive(true);
            }

            back.gameObject.SetActive(isShowBack);
        }

        private void ShowTail(TailType tailType, float posX = 0f) {
            float posXTemp = posX;

            switch (tailType) {
                case TailType.Up:
                    tailUp.gameObject.SetActive(true);
                    tailDown.gameObject.SetActive(false);

                    if (posXTemp < rangeXTailUp.x) {
                        posXTemp = rangeXTailUp.x;
                    }
                    else if (posXTemp > rangeXTailUp.y) {
                        posXTemp = rangeXTailUp.y;
                    }
                    tailUp.anchoredPosition = new Vector2(posXTemp, tailUp.anchoredPosition.y);

                    break;
                case TailType.Down:
                    tailUp.gameObject.SetActive(false);
                    tailDown.gameObject.SetActive(true);

                    if (posXTemp < rangeXTailDown.x) {
                        posXTemp = rangeXTailDown.x;
                    }
                    else if (posXTemp > rangeXTailDown.y) {
                        posXTemp = rangeXTailDown.y;
                    }
                    tailDown.anchoredPosition = new Vector2(posXTemp, tailDown.anchoredPosition.y);

                    break;
                default:
                    tailUp.gameObject.SetActive(false);
                    tailDown.gameObject.SetActive(false);
                    break;
            }
        }

        private void StartHide() {
            //_myAnimator.SetTrigger( "startHide" );
            //Invoke( "HideInfo", _hideAnimationLength );

            OnHideInfo();

            OnShowInfo = () => { };
            OnHideInfo = () => { };

            Hide();
        }

        private void Hide() {
            back.gameObject.SetActive(false);
            panelRectTransform.gameObject.SetActive(false);
            blockObject.SetActive(false);

            isShown = false;
        }
    }
}