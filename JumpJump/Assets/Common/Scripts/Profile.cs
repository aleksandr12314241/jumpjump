using System;
using System.Collections.Generic;

namespace Common {
    public class Profile {
        public int level = 0;
        public int coins = 0;

        public SkinType currentSkin;
        public List<SkinType> availableSkins;

        public bool soundOn;
        public LanguageType language;

        public Profile() {
            //��������� ��������� �������������� � ProfileToSave
            ProfileToSave profileToSave = SaveSystem.LoadProfileToSave();

            SetProfile(profileToSave);
        }

        private void SetProfile(ProfileToSave profileToSave) {
            level = profileToSave.level;
            coins = profileToSave.coins;

            currentSkin = profileToSave.currentSkin;
            availableSkins = profileToSave.availableSkins;

            soundOn = profileToSave.soundOn;
            language = (LanguageType)Enum.Parse(typeof(LanguageType), profileToSave.language);
        }

        internal void TrySaveNewLevel(int currentLevelIndex) {
            if (currentLevelIndex <= level) {
                return;
            }
            level = currentLevelIndex;

            SaveSystem.SaveProfile(this);
        }

        internal void OnChangeSoundOn(bool soundOn) {
            if (this.soundOn == soundOn) {
                return;
            }

            this.soundOn = soundOn;

            SaveSystem.SaveProfile(this);
        }

        internal void AddSkin(SkinType skinType, bool isSetCurrentSkin = true) {
            bool isNeedSave = false;

            if (!availableSkins.Contains(skinType)) {
                availableSkins.Add(skinType);
                isNeedSave = true;
            }

            if (isSetCurrentSkin) {
                currentSkin = skinType;
                isNeedSave = true;
            }

            if (isNeedSave) {
                SaveSystem.SaveProfile(this);
            }
        }
    }
}