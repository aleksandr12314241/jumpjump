using System.IO;
using Utility;

namespace Common {
    public static class SaveSystem {
        internal static ProfileToSave LoadProfileToSave() {
            string savePath = SaveUtility.GetSavePath("save");
            if (!File.Exists(savePath)) {
                ProfileToSave profileToSave = new ProfileToSave();

                SaveProfile(profileToSave);
                return profileToSave;
            }
            else {
                ProfileToSave profileToSave = SaveUtility.LoadObject<ProfileToSave>("save");

                return profileToSave;
            }
        }

        internal static void SaveProfile(Profile profile) {
            ProfileToSave profileToSave = new ProfileToSave(profile);
            SaveProfile(profileToSave);
        }

        internal static void SaveProfile(ProfileToSave profileToSave) {
            SaveUtility.SaveObject("save", profileToSave);
        }

        internal static Tutorial.TutorialProfileToSave LoadTutorialProfileToSave() {
            string savePath = SaveUtility.GetSavePath("save_tutorial");
            Tutorial.TutorialProfileToSave tutorialProfileToSave;

            if (!File.Exists(savePath)) {
                tutorialProfileToSave = new Tutorial.TutorialProfileToSave();

                SaveTutorialProfile(tutorialProfileToSave);
                return tutorialProfileToSave;
            }
            else {
                tutorialProfileToSave = SaveUtility.LoadObject<Tutorial.TutorialProfileToSave>("save_tutorial");

                return tutorialProfileToSave;
            }
        }

        internal static void SavTutorialeProfile(Tutorial.TutorialProfile tutorialProfile) {
            Tutorial.TutorialProfileToSave tutorialProfileToSave = new Tutorial.TutorialProfileToSave(tutorialProfile);
            SaveTutorialProfile(tutorialProfileToSave);
        }

        internal static void SaveTutorialProfile(Tutorial.TutorialProfileToSave tutorialProfileToSave) {
            SaveUtility.SaveObject("save_tutorial", tutorialProfileToSave);
        }
    }
}