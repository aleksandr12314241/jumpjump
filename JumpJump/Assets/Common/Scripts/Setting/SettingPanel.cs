using UnityEngine;
using Menu;
using Zenject;
using System;

namespace Common {
    public class SettingPanel : MonoBehaviour, IInitializable, IDisposable {
        [SerializeField]
        private SettingItem[] settingItems;

        [Inject]
        private SignalBus signalBus;
        [Inject]
        private Profile profile;

        public void Initialize() {
            for (int i = 0; i < settingItems.Length; i++) {
                settingItems[i].Init();
            }

            settingItems[0].OnClickItem += AudioClick;

            AudioSettingItem audioSettingItem = (AudioSettingItem)settingItems[0];
            audioSettingItem.ChangeAudioIcon(profile.soundOn);
        }

        public void Dispose() {
            Debug.Log("Dispose SettingsPanel");
        }

        private void AudioClick() {
            bool soundOn = profile.soundOn;
            bool newSound = !soundOn;

            signalBus.Fire(new OnChangeSoundOn() {
                soundOn = newSound
            });
        }

        internal void OnChangeSoundOn(bool soundOn) {
            int soundVolume = soundOn ? 1 : 0;
            AudioListener.volume = soundVolume;

            AudioSettingItem audioSettingItem = (AudioSettingItem)settingItems[0];
            audioSettingItem.ChangeAudioIcon(soundOn);
        }
    }
}