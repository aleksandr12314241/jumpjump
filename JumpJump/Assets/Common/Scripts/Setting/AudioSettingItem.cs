using UnityEngine;

namespace Common {
    public class AudioSettingItem : SettingItem {
        [SerializeField]
        private GameObject[] iconImageGameObject;

        internal void ChangeAudioIcon(bool soundOn) {
            if (soundOn) {
                iconImageGameObject[0].gameObject.SetActive(true);
                iconImageGameObject[1].gameObject.SetActive(false);

                SetTextById("txt_menu_sound_on");
            }
            else {
                iconImageGameObject[0].gameObject.SetActive(false);
                iconImageGameObject[1].gameObject.SetActive(true);

                SetTextById("txt_menu_sound_off");
            }
        }
    }
}