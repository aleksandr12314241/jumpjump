using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Common {
    [RequireComponent(typeof(Button))]
    public class SettingItem : MonoBehaviour {
        internal Action OnClickItem = () => { };

        [SerializeField]
        Button settingButton;
        //[SerializeField]
        //TextPlaceholder textPlaceholder;
        [SerializeField]
        private Text text;

#if UNITY_EDITOR
        private void OnValidate() {
            if (settingButton == null) {
                settingButton = GetComponent<Button>();
            }
        }
#endif

        internal void Init() {
            if (settingButton == null) {
                settingButton = GetComponent<Button>();
            }

            settingButton.onClick.AddListener(ClickItem);
        }

        void ClickItem() {
            OnClickItem();
        }

        //internal void SetTextById(string textId) {
        //    textPlaceholder.SetTextById(textId);
        //}

        internal void SetTextById(string textId) {
            text.text = textId;
        }
    }
}