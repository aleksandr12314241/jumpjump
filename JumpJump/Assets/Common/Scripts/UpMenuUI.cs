using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Common;
using System;

namespace Common {
    public class UpMenuUI : MonoBehaviour {
        [SerializeField]
        private bool isShowOnStart = true;
        [SerializeField]
        private RectTransform upMenuContainerRT;
        [SerializeField]
        private RectTransform coinImageRT;
        [SerializeField]
        private Text coinCountText;

        [Inject]
        private Profile profile;

        private int coins = 0;

        private RectTransform myRectTransform;

        internal RectTransform MyRectTransform {
            get => myRectTransform;
        }
        internal RectTransform CoinImageRT {
            get => coinImageRT;
        }

        private void Start() {
            myRectTransform = GetComponent<RectTransform>();

            coins = profile.coins;

            UpdateCoinCount();

            if (!isShowOnStart) {
                HideUpMenu();
            }
        }

        private void ShowUpMenu() {
            upMenuContainerRT.DOKill();

            upMenuContainerRT.DOAnchorPosY(0f, 0.5f);
        }

        private void HideUpMenu() {
            upMenuContainerRT.DOKill();

            upMenuContainerRT.DOAnchorPosY(100f, 0.5f);
        }

        internal void UpdateCoinCount() {
            coinCountText.text = coins.ToString();
        }

        internal void AddCoinCount(int additionalCoins) {
            coins += additionalCoins;

            //TODO: анимация 
            UpdateCoinCount();
        }

        internal void CollectCoinLevel(int additionalCoins) {
            AddCoinCount(additionalCoins);

            //ShowUpMenu();

            //transform.DOKill();
            //transform.DOScale(Vector3.one, 1.5f).OnComplete(() => {
            //    HideUpMenu();
            //});
        }

        internal void Restart() {
            coins = profile.coins;

            UpdateCoinCount();
        }
    }
}