namespace Tools {
    public static class LevelEditorPath {
        internal static readonly string levelExpand = ".asset";
        internal static readonly string levelFolder = "Assets/Level/DataSet/Levels/";
        internal static readonly string fieldCellsDataSetPath = "Assets/Tools/LevelEditor/DataSet/FieldCellsDataSet.asset";
        internal static readonly string levelDataSetPath = "Assets/Tools/LevelEditor/DataSet/LevelDataSet.asset";
    }
}