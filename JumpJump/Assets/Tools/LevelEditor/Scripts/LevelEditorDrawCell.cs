#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Gameplay;
using System;

namespace Tools.LevelEditor {
    public class LevelEditorDrawCell {
        private LevelEditorWindow levelEditorWindow;

        public LevelEditorDrawCell(LevelEditorWindow levelEditorWindow) {
            this.levelEditorWindow = levelEditorWindow;
        }

        internal void DrawSpike(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataSpike aboveCellDataSpike = AboveCellDataSpike.GetAboveCellDataSpike(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataSpike.startDelay.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "��������� ��������:", levelEditorWindow.cellInfoString);
            float newStartDelay;

            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newStartDelay)) {
                infoNumber = "���������";
                aboveCellDataSpike.startDelay = newStartDelay;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            levelEditorWindow.cellInfoString = aboveCellDataSpike.frequency.ToString();
            float newFrequency;
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + (stepPosY * 3), 200f, 20f), "�������:", levelEditorWindow.cellInfoString);
            if (float.TryParse(levelEditorWindow.cellInfoString, out newFrequency)) {
                infoNumber = "���������";
                aboveCellDataSpike.frequency = newFrequency;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 4), 200f, 20f), infoNumber);

            cellData.aboveCellDataToSave = aboveCellDataSpike.GetAboveCellDataToSave();
        }

        internal void DrawEnemyInfo(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;

            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataEnemy_0 aboveCellDataEnemy_0 = AboveCellDataEnemy_0.GetAboveCellDataEnemy_0(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataEnemy_0.speed.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "��������:", levelEditorWindow.cellInfoString);

            float newSpeed;

            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newSpeed)) {
                infoNumber = "���������";
                aboveCellDataEnemy_0.speed = newSpeed;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            Rect pathToggleRect = new Rect(20f, startPosY + (stepPosY * 3), 100f, 20f);

            bool newIsPathFill = GUI.Toggle(pathToggleRect, levelEditorWindow.isPathFill, "����");

            if (newIsPathFill != levelEditorWindow.isPathFill && newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();

                for (int i = 0; i < aboveCellDataEnemy_0.path.Count; i++) {
                    levelEditorWindow.path.Add(aboveCellDataEnemy_0.path[i]);
                }
            }

            if (levelEditorWindow.isPathFill) {
                if (levelEditorWindow.path.Count != aboveCellDataEnemy_0.path.Count && levelEditorWindow.path.Count > 0) {
                    aboveCellDataEnemy_0.path = new List<Vector2Int>();
                    for (int i = 0; i < levelEditorWindow.path.Count; i++) {
                        aboveCellDataEnemy_0.path.Add(levelEditorWindow.path[i]);
                    }
                }

                string lastPathIndex = aboveCellDataEnemy_0.path.Count.ToString() + " - �����.; " + aboveCellDataEnemy_0.path[aboveCellDataEnemy_0.path.Count - 1].x + "_" +
                    aboveCellDataEnemy_0.path[aboveCellDataEnemy_0.path.Count - 1].y;

                EditorGUI.LabelField(new Rect(pathToggleRect.x + pathToggleRect.width + 10f, pathToggleRect.y, 100f, pathToggleRect.height), lastPathIndex);
            }

            if (levelEditorWindow.isPathFill && !newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();
            }

            levelEditorWindow.isPathFill = newIsPathFill;

            cellData.aboveCellDataToSave = aboveCellDataEnemy_0.GetAboveCellDataToSave();
        }

        internal void DrawDestroyedCell(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataDestroyedOnStep aboveCellDataDestroyed = AboveCellDataDestroyedOnStep.GetAboveCellDataDestroyed(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataDestroyed.timeDestroyed.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "���������� �����, ���:", levelEditorWindow.cellInfoString);

            float newTimeDestroyed;

            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newTimeDestroyed)) {
                infoNumber = "���������";
                aboveCellDataDestroyed.timeDestroyed = newTimeDestroyed;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            cellData.aboveCellDataToSave = aboveCellDataDestroyed.GetAboveCellDataToSave();
        }

        internal void DrawPortalCell(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataPortal aboveCellDataPortal = AboveCellDataPortal.GetAboveCellDataPortal(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataPortal.otherCellX.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "������ X:", levelEditorWindow.cellInfoString);
            int newOtherCellX;

            string infoNumber;
            if (int.TryParse(levelEditorWindow.cellInfoString, out newOtherCellX)) {
                infoNumber = "���������";
                aboveCellDataPortal.otherCellX = newOtherCellX;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            levelEditorWindow.cellInfoString = aboveCellDataPortal.otherCellY.ToString();
            int newOtherCellY;
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + (stepPosY * 2), 200f, 20f), "������ Y:", levelEditorWindow.cellInfoString);
            if (int.TryParse(levelEditorWindow.cellInfoString, out newOtherCellY)) {
                infoNumber = "���������";
                aboveCellDataPortal.otherCellY = newOtherCellY;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 3), 200f, 20f), infoNumber);

            Vector2Int exitCell = new Vector2Int(newOtherCellX, newOtherCellY);
            CellType cellType = levelEditorWindow.levelDataSet.GetCellType(exitCell);
            if (!cellType.Equals(CellType.Portal)) {
                EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 4), 200f, 20f), "��� �� ������");
            }

            levelEditorWindow.portal = new Vector2Int[2];
            levelEditorWindow.portal[0] = cellIndex;
            levelEditorWindow.portal[1] = new Vector2Int(aboveCellDataPortal.otherCellX, aboveCellDataPortal.otherCellY);

            cellData.aboveCellDataToSave = aboveCellDataPortal.GetAboveCellDataToSave();
        }

        internal void DrawWallMoving(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;

            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataWallMoving aboveCellDataWallMoving = AboveCellDataWallMoving.GetAboveCellDataWallMoving(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataWallMoving.startDelay.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "��������� ��������:", levelEditorWindow.cellInfoString);

            string infoNumber;

            float newStartDelay;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newStartDelay)) {
                infoNumber = "���������";
                aboveCellDataWallMoving.startDelay = newStartDelay;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            levelEditorWindow.cellInfoString = aboveCellDataWallMoving.speed.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, (startPosY + (stepPosY * 2f)), 200f, 20f), "��������:", levelEditorWindow.cellInfoString);
            float newSpeed;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newSpeed)) {
                infoNumber = "���������";
                aboveCellDataWallMoving.speed = newSpeed;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 3), 200f, 20f), infoNumber);

            Rect pathToggleRect = new Rect(20f, startPosY + (stepPosY * 4), 100f, 20f);
            bool newIsPathFill = GUI.Toggle(pathToggleRect, levelEditorWindow.isPathFill, "����");
            if (newIsPathFill != levelEditorWindow.isPathFill && newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();

                for (int i = 0; i < aboveCellDataWallMoving.path.Count; i++) {
                    levelEditorWindow.path.Add(aboveCellDataWallMoving.path[i]);
                }
            }

            if (levelEditorWindow.isPathFill) {
                if (levelEditorWindow.path.Count != aboveCellDataWallMoving.path.Count && levelEditorWindow.path.Count > 0) {
                    aboveCellDataWallMoving.path = new List<Vector2Int>();
                    for (int i = 0; i < levelEditorWindow.path.Count; i++) {
                        aboveCellDataWallMoving.path.Add(levelEditorWindow.path[i]);
                    }
                }

                string lastPathIndex = aboveCellDataWallMoving.path.Count.ToString() + " - �����.; " + aboveCellDataWallMoving.path[aboveCellDataWallMoving.path.Count - 1].x + "_" +
                    aboveCellDataWallMoving.path[aboveCellDataWallMoving.path.Count - 1].y;

                EditorGUI.LabelField(new Rect(pathToggleRect.x + pathToggleRect.width + 10f, pathToggleRect.y, 100f, pathToggleRect.height), lastPathIndex);
            }

            if (levelEditorWindow.isPathFill && !newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();
            }

            levelEditorWindow.isPathFill = newIsPathFill;

            cellData.aboveCellDataToSave = aboveCellDataWallMoving.GetAboveCellDataToSave();
        }

        internal void DrawEmptyMoving(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;

            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataEmptyMoving aboveCellDataEmptyMoving = AboveCellDataEmptyMoving.GetAboveCellDataEmptyMoving(aboveCellDataToSave);

            string infoNumber;

            levelEditorWindow.cellInfoString = aboveCellDataEmptyMoving.speed.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, (startPosY + stepPosY), 200f, 20f), "��������:", levelEditorWindow.cellInfoString);
            float newSpeed;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newSpeed)) {
                infoNumber = "���������";
                aboveCellDataEmptyMoving.speed = newSpeed;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 2f), 200f, 20f), infoNumber);

            Rect pathToggleRect = new Rect(20f, startPosY + (stepPosY * 3f), 100f, 20f);
            bool newIsPathFill = GUI.Toggle(pathToggleRect, levelEditorWindow.isPathFill, "����");
            if (newIsPathFill != levelEditorWindow.isPathFill && newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();

                for (int i = 0; i < aboveCellDataEmptyMoving.path.Count; i++) {
                    levelEditorWindow.path.Add(aboveCellDataEmptyMoving.path[i]);
                }
            }

            if (levelEditorWindow.isPathFill) {
                if (levelEditorWindow.path.Count != aboveCellDataEmptyMoving.path.Count && levelEditorWindow.path.Count > 0) {
                    aboveCellDataEmptyMoving.path = new List<Vector2Int>();
                    for (int i = 0; i < levelEditorWindow.path.Count; i++) {
                        aboveCellDataEmptyMoving.path.Add(levelEditorWindow.path[i]);
                    }
                }

                string lastPathIndex = aboveCellDataEmptyMoving.path.Count.ToString() + " - �����.; " + aboveCellDataEmptyMoving.path[aboveCellDataEmptyMoving.path.Count - 1].x + "_" +
                    aboveCellDataEmptyMoving.path[aboveCellDataEmptyMoving.path.Count - 1].y;

                EditorGUI.LabelField(new Rect(pathToggleRect.x + pathToggleRect.width + 10f, pathToggleRect.y, 100f, pathToggleRect.height), lastPathIndex);
            }

            if (levelEditorWindow.isPathFill && !newIsPathFill) {
                levelEditorWindow.path = new List<Vector2Int>();
            }

            levelEditorWindow.isPathFill = newIsPathFill;

            cellData.aboveCellDataToSave = aboveCellDataEmptyMoving.GetAboveCellDataToSave();
        }

        internal void DrawEnemyShooter(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataEnemyShooter aboveCellDataEnemyShooter = AboveCellDataEnemyShooter.GetAboveCellDataEnemyShooter(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataEnemyShooter.delayShoot.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "��������, ���:", levelEditorWindow.cellInfoString);
            float newDelayShoot;
            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newDelayShoot)) {
                infoNumber = "���������";
                aboveCellDataEnemyShooter.delayShoot = newDelayShoot;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            //EditorGUI.LabelField(new Rect(20f, startPosY + stepPosY, 200f, 20f), infoNumber);

            levelEditorWindow.cellInfoString = aboveCellDataEnemyShooter.speedShoot.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, (startPosY + stepPosY), 200f, 20f), "�������� ��������, ���:", levelEditorWindow.cellInfoString);
            float newSpeedShoot;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newSpeedShoot)) {
                infoNumber = "���������";
                aboveCellDataEnemyShooter.speedShoot = newSpeedShoot;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            //EditorGUI.LabelField(new Rect(20f, (startPosY + stepPosY * 3), 200f, 20f), infoNumber);

            levelEditorWindow.cellInfoString = aboveCellDataEnemyShooter.shootRange.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + (stepPosY * 2), 200f, 20f), "�������� �������, ������:", levelEditorWindow.cellInfoString);
            int newShootRange;
            if (int.TryParse(levelEditorWindow.cellInfoString, out newShootRange)) {
                infoNumber = "���������";
                aboveCellDataEnemyShooter.shootRange = newShootRange;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }
            //EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 5), 200f, 20f), infoNumber);

            Vector2Int direction = new Vector2Int(aboveCellDataEnemyShooter.directionX, aboveCellDataEnemyShooter.directionY);
            var text = new string[] { "����.", "���.", "��� ���.", "��� ����." };

            int currectDirection = 0;
            if (direction.x == 1 && direction.y == 0) {
                currectDirection = 0;
            }
            else if (direction.x == 0 && direction.y == 1) {
                currectDirection = 1;
            }
            else if (direction.x == -1 && direction.y == 0) {
                currectDirection = 2;
            }
            else if (direction.x == 0 && direction.y == -1) {
                currectDirection = 3;
            }
            int newCurrentCellTypeIndex = GUI.SelectionGrid(new Rect(20f, startPosY + (stepPosY * 3), 200f, 40f), currectDirection, text, 3, EditorStyles.radioButton);

            Vector2Int newDirection = new Vector2Int(1, 0);
            switch (newCurrentCellTypeIndex) {
                case 0:
                    newDirection = new Vector2Int(1, 0);
                    break;
                case 1:
                    newDirection = new Vector2Int(0, 1);
                    break;
                case 2:
                    newDirection = new Vector2Int(-1, 0);
                    break;
                case 3:
                    newDirection = new Vector2Int(0, -1);
                    break;
            }
            aboveCellDataEnemyShooter.directionX = newDirection.x;
            aboveCellDataEnemyShooter.directionY = newDirection.y;

            cellData.aboveCellDataToSave = aboveCellDataEnemyShooter.GetAboveCellDataToSave();
        }

        internal void DrawEnemyTwisting(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataEnemyTwisting aboveCellDataEnemyTwisting = AboveCellDataEnemyTwisting.GetAboveCellDataEnemyTwisting(aboveCellDataToSave);

            aboveCellDataEnemyTwisting.clockwise = GUI.Toggle(new Rect(20f, startPosY, 200f, 20f), aboveCellDataEnemyTwisting.clockwise, "�� ������� �������");

            levelEditorWindow.cellInfoString = aboveCellDataEnemyTwisting.speedRoatation.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + startPosY, 200f, 20f), "�������� ��������, ���:", levelEditorWindow.cellInfoString);
            float newSpeedRoatation;
            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newSpeedRoatation)) {
                infoNumber = "���������";
                aboveCellDataEnemyTwisting.speedRoatation = newSpeedRoatation;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            cellData.aboveCellDataToSave = aboveCellDataEnemyTwisting.GetAboveCellDataToSave();
        }

        internal void DrawEnemySpiked(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataEnemySpiked aboveCellDataEnemySpiked = AboveCellDataEnemySpiked.GetAboveCellDataEnemySpiked(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataEnemySpiked.delayTime.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "��������, ���:", levelEditorWindow.cellInfoString);
            float newDelayTime;
            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newDelayTime)) {
                infoNumber = "���������";
                aboveCellDataEnemySpiked.delayTime = newDelayTime;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            float startPosY_toggle = startPosY + stepPosY;

            //aboveCellDataEnemySpiked.right = GUI.Toggle(new Rect(20f, startPosY_toggle, 100f, 20f), aboveCellDataEnemySpiked.right, "����. �����");
            //aboveCellDataEnemySpiked.up = GUI.Toggle(new Rect(120f, startPosY_toggle, 100f, 20f), aboveCellDataEnemySpiked.up, "�����");
            //aboveCellDataEnemySpiked.left = GUI.Toggle(new Rect(20f, startPosY_toggle + stepPosY, 100f, 20f), aboveCellDataEnemySpiked.left, "���. �����");
            //aboveCellDataEnemySpiked.left_left = GUI.Toggle(new Rect(120f, startPosY_toggle + stepPosY, 100f, 20f), aboveCellDataEnemySpiked.left_left, "���.");
            //aboveCellDataEnemySpiked.leftDown = GUI.Toggle(new Rect(20f, startPosY_toggle + (stepPosY * 2f), 100f, 20f), aboveCellDataEnemySpiked.leftDown, "���. ���");
            //aboveCellDataEnemySpiked.down = GUI.Toggle(new Rect(120f, startPosY_toggle + (stepPosY * 2f), 100f, 20f), aboveCellDataEnemySpiked.down, "���");
            //aboveCellDataEnemySpiked.rightDown = GUI.Toggle(new Rect(20f, startPosY_toggle + (stepPosY * 3f), 100f, 20f), aboveCellDataEnemySpiked.rightDown, "����. ���");
            //aboveCellDataEnemySpiked.right_right = GUI.Toggle(new Rect(120f, startPosY_toggle + (stepPosY * 3f), 100f, 20f), aboveCellDataEnemySpiked.right_right, "����.");

            aboveCellDataEnemySpiked.up = GUI.Toggle(new Rect(70f, startPosY_toggle, 100f, 20f), aboveCellDataEnemySpiked.up, "�����");
            aboveCellDataEnemySpiked.right = GUI.Toggle(new Rect(120f, startPosY_toggle + stepPosY, 100f, 20f), aboveCellDataEnemySpiked.right, "����. �����");
            aboveCellDataEnemySpiked.left = GUI.Toggle(new Rect(20f, startPosY_toggle + stepPosY, 100f, 20f), aboveCellDataEnemySpiked.left, "���. �����");

            aboveCellDataEnemySpiked.left_left = GUI.Toggle(new Rect(0f, startPosY_toggle + (stepPosY * 2f), 100f, 20f), aboveCellDataEnemySpiked.left_left, "���.");
            aboveCellDataEnemySpiked.right_right = GUI.Toggle(new Rect(140f, startPosY_toggle + (stepPosY * 2f), 100f, 20f), aboveCellDataEnemySpiked.right_right, "����.");

            aboveCellDataEnemySpiked.leftDown = GUI.Toggle(new Rect(20f, startPosY_toggle + (stepPosY * 3f), 100f, 20f), aboveCellDataEnemySpiked.leftDown, "���. ���");
            aboveCellDataEnemySpiked.rightDown = GUI.Toggle(new Rect(120f, startPosY_toggle + (stepPosY * 3f), 100f, 20f), aboveCellDataEnemySpiked.rightDown, "����. ���");
            aboveCellDataEnemySpiked.down = GUI.Toggle(new Rect(70f, startPosY_toggle + (stepPosY * 4f), 100f, 20f), aboveCellDataEnemySpiked.down, "���");

            cellData.aboveCellDataToSave = aboveCellDataEnemySpiked.GetAboveCellDataToSave();
        }

        internal void DrawWallPushing(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataWallPushing aboveCellDataWallPushing = AboveCellDataWallPushing.GetAboveCellDataWallPushing(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataWallPushing.startDelay.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "���. ��������, ���:", levelEditorWindow.cellInfoString);
            float newStartDelay;
            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newStartDelay)) {
                infoNumber = "���������";
                aboveCellDataWallPushing.startDelay = newStartDelay;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            levelEditorWindow.cellInfoString = aboveCellDataWallPushing.frequency.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + stepPosY, 200f, 20f), "�������, ���:", levelEditorWindow.cellInfoString);
            float newFrequency;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newFrequency)) {
                infoNumber = "���������";
                aboveCellDataWallPushing.frequency = newFrequency;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            string[] enumstrings = Enum.GetNames(typeof(AboveCellDataWallPushing.WallPushingDirectionType));
            int directionType = (int)aboveCellDataWallPushing.wallPushingDirectionType;
            int newDirectionType = GUI.SelectionGrid(new Rect(20f, startPosY + (stepPosY * 2), 200f, 40f), directionType, enumstrings, 2, EditorStyles.radioButton);
            aboveCellDataWallPushing.wallPushingDirectionType = (AboveCellDataWallPushing.WallPushingDirectionType)newDirectionType;

            cellData.aboveCellDataToSave = aboveCellDataWallPushing.GetAboveCellDataToSave();
        }

        internal void DrawSpring(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataSpring aboveCellDataSpring = AboveCellDataSpring.GetAboveCellDataSpring(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataSpring.frequency.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "������� �����, ���:", levelEditorWindow.cellInfoString);
            float newFrequency;
            string infoNumber;
            if (float.TryParse(levelEditorWindow.cellInfoString, out newFrequency)) {
                infoNumber = "���������";
                aboveCellDataSpring.frequency = newFrequency;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            EditorGUI.LabelField(new Rect(20f, startPosY + (stepPosY * 1), 200f, 20f), "����� �������");
            levelEditorWindow.cellInfoString = "";
            for (int i = 0; i < aboveCellDataSpring.countDiscardCells.Length; i++) {
                levelEditorWindow.cellInfoString += aboveCellDataSpring.countDiscardCells[i].ToString();

                if (i < aboveCellDataSpring.countDiscardCells.Length - 1) {
                    levelEditorWindow.cellInfoString += ",";
                }
            }
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY + (stepPosY * 2), 200f, 20f), "���-�� �����:", levelEditorWindow.cellInfoString);
            string[] countDiscardCellsStr = levelEditorWindow.cellInfoString.Split(',');
            List<int> countCellsTemp = new List<int>();
            for (int i = 0; i < countDiscardCellsStr.Length; i++) {
                int countDiscard;
                if (int.TryParse(countDiscardCellsStr[i], out countDiscard)) {
                    countCellsTemp.Add(countDiscard);
                }
            }

            if (countCellsTemp.Count > 0) {
                aboveCellDataSpring.countDiscardCells = new int[countCellsTemp.Count];
                for (int i = 0; i < countCellsTemp.Count; i++) {
                    aboveCellDataSpring.countDiscardCells[i] = countCellsTemp[i];
                }
            }

            Vector2Int direction = new Vector2Int(aboveCellDataSpring.directionX, aboveCellDataSpring.directionY);
            var text = new string[] { "���.", "����.", "��� ���.", "��� ����." };

            int currectDirection = 0;
            if (direction.x == 0 && direction.y == 1) {
                currectDirection = 0;
            }
            else if (direction.x == 1 && direction.y == 0) {
                currectDirection = 1;
            }
            else if (direction.x == -1 && direction.y == 0) {
                currectDirection = 2;
            }
            else if (direction.x == 0 && direction.y == -1) {
                currectDirection = 3;
            }
            int newCurrentCellTypeIndex = GUI.SelectionGrid(new Rect(20f, startPosY + (stepPosY * 3), 200f, 40f), currectDirection, text, 2, EditorStyles.radioButton);

            Vector2Int newDirection = new Vector2Int(1, 0);
            switch (newCurrentCellTypeIndex) {
                case 0:
                    newDirection = new Vector2Int(0, 1);
                    break;
                case 1:
                    newDirection = new Vector2Int(1, 0);
                    break;
                case 2:
                    newDirection = new Vector2Int(-1, 0);
                    break;
                case 3:
                    newDirection = new Vector2Int(0, -1);
                    break;
            }
            aboveCellDataSpring.directionX = newDirection.x;
            aboveCellDataSpring.directionY = newDirection.y;

            cellData.aboveCellDataToSave = aboveCellDataSpring.GetAboveCellDataToSave();
        }

        internal void DrawCoin(Rect cellInfoRect, Vector2Int cellIndex) {
            float startPosY = 60f;
            float stepPosY = 20f;
            LevelDataSet.CellData cellData = levelEditorWindow.levelDataSet.GetCellData(cellIndex);
            AboveCellDataToSave aboveCellDataToSave = cellData.aboveCellDataToSave;
            AboveCellDataCoin aboveCellDataCoin = AboveCellDataCoin.GetAboveCellDataCoin(aboveCellDataToSave);

            levelEditorWindow.cellInfoString = aboveCellDataCoin.value.ToString();
            levelEditorWindow.cellInfoString = EditorGUI.TextField(new Rect(20f, startPosY, 200f, 20f), "���������, ���:", levelEditorWindow.cellInfoString);
            int newValue;
            string infoNumber;
            if (int.TryParse(levelEditorWindow.cellInfoString, out newValue)) {
                infoNumber = "���������";
                aboveCellDataCoin.value = newValue;
            }
            else {
                infoNumber = "�� ���������. ����� �����";
            }

            cellData.aboveCellDataToSave = aboveCellDataCoin.GetAboveCellDataToSave();
        }
    }
}
#endif