#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Common;
using Gameplay;

namespace Tools.LevelEditor {
    public class LevelEditorSaveSystem {
        private LevelEditorWindow levelEditorWindow;
        public LevelEditorSaveSystem( LevelEditorWindow levelEditorWindow ) {
            this.levelEditorWindow = levelEditorWindow;
        }

        internal void SaveLevel( string levelName ) {
            if ( string.IsNullOrEmpty( levelName ) ) {
                EditorUtility.DisplayDialog( "", "������� ��� ������", "��" );
                return;
            }

            if ( levelEditorWindow.fileLevelNames.Contains( levelName ) ) {
                if ( EditorUtility.DisplayDialog( "", "������� � ����� ������ ��� ����������. ����������?", "��", "���, �� ������������" ) ) {
                    SaveLevelToFile( levelEditorWindow.levelDataSet, levelName );
                    levelEditorWindow.fileLevelNames.Add( levelName );
                }
                return;
            }

            if ( !levelEditorWindow.levelDataSet.IsContainsExit() ) {
                EditorUtility.DisplayDialog( "", "������� �� �������� ������. �������� ����� �� �������. (������ ������)", "��" );
                return;
            }

            SaveLevelToFile( levelEditorWindow.levelDataSet, levelName );
        }

        private void SaveLevelToFile( LevelDataSet levelDataSetFromEditor, string levelName ) {
            LevelDataSet levelDataSetToSave = ScriptableObject.CreateInstance<LevelDataSet>();
            levelDataSetToSave.CloneLevelDataSet( levelDataSetFromEditor );

            string pathToLevelFile = LevelEditorPath.levelFolder + levelName + LevelEditorPath.levelExpand;

            AssetDatabase.CreateAsset( levelDataSetToSave, pathToLevelFile );
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = levelDataSetToSave;

            levelEditorWindow.fileLevelNames.Add( levelName );

            List<int> indexForDelete = new List<int>();
            SequenceLevelsDataSet sequenceLevelsDataSet = levelEditorWindow.sequenceLevelsDataSet;
            int notNullElements = sequenceLevelsDataSet.Levels.Length;

            for ( int i = 0; i < sequenceLevelsDataSet.Levels.Length; i++ ) {
                if ( sequenceLevelsDataSet.Levels[i].Level == null ) {
                    notNullElements--;
                    continue;
                }

                if ( sequenceLevelsDataSet.Levels[i].Level.name.Equals( levelName ) ) {
                    sequenceLevelsDataSet.Levels[i].SetLevel( levelDataSetToSave );
                }
            }

            if ( notNullElements != sequenceLevelsDataSet.Levels.Length ) {
                SequenceLevelsDataSet.LevelInfoSequence[] levelsNew = new SequenceLevelsDataSet.LevelInfoSequence[notNullElements];
                int index = 0;
                for ( int i = 0; i < sequenceLevelsDataSet.Levels.Length; i++ ) {
                    if ( sequenceLevelsDataSet.Levels[i].Level == null ) {
                        continue;
                    }

                    levelsNew[index] = new SequenceLevelsDataSet.LevelInfoSequence();
                    levelsNew[index].levelName = sequenceLevelsDataSet.Levels[i].levelName;
                    levelsNew[index].SetLevel( sequenceLevelsDataSet.Levels[i].Level );
                    index++;
                }

                sequenceLevelsDataSet.SetLevels( levelsNew );
            }

            EditorUtility.DisplayDialog( "", "������� ������� ��������", "������" );
        }

        internal void LoadLevel( string levelName ) {
            if ( string.IsNullOrEmpty( levelName ) || !levelEditorWindow.fileLevelNames.Contains( levelName ) ) {
                EditorUtility.DisplayDialog( "", "������ � ����� ������ �� ����������", "��" );
                return;
            }

            if ( !levelEditorWindow.levelDataSet.IsEmptyLevel() ) {
                if ( EditorUtility.DisplayDialog( "", "������� ��������� ������� ����� ������. ���������?", "��", "���, �� ���������" ) ) {
                    LoadLevelFromFile( levelName );
                }
                return;
            }

            LoadLevelFromFile( levelName );
        }

        private void LoadLevelFromFile( string levelName ) {
            string pathToLevelFile = LevelEditorPath.levelFolder + levelName + LevelEditorPath.levelExpand;
            LevelDataSet levelDataSetTemp = AssetDatabase.LoadAssetAtPath<LevelDataSet>( pathToLevelFile );
            levelEditorWindow.cellsRaw = levelDataSetTemp.FieldSize;

            levelEditorWindow.levelDataSet.CloneLevelDataSet( levelDataSetTemp );
        }
    }
}
#endif