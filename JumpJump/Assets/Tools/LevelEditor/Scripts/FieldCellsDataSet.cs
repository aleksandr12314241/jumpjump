#if UNITY_EDITOR
using UnityEngine;
using System;
using Gameplay;

namespace Tools.LevelEditor {
    [CreateAssetMenu( fileName = "FieldCellsDataSet", menuName = "JumpJump/Tools/FieldCellsDataSet", order = 1 )]
    public class FieldCellsDataSet : ScriptableObject {
        [SerializeField]
        internal Sprite voidCellSprite;
        [SerializeField]
        internal Sprite fingerSprite;
        [SerializeField]
        internal CellData[] cells;

        internal CellData GetCellData( CellType cellType ) {
            for ( int i = 0; i < cells.Length; i++ ) {
                if ( cells[i].CellType == cellType ) {
                    return cells[i];
                }
            }

            return cells[0];
        }

        [Serializable]
        internal struct CellData {
            [SerializeField]
            internal CellType CellType;
            [SerializeField]
            internal Sprite CellSprites;
            [SerializeField]
            internal string CellDescription;
        }
    }
}
#endif