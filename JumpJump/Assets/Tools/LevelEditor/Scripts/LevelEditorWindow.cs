#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using Gameplay;
using System;
using Common;

namespace Tools.LevelEditor {
    public class LevelEditorWindow : EditorWindow {
        private const string Title = "Level Editor Window";

        private int currentCellTypeIndex = 0;
        private int currentCellTypeIndexWithOffset => currentCellTypeIndex - 1;
        private Vector2Int currentCellIndex;
        private Texture[] cellsTexture;
        internal SequenceLevelsDataSet sequenceLevelsDataSet;
        private FieldCellsDataSet fieldCellsDataSet;

        private Vector2 startFieldCellsPos;
        private Vector2 fieldCellsMouseClickPos;
        private bool isMousePressedOnFieldCells = false;
        private bool isFillFieldCells = false;

        private string choosenLevelName;
        internal List<string> fileLevelNames;
        private int fileLevelNameIndex = -1;

        private const float cellSize = 30f;
        internal int cellsRaw;
        private const int startCellsRaw = 3;
        private const float divWidthWindow = 1.55f;

        private bool isFirstUpdateGui = false;

        private ConfigDataSet configDataSet;
        internal LevelDataSet levelDataSet;

        internal List<Vector2Int> path;
        internal Vector2Int[] portal;

        internal bool isPathFill;
        internal string cellInfoString = "";
        internal bool isPortal;

        private GUIStyle guiStylePath = new GUIStyle(); //create a new variable

        private LevelEditorDrawCell levelEditorDrawCell;
        private LevelEditorSaveSystem levelEditorSaveSystem;

        [MenuItem("Tools/LevelEditorWindow")]
        public static void OpenEditorWindow() {
            var win = GetWindow<LevelEditorWindow>(false);
            win.titleContent.text = Title;

            win.position = new Rect(150f, 150f, win.position.width, win.position.height);
        }

        public void Awake() {
            levelEditorDrawCell = new LevelEditorDrawCell(this);
            levelEditorSaveSystem = new LevelEditorSaveSystem(this);

            fieldCellsDataSet = AssetDatabase.LoadAssetAtPath<FieldCellsDataSet>(LevelEditorPath.fieldCellsDataSetPath);
            sequenceLevelsDataSet = AssetDatabase.LoadAssetAtPath<SequenceLevelsDataSet>("Assets/Common/DataSet/SequenceLevelsDataSet.asset");

            cellsTexture = new Texture[fieldCellsDataSet.cells.Length + 1];
            cellsTexture[0] = fieldCellsDataSet.fingerSprite.texture;

            for (int i = 1; i < cellsTexture.Length; i++) {
                cellsTexture[i] = fieldCellsDataSet.cells[i - 1].CellSprites.texture;
            }

            fieldCellsMouseClickPos = Vector2.zero;
            fileLevelNames = GetLevelsFiles();
            cellsRaw = startCellsRaw;

            isFirstUpdateGui = true;

            configDataSet = Resources.Load<ConfigDataSet>("ConfigDataSet");

            levelDataSet = ScriptableObject.CreateInstance<LevelDataSet>();
            levelDataSet.Init(cellsRaw);

            currentCellIndex = new Vector2Int(-1, -1);

            if (configDataSet.IsEditorMode || configDataSet.IsEditorPlaying) {
                string levelDataSetPathTemp = configDataSet.GetPathToLevel();

                LevelDataSet levelDataSetTemp = AssetDatabase.LoadAssetAtPath<LevelDataSet>(levelDataSetPathTemp);

                if (levelDataSetTemp == null) {

                    levelDataSetTemp = ScriptableObject.CreateInstance<LevelDataSet>();
                    levelDataSetTemp.Init(cellsRaw);
                    AssetDatabase.CreateAsset(levelDataSetTemp, LevelEditorPath.levelDataSetPath);
                    AssetDatabase.SaveAssets();
                }

                levelDataSet.CloneLevelDataSet(levelDataSetTemp);
                cellsRaw = levelDataSet.FieldSize;

                for (int i = 0; i < fileLevelNames.Count; i++) {
                    if (fileLevelNames[i].Contains(configDataSet.EditroLevelName)) {
                        fileLevelNameIndex = i;
                        break;
                    }
                }

                if (!configDataSet.IsEditorPlaying) {
                    configDataSet.SetIsEditorMode(false);

                    configDataSet.SetEditorLevelName("");
                }
            }

            path = new List<Vector2Int>();
        }

        private void OnEnable() {
        }

        void OnDisable() {
        }

        void OnGUI() {
            if (isFirstUpdateGui) {
                isFirstUpdateGui = false;
                startFieldCellsPos = new Vector2(-position.width / 3f, -position.height - (position.height / 10f));
            }

            MainFieldCellsDraw();

            MenuDraw();
        }

        private List<string> GetLevelsFiles() {
            string pathToLevelFile = LevelEditorPath.levelFolder;
            var info = new DirectoryInfo(pathToLevelFile);
            var fileInfo = info.GetFiles();
            List<string> filesName = new List<string>();

            for (int i = 0; i < fileInfo.Length; i++) {
                FileInfo file = fileInfo[i];

                int indexStart = file.Name.Length - 4;
                string fileExpand = file.Name.Substring(indexStart);
                if (fileExpand.Equals("meta")) {
                    continue;
                }

                filesName.Add(file.Name.Substring(0, file.Name.Length - LevelEditorPath.levelExpand.Length));
            }

            return filesName;
        }

        private Rect GetSizeCellInfo(Rect fieldCellsRect) {
            float divCellInfo = 3f;

            return new Rect(
               fieldCellsRect.width - (fieldCellsRect.width / divCellInfo),
               fieldCellsRect.height - (fieldCellsRect.height / divCellInfo),
               fieldCellsRect.width / divCellInfo,
               fieldCellsRect.height / divCellInfo);
        }

        private void MainFieldCellsDraw() {
            //main field cells
            Rect fieldCellsRect = new Rect(0f, 0f, position.width / divWidthWindow, position.height);
            Vector2 offsetStartFieldCellsPos = MoveOffsetFieldCells(fieldCellsRect);

            //==========
            //TODO
            //if ( GUI.Button( new Rect( 0f, 0f, 80f, 50f ), "�������" ) ) {
            //    levelDataSet_Temp = ScriptableObject.CreateInstance<LevelDataSet>();
            //    levelDataSet_Temp.Init( cellsRaw );
            //
            //    levelDataSet_Temp.UpdateCellType( new Vector2Int( 1, 0 ), CellType.Empty );
            //    levelDataSet_Temp.UpdateCellType( new Vector2Int( 0, 1 ), CellType.Spike );
            //
            //    AssetDatabase.CreateAsset( levelDataSet_Temp, "Assets/Tools/LevelEditor/DataSet/LevelDataSet_Temp.asset" );
            //    AssetDatabase.SaveAssets();
            //    AssetDatabase.Refresh();
            //}
            //
            //if ( GUI.Button( new Rect( 90f, 0f, 80f, 50f ), "���������" ) ) {
            //    levelDataSet_Temp = AssetDatabase.LoadAssetAtPath<LevelDataSet>( "Assets/Tools/LevelEditor/DataSet/LevelDataSet_Temp.asset" );
            //
            //    for ( int i = 0; i < levelDataSet_Temp.CellDatas.Count; i++ ) {
            //        /*if ( levelDataSet_Temp.CellDatas[i].aboveCellData != null ) {
            //            AboveCellDataEmpty cellDataEmpty = (AboveCellDataEmpty)levelDataSet_Temp.CellDatas[i].aboveCellData;
            //            Debug.Log( levelDataSet_Temp.CellDatas[i].x + " _ " + levelDataSet_Temp.CellDatas[i].y + " = " + levelDataSet_Temp.CellDatas[i].CellType.ToString() + "  ;   ABOVE  = " + cellDataEmpty.upper );
            //        }
            //        else {
            //            Debug.Log( levelDataSet_Temp.CellDatas[i].x + " _ " + levelDataSet_Temp.CellDatas[i].y + " = " + levelDataSet_Temp.CellDatas[i].CellType.ToString() + "  ;   IS  NULL" );
            //        }*/
            //    }
            //}
            //==========

            GUI.BeginGroup(fieldCellsRect);
            GUI.Box(new Rect(0, 0, position.width / divWidthWindow, position.height), "Field cells");

            if (GUI.Button(new Rect(0f, fieldCellsRect.height - 25f, 100f, 25f), "������������")) {
                isFirstUpdateGui = true;
                return;
            }

            FieldCellsMoveDraw(fieldCellsRect, offsetStartFieldCellsPos);

            Rect cellInfoRect = GetSizeCellInfo(fieldCellsRect);

            FieldCellChooseInfo(cellInfoRect);

            if (isMousePressedOnFieldCells) {
                isFillFieldCells = false;
                Repaint();
            }

            GUI.EndGroup();
        }

        private void FieldCellsMoveDraw(Rect fieldCellsRect, Vector2 offsetStartFieldCellsPos) {
            Rect fieldCellsMoveRect = new Rect(
                                startFieldCellsPos.x + offsetStartFieldCellsPos.x, startFieldCellsPos.y + offsetStartFieldCellsPos.y,
                                fieldCellsRect.width * 2, fieldCellsRect.height * 2
                            );

            Rect cellInfoRect = GetSizeCellInfo(fieldCellsRect);
            Vector2 border = new Vector2(fieldCellsRect.width - cellInfoRect.width, fieldCellsRect.height - cellInfoRect.height);
            bool isClick = false;
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0 && (Event.current.mousePosition.x < border.x || Event.current.mousePosition.y < border.y)) {
                isClick = true;
            }

            GUI.BeginGroup(fieldCellsMoveRect);

            float startPosCellX = (fieldCellsMoveRect.width / 2f) - cellSize;
            float startPosCellY = fieldCellsMoveRect.height - cellSize;

            bool isDrawOutline = false;
            Rect rectImageOutline = new Rect(0f, 0f, 0f, 0f);

            for (int n = 0; n < cellsRaw; n++) {
                int countCells = n + 1;
                float startPosXWithOffset = startPosCellX - (n * (cellSize / 2));
                for (int m = 0; m < countCells; m++) {
                    Vector2Int cellIndex = new Vector2Int(m, n - m);

                    Rect rectImage = new Rect(startPosXWithOffset + (m * cellSize), startPosCellY - (n * cellSize), cellSize - 2.5f, cellSize);

                    CellType cellType = levelDataSet.GetCellType(cellIndex);

                    FieldCellsDataSet.CellData cellData = fieldCellsDataSet.GetCellData(cellType);

                    Debug.Log("cellIndex  " + cellIndex + " ,  rectImage " + rectImage);

                    if (currentCellIndex.x == cellIndex.x && currentCellIndex.y == cellIndex.y) {
                        rectImageOutline = new Rect(rectImage.x - 4f, rectImage.y - 4f, rectImage.width + 8f, rectImage.height + 8f);
                        isDrawOutline = true;
                    }

                    if (cellData.CellType.Equals(CellType.Void)) {
                        GUI.DrawTexture(rectImage, fieldCellsDataSet.voidCellSprite.texture, ScaleMode.ScaleToFit);
                    }
                    else {
                        GUI.DrawTexture(rectImage, cellData.CellSprites.texture, ScaleMode.ScaleToFit);
                    }
                }
            }

            DrawEnemyPath(startPosCellX, startPosCellY);
            DrawPortal(startPosCellX, startPosCellY);

            if (isDrawOutline) {
                GUI.DrawTexture(rectImageOutline, fieldCellsDataSet.voidCellSprite.texture, ScaleMode.ScaleToFit, true, 0f, Color.blue, 6f, 0f);
            }

            if (isClick) {
                FieldCellsMoveMouseClickHandler(startPosCellX, startPosCellY);
            }

            GUI.EndGroup();
        }

        private void DrawEnemyPath(float startPosCellX, float startPosCellY) {
            if (path != null && path.Count > 0) {

                Color textColor = GUI.color;
                GUI.color = Color.yellow;
                guiStylePath.fontSize = 20; //change the font size
                guiStylePath.fontStyle = FontStyle.Bold;

                for (int i = 0; i < path.Count; i++) {
                    //Vector2Int cellIndex = new Vector2Int( m, n - m );
                    int m = path[i].x;
                    int n = path[i].y + m;

                    float startPosXWithOffset = startPosCellX - (n * (cellSize / 2));
                    Rect rectImage = new Rect(startPosXWithOffset + (m * cellSize), startPosCellY - (n * cellSize), cellSize - 2.5f, cellSize);
                    GUI.Label(rectImage, i.ToString(), guiStylePath);
                }

                GUI.color = textColor;
            }
        }

        private void DrawPortal(float startPosCellX, float startPosCellY) {
            if (portal != null && portal.Length > 0) {

                Color textColor = GUI.color;
                GUI.color = Color.yellow;
                guiStylePath.fontSize = 20; //change the font size
                guiStylePath.fontStyle = FontStyle.Bold;

                for (int i = 0; i < portal.Length; i++) {
                    //Vector2Int cellIndex = new Vector2Int( m, n - m );
                    int m = portal[i].x;
                    int n = portal[i].y + m;

                    float startPosXWithOffset = startPosCellX - (n * (cellSize / 2));
                    Rect rectImage = new Rect(startPosXWithOffset + (m * cellSize), startPosCellY - (n * cellSize), cellSize - 2.5f, cellSize);
                    GUI.Label(rectImage, i.ToString(), guiStylePath);
                }

                GUI.color = textColor;
            }
        }

        private void FieldCellsMoveMouseClickHandler(float startPosCellX, float startPosCellY) {
            float startCellIndexN = (-(Event.current.mousePosition.y - startPosCellY) / (cellSize / 2));
            int cellIndexN = (startCellIndexN < 0) ? 0 : ((int)startCellIndexN / 2) + 1; // n

            float startPosXWithOffset = startPosCellX - (cellIndexN * (cellSize / 2));

            float startIndexX = (Event.current.mousePosition.x - startPosXWithOffset) / (cellSize / 2);

            int cellIndexM = ((startIndexX < 0 && startIndexX > -2) ? -1 : ((int)startIndexX / 2));

            Vector2Int cellIndex = new Vector2Int(cellIndexM, cellIndexN - cellIndexM);

            if (cellIndex.x < 0 || cellIndex.y < 0 || cellIndex.x + cellIndex.y >= cellsRaw) {
                return;
            }

            if (cellIndex.x == 0 && cellIndex.y == 0) {
                return;
            }

            if (isPathFill) {
                FiilPath(cellIndex);
            }
            else if (isPortal) {

            }
            else if (currentCellTypeIndex == 0) {
                ChooseCellIndex(cellIndex);
            }
            else {
                FillCell(cellIndex);
            }
        }

        private void SetCurrentCellIndex(Vector2Int cellIndex) {
            currentCellIndex = cellIndex;
            GUI.FocusControl(null);
            isPathFill = false;
        }

        private void FiilPath(Vector2Int cellIndex) {
            int lastIndex = path.Count - 1;
            Vector2Int pathLastIndex = path[lastIndex];

            Vector2Int diff = new Vector2Int(Math.Abs(cellIndex.x - pathLastIndex.x), Math.Abs(cellIndex.y - pathLastIndex.y));

            if (pathLastIndex.Equals(cellIndex)) {
                if (path.Count >= 2) {
                    path.RemoveAt(lastIndex);
                }
            }
            else if (diff.x <= 1 && diff.y <= 1) {
                path.Add(cellIndex);
            }

            Repaint();
        }

        private void ChooseCellIndex(Vector2Int cellIndex) {
            if (currentCellIndex.x < 0) {
                SetCurrentCellIndex(cellIndex);
            }
            else if (currentCellIndex.x == cellIndex.x && currentCellIndex.y == cellIndex.y) {
                SetCurrentCellIndex(new Vector2Int(-1, -1));
            }
            else {
                SetCurrentCellIndex(cellIndex);
            }

            Repaint();
        }

        private void FillCell(Vector2Int cellIndex) {

            if (currentCellTypeIndexWithOffset < 0) {
                return;
            }

            levelDataSet.UpdateCellType(cellIndex, fieldCellsDataSet.cells[currentCellTypeIndexWithOffset].CellType);

            SetCurrentCellIndex(cellIndex);
            Repaint();
        }

        private void FieldCellChooseInfo(Rect cellInfoRect) {
            GUI.BeginGroup(cellInfoRect);
            Color guiColor = GUI.color;
            GUI.color = Color.yellow;
            GUI.Box(new Rect(0f, 0f, cellInfoRect.width, cellInfoRect.height), "���������� � ������");
            GUI.color = guiColor;

            string indexInfo = currentCellIndex.x.ToString() + " _ " + currentCellIndex.y.ToString();
            GUI.Label(new Rect(0f, 10f, 200f, 25f), indexInfo);

            //if ( GUI.Button( new Rect( 0f, 0, 25f, 25f ), "��" ) ) {
            //}
            //Debug.Log( "currentCellIndex  " + currentCellIndex );

            if (currentCellIndex.x > -1 && currentCellIndex.y > -1) {
                CellType cellType = levelDataSet.GetCellType(currentCellIndex);

                FieldCellsDataSet.CellData cellData = fieldCellsDataSet.GetCellData(cellType);
                GUI.DrawTexture(new Rect(cellInfoRect.width - 30f, 20f, 30f, 30f), cellData.CellSprites.texture, ScaleMode.ScaleToFit);

                if (levelEditorDrawCell == null) {
                    levelEditorDrawCell = new LevelEditorDrawCell(this);
                }

                switch (cellType) {
                    case CellType.Spike:
                        levelEditorDrawCell.DrawSpike(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.Enemy_0:
                        levelEditorDrawCell.DrawEnemyInfo(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.DestroyedOnStep:
                        levelEditorDrawCell.DrawDestroyedCell(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.Portal:
                        levelEditorDrawCell.DrawPortalCell(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.WallMoving:
                        levelEditorDrawCell.DrawWallMoving(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.EmptyMoving:
                        levelEditorDrawCell.DrawEmptyMoving(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.EnemyShooter:
                        levelEditorDrawCell.DrawEnemyShooter(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.EnemyTwisting:
                        levelEditorDrawCell.DrawEnemyTwisting(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.EnemySpiked:
                        levelEditorDrawCell.DrawEnemySpiked(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.WallPushing:
                        levelEditorDrawCell.DrawWallPushing(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.Spring:
                        levelEditorDrawCell.DrawSpring(cellInfoRect, currentCellIndex);
                        break;
                    case CellType.Coin:
                        levelEditorDrawCell.DrawCoin(cellInfoRect, currentCellIndex);
                        break;
                }
            }
            GUI.EndGroup();
        }

        private Vector2 MoveOffsetFieldCells(Rect fieldCellsRect) {
            if (Event.current.type == EventType.MouseDown && Event.current.button == 2 && fieldCellsRect.Contains(Event.current.mousePosition)) {
                fieldCellsMouseClickPos = Event.current.mousePosition;
                isMousePressedOnFieldCells = true;
            }
            Vector2 offsetStartFieldCellsPos = isMousePressedOnFieldCells ? (Event.current.mousePosition - fieldCellsMouseClickPos) : new Vector2(0f, 0f);

            if (Event.current.type == EventType.MouseUp && Event.current.button == 2 && isMousePressedOnFieldCells) {
                isMousePressedOnFieldCells = false;
                startFieldCellsPos += offsetStartFieldCellsPos;
            }
            else if (Event.current.mousePosition.x < 0 || Event.current.mousePosition.y < 0 ||
                Event.current.mousePosition.x > position.width || Event.current.mousePosition.y > position.height && isMousePressedOnFieldCells) {
                isMousePressedOnFieldCells = false;
            }

            return offsetStartFieldCellsPos;
        }

        private void MenuDraw() {
            //menu field
            Rect menuRect = new Rect(position.width / divWidthWindow, 0f, position.width - (position.width / divWidthWindow), position.height);
            GUI.BeginGroup(menuRect);
            //GUI.Box( new Rect( 10f, 0f, position.width - (position.width / divWidthWindow) - 20f, position.height ), "Test" );
            GUI.Label(new Rect(menuRect.width / 2f, 0, menuRect.width - (menuRect.width / 2f), 25f), "Menu");

            float startPosY = 25f;
            MenuClearLevelButton(menuRect, startPosY);
            MenuChangeRawButton(startPosY);

            float divMenuField = 3f;
            ChangeCurrentCellTypeIndex(menuRect, divMenuField);

            MenuChooseCellTypeInfo(menuRect, divMenuField);
            MenuSaveLevel(menuRect, divMenuField);

            GUI.EndGroup();
        }

        private void ChangeCurrentCellTypeIndex(Rect menuRect, float divMenuField) {
            int newCurrentCellTypeIndex = GUI.SelectionGrid(new Rect(10f, 55f, menuRect.width - 20f, (menuRect.height / divMenuField)),
                                        currentCellTypeIndex, cellsTexture, 6);
            if (currentCellTypeIndex != newCurrentCellTypeIndex) {
                isPathFill = false;
                path = new List<Vector2Int>();
            }
            currentCellTypeIndex = newCurrentCellTypeIndex;
        }

        private void MenuClearLevelButton(Rect menuRect, float startPosY) {
            if (GUI.Button(new Rect(menuRect.width - 100f, startPosY, 100f, 25f), "�����")) {
                CreateNewLevel();
            }
        }

        private void MenuChangeRawButton(float startPosY) {
            if (GUI.Button(new Rect(0f, startPosY, 30f, 25f), "-")) {
                AddCellRaw(-1);
            }
            if (GUI.Button(new Rect(35f, startPosY, 30f, 25f), "+")) {
                AddCellRaw(1);
            }
            GUI.Label(new Rect(75f, startPosY, 200f, 25f), "����� = " + cellsRaw);
        }

        private void MenuChooseCellTypeInfo(Rect menuRect, float divMenuField) {
            GUI.BeginGroup(new Rect(0f, 60f + (menuRect.height / divMenuField), position.width - (position.width / divWidthWindow), (menuRect.height / divMenuField)));
            GUI.DrawTexture(new Rect(20f, 20f, 50f, 50f), cellsTexture[currentCellTypeIndex], ScaleMode.ScaleToFit);

            string description = "����� ������";
            if (currentCellTypeIndexWithOffset >= 0) {
                FieldCellsDataSet.CellData cellData = fieldCellsDataSet.cells[currentCellTypeIndexWithOffset];
                if (currentCellIndex.x > -1) {
                    CellType cellType = levelDataSet.GetCellType(currentCellIndex);

                    if (!cellData.CellType.Equals(cellType)) {
                        SetCurrentCellIndex(new Vector2Int(-1, -1));
                    }
                }

                description = cellData.CellDescription;
            }
            GUI.Label(new Rect(20f + 50f, 20f + 50f, position.width - (position.width / divWidthWindow), (menuRect.height / divMenuField) - (20f + 50f)), description);
            GUI.EndGroup();
        }

        private void MenuSaveLevel(Rect menuRect, float divMenuField) {
            GUI.BeginGroup(new Rect(0f, 50f + (menuRect.height / divMenuField) + (menuRect.height / divMenuField), (position.width - (position.width / divWidthWindow)) - 10f, (menuRect.height / divMenuField) - 60f));
            GUI.Box(new Rect(0f, 0f, position.width - (position.width / divWidthWindow) - 10f, (menuRect.height / divMenuField) - 60f), "");

            fileLevelNameIndex = EditorGUI.Popup(new Rect(0f, 0f, (position.width - (position.width / divWidthWindow)) - 10f, 20f), fileLevelNameIndex, fileLevelNames.ToArray());
            if (fileLevelNameIndex >= 0 && fileLevelNameIndex < fileLevelNames.Count) {
                choosenLevelName = fileLevelNames[fileLevelNameIndex];
            }
            choosenLevelName = EditorGUI.TextField(new Rect(0f, 20f, (position.width - (position.width / divWidthWindow)) - 10f, 20f), "��� ������:", choosenLevelName);
            if (fileLevelNameIndex >= 0 && fileLevelNameIndex < fileLevelNames.Count && !choosenLevelName.Equals(fileLevelNames[fileLevelNameIndex])) {
                fileLevelNameIndex = -1;
            }

            if (levelEditorSaveSystem == null) {
                levelEditorSaveSystem = new LevelEditorSaveSystem(this);
            }

            float sizeButton = menuRect.width / 4f;
            if (GUI.Button(new Rect(10f, ((menuRect.height / divMenuField) - 60f) - 30f, sizeButton, 20f), "���������")) {
                levelEditorSaveSystem.SaveLevel(choosenLevelName);
            }

            if (GUI.Button(new Rect(sizeButton + 20f, ((menuRect.height / divMenuField) - 60f) - 30f, sizeButton, 20f), "���������")) {
                levelEditorSaveSystem.LoadLevel(choosenLevelName);
            }

            if (GUI.Button(new Rect(sizeButton * 2f + 30f, ((menuRect.height / divMenuField) - 60f) - 30f, sizeButton, 20f), "������")) {
                PlayLevel(choosenLevelName);
            }

            GUI.EndGroup();
        }

        private void CreateNewLevel() {
            if (!levelDataSet.IsEmptyLevel()) {
                if (EditorUtility.DisplayDialog("", "������� ��������� ������� ����� ������. ������� �����?", "��", "���, �� ���������")) {
                    levelDataSet = ScriptableObject.CreateInstance<LevelDataSet>();
                    cellsRaw = startCellsRaw;
                    levelDataSet.Init(cellsRaw);
                }
                return;
            }

            levelDataSet = ScriptableObject.CreateInstance<LevelDataSet>();
            cellsRaw = startCellsRaw;
            levelDataSet.Init(cellsRaw);
        }

        private void AddCellRaw(int addedRaw) {
            cellsRaw += addedRaw;
            if (cellsRaw < 2) {
                cellsRaw = 2;
            }

            levelDataSet.UpdateFieldSize(cellsRaw);
        }

        private void PlayLevel(string levelName) {
            if (!levelDataSet.IsContainsExit()) {
                EditorUtility.DisplayDialog("", "������� �� �������� ������. �������� ����� �� �������. (������ ������)", "��");
                return;
            }

            configDataSet.SetIsEditorMode(true);
            configDataSet.SetIsEditorPlaying(true);

            if (fileLevelNames.Contains(levelName)) {
                configDataSet.SetEditorLevelName(levelName);

                if (!AssetDatabase.Contains(levelDataSet)) {
                    AssetDatabase.CreateAsset(levelDataSet, LevelEditorPath.levelDataSetPath);
                }
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            else {
                configDataSet.SetEditorLevelName("");
                if (!AssetDatabase.Contains(levelDataSet)) {
                    AssetDatabase.CreateAsset(levelDataSet, LevelEditorPath.levelDataSetPath);
                }
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }

            UnityEditor.SceneManagement.EditorSceneManager.OpenScene("Assets/Level/Level.unity");
            EditorApplication.isPlaying = true;
        }
    }
}
#endif