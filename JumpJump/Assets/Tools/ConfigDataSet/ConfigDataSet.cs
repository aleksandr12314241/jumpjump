using UnityEngine;

namespace Tools {
    [System.Serializable]
    //[CreateAssetMenu( fileName = "ConfigDataSet", menuName = "JumpJump/ConfigDataSet", order = 1 )]
    public class ConfigDataSet : ScriptableObject {
#if UNITY_EDITOR
        [SerializeField]
        private bool isEditorMode;
        [SerializeField]
        private bool isEditorPlaying;
        [SerializeField]
        private string editroLevelName;

        internal bool IsEditorMode => isEditorMode;
        internal bool IsEditorPlaying => isEditorPlaying;

        internal string EditroLevelName => editroLevelName;
        internal bool IsEditroLevelName => string.IsNullOrEmpty( editroLevelName );

        //GETTERS 
        internal string GetPathToLevel() {
            if ( string.IsNullOrEmpty( editroLevelName ) ) {
                return LevelEditorPath.levelDataSetPath;
            }

            return LevelEditorPath.levelFolder + editroLevelName + LevelEditorPath.levelExpand;
        }

        //internal string GetPathToLevel() {
        //    return LevelEditorPath.levelDataSetPath;
        //}

        //SETTERS
        internal void SetIsEditorMode( bool isEditorMode ) {
            this.isEditorMode = isEditorMode;
        }

        internal void SetIsEditorPlaying( bool isEditorPlaying ) {
            this.isEditorPlaying = isEditorPlaying;
        }

        internal void SetEditorLevelName( string editroLevelName ) {
            this.editroLevelName = editroLevelName;
        }
#endif
    }
}