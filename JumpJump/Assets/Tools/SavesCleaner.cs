#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using Utility;

namespace Tools {

    public class SavesCleaner {
        [MenuItem("Tools/Clear Save")]
        static void StartClearSave() {
            EditorUtility.DisplayDialog("", "���������� ���� �������", "��");

            string savePath = SaveUtility.GetSavePath("save");

            if (File.Exists(savePath)) {

                string pathDecipher = Application.dataPath + "/Data/";
                var info = new DirectoryInfo(pathDecipher);
                var fileInfo = info.GetFiles();

                for (int i = 0; i < fileInfo.Length; i++) {
                    FileInfo file = fileInfo[i];

                    File.Delete(file.FullName);
                }
            }
        }
    }
}
#endif